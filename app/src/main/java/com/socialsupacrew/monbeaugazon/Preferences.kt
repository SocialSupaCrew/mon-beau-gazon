package com.socialsupacrew.monbeaugazon

import android.content.Context
import android.content.SharedPreferences

class Preferences constructor(context: Context) {
    private val prefsFileName: String = context.packageName + ".prefs"
    private var prefs: SharedPreferences

    init {
        prefs = context.getSharedPreferences(prefsFileName, Context.MODE_PRIVATE)
    }

    fun setString(entry: Entry, value: String?) {
        prefs.edit().putString(entry.name, value).apply()
    }

    fun getString(entry: Entry): String? {
        return getString(entry, null)
    }

    private fun getString(entry: Entry, defaultValue: String?): String? {
        return prefs.getString(entry.name, defaultValue)
    }
}

enum class Entry {
    TOKEN,
    OID
}
