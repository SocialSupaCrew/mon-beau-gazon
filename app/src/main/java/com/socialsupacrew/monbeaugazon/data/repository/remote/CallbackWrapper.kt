package com.socialsupacrew.monbeaugazon.data.repository.remote

import android.util.Log
import io.reactivex.observers.DisposableObserver
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

abstract class CallbackWrapper<T> : DisposableObserver<T>() {

    protected abstract fun onSuccess(result: T)

    override fun onNext(t: T) {
        // You can return StatusCodes of different cases from your API and handle it here.
        // I usually include these cases on BaseResponse and inherit it from every Response
        onSuccess(t)
    }

    override fun onError(throwable: Throwable) {
        when (throwable) {
            is HttpException -> {
                val responseBody = throwable.response()?.errorBody()
                Log.e(TAG, "HttpException: " + getErrorMessage(responseBody))
//                view.onUnknownError(getErrorMessage(responseBody))
            }
            is SocketTimeoutException -> Log.e(TAG, "timeout: ${throwable.message}")
            is IOException -> Log.e(TAG, "network error: ${throwable.message}")
            else -> Log.e(TAG, "unknown error: ${throwable.message}")
        }
    }

    override fun onComplete() {

    }

    private fun getErrorMessage(responseBody: ResponseBody?): String? {
        return try {
            val jsonObject = JSONObject(responseBody?.string())
            jsonObject.getString("error")
        } catch (e: Exception) {
            e.message
        }
    }

    companion object {
        private const val TAG = "CallbackWrapper"
    }
}
