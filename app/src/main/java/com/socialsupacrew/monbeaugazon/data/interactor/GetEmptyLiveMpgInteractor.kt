package com.socialsupacrew.monbeaugazon.data.interactor

import com.socialsupacrew.monbeaugazon.core.domain.Interactor
import com.socialsupacrew.monbeaugazon.data.interactor.GetEmptyLiveMpgInteractor.Result

interface GetEmptyLiveMpgInteractor : Interactor<Result> {

    sealed class Result {
        data class OnSuccess(val time: Long) : Result()
        object OnError : Result()
        object OnLoading : Result()
    }

    fun execute()
}
