package com.socialsupacrew.monbeaugazon.data.repository.local

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import androidx.room.Relation

@Entity(
    tableName = "player",
    foreignKeys = [
        ForeignKey(
            entity = Friend::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("friendId"),
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Club::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("clubId"),
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Player(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "fakeId") val fakeId: String,
    @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "firstName") val firstName: String?,
    @ColumnInfo(name = "lastname") val lastname: String,
    @ColumnInfo(name = "position") val position: Int,
    @ColumnInfo(name = "ultraPosition") val ultraPosition: Int,
    @ColumnInfo(name = "pricePaid") val pricePaid: Int,
    @ColumnInfo(name = "friendId") val friendId: String,
    @ColumnInfo(name = "leagueId") val leagueId: String,
    @ColumnInfo(name = "clubId") val clubId: String
)

class PlayerAndClub {

    @Embedded
    var nullPlayer: Player? = null

    val player: Player
        get() = nullPlayer!!

    @Relation(parentColumn = "clubId", entityColumn = "id")
    var _clubs: List<Club> = emptyList()

    val club: Club
        get() = _clubs[0]

    override fun toString(): String {
        return "$player $club"
    }
}
