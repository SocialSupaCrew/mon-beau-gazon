package com.socialsupacrew.monbeaugazon.data.repository.remote

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import java.lang.reflect.Type
import kotlin.Throws

class LocalDateTimeConverter: JsonDeserializer<LocalDateTime> {

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): LocalDateTime {
        val instant = if (json.asString.endsWith("Z")) {
            Instant.parse(json.asString)
        } else {
            Instant.ofEpochMilli(json.asJsonPrimitive.asLong)
        }
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault())
    }
}
