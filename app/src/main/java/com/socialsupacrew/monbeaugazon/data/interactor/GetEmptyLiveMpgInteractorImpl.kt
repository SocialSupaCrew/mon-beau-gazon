package com.socialsupacrew.monbeaugazon.data.interactor

import com.socialsupacrew.monbeaugazon.core.domain.BaseInteractor
import com.socialsupacrew.monbeaugazon.data.interactor.GetEmptyLiveMpgInteractor.Result
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveEmptyJson
import com.socialsupacrew.monbeaugazon.utils.MbgLogger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GetEmptyLiveMpgInteractorImpl constructor(
    private val repository: LiveRepository
) : BaseInteractor<Result>(), GetEmptyLiveMpgInteractor {

    override fun execute() {
        liveData.value = Result.OnLoading
        repository.getEmptyLive()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::success, ::error)
            .track()
    }

    private fun success(json: LiveEmptyJson) {
        liveData.value = Result.OnSuccess(json.nextLiveIn)
    }

    private fun error(throwable: Throwable) {
        MbgLogger.error(throwable)
        liveData.value = Result.OnError
    }
}
