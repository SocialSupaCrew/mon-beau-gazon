package com.socialsupacrew.monbeaugazon.data.repository.remote

import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers

interface ClubService {
    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("/live/standing/1")
    fun getClubs(): Single<FullClubJson>
}

data class FullClubJson(
    @SerializedName("success") val success: Boolean,
    @SerializedName("standing") val clubs: List<ClubJson>
)

data class ClubJson(
    @SerializedName("against") val goalAgainst: Int,
    @SerializedName("for") val goalFor: Int,
    @SerializedName("drawn") val drawn: Int,
    @SerializedName("lost") val lost: Int,
    @SerializedName("won") val won: Int,
    @SerializedName("points") val points: Int,
    @SerializedName("rank") val rank: Int,
    @SerializedName("played") val played: Int,
    @SerializedName("goalAverage") val goalAverage: Int,
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String
)
