package com.socialsupacrew.monbeaugazon.data.repository.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "friend",
    foreignKeys = [
        ForeignKey(
            entity = League::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("leagueId"),
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Friend(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "president") val president: String?,
    @ColumnInfo(name = "stadium") val stadium: String,
    @ColumnInfo(name = "leagueId") val leagueId: String
)
