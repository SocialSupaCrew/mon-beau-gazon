package com.socialsupacrew.monbeaugazon.data.repository

import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity
import com.socialsupacrew.monbeaugazon.data.repository.local.AppDatabase
import com.socialsupacrew.monbeaugazon.data.repository.local.League
import com.socialsupacrew.monbeaugazon.data.repository.remote.CalendarJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.FullCalendarJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.LeagueService
import com.socialsupacrew.monbeaugazon.data.repository.remote.Ranking
import io.reactivex.Flowable
import io.reactivex.Single

class LeagueRepository constructor(
    private val remoteDataSource: LeagueRemoteDataSource,
    private val localDataSource: LeagueLocalDataSource
) {

    fun getLeague(id: String): Flowable<League> {
        return localDataSource.getLeague(id)
    }

    fun getCalendar(leagueId: String): Single<CalendarJson> {
        return remoteDataSource.getCalendar(leagueId)
            .map { it.data.results }
    }

    fun getCalendar(leagueId: String, day: Int): Single<CalendarJson> {
        return remoteDataSource.getCalendar(leagueId, day)
            .map { it.data.results }
    }

    fun getRanking(leagueId: String): Single<List<LiveRankingEntity>> {
        return remoteDataSource.getRanking(leagueId)
            .map { toEntity(it) }
    }

    private fun toEntity(from: Ranking): List<LiveRankingEntity> {
        val to: MutableList<LiveRankingEntity> = mutableListOf()
        to.add(LiveRankingEntity.LiveRankingHeaderEntity)

        from.ranking.forEach ranking@{ team ->
            from.teams.forEach team@{ (id, teamName) ->
                if (team.teamId == id) {
                    team.name = teamName.name
                    team.jerseyUrl = teamName.jerseyUrl
                    return@team
                }
            }
        }

        from.ranking.forEach {
            to.add(
                LiveRankingEntity.LiveRankingTeamEntity(
                    it.teamId,
                    it.rank.toString(),
                    null,
                    it.jerseyUrl,
                    it.name,
                    it.points.toString(),
                    it.played.toString(),
                    it.victory.toString(),
                    it.defeat.toString(),
                    it.draw.toString(),
                    it.goal.toString(),
                    it.goalConceded.toString(),
                    it.difference.toString()
                )
            )
        }
        to.add(LiveRankingEntity.LiveRankingFooterEntity)
        return to
    }
}

class LeagueRemoteDataSource constructor(private val leagueService: LeagueService) {

    fun getCalendar(leagueId: String): Single<FullCalendarJson> {
        return leagueService.getCalendar(leagueId)
    }

    fun getCalendar(leagueId: String, day: Int): Single<FullCalendarJson> {
        return leagueService.getCalendar(leagueId, day)
    }

    fun getRanking(leagueId: String): Single<Ranking> {
        return leagueService.getRanking(leagueId)
    }
}

class LeagueLocalDataSource constructor(private val database: AppDatabase) {

    fun getLeague(id: String): Flowable<League> {
        return database.leagueDao().getLeague(id)
    }
}
