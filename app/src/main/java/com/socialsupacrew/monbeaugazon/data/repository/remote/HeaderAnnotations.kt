package com.socialsupacrew.monbeaugazon.data.repository.remote

import okhttp3.Request

class HeaderAnnotations {
    companion object {
        const val HEADER_NAME = "@"
        private const val HEADER_PREFIX = "$HEADER_NAME: "

        internal const val AUTHENTICATED_VALUE = "Authenticated"
        const val AUTHENTICATED = "$HEADER_PREFIX$AUTHENTICATED_VALUE"

        internal const val NOT_AUTHENTICATED_VALUE = "NotAuthenticated"
        const val NOT_AUTHENTICATED = "$HEADER_PREFIX$NOT_AUTHENTICATED_VALUE"

        internal fun getCustomAnnotation(request: Request): List<String> {
            return request.headers.values(HEADER_NAME)
        }
    }
}
