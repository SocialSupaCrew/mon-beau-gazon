package com.socialsupacrew.monbeaugazon.data.repository.entity

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.socialsupacrew.monbeaugazon.R
import org.threeten.bp.LocalDateTime
import java.util.Locale

sealed class LiveRealMatchEntity(open val id: String) {
    data class LiveRealMatchHeader(
        val matchId: String,
        val homeTeam: TeamInfo,
        val awayTeam: TeamInfo,
        val dateMatch: String,
    ) : LiveRealMatchEntity(matchId)

    object TimelineHeader : LiveRealMatchEntity(ID_TIMELINE_HEADER)

    data class TimelineItem(
        override val id: String,
        val side: String,
        val time: String,
        val type: TimelineEventType,
        val playerName: String,
        val secondPlayerName: String?,
        val reason: String?,
        val details: Int?,
        val isAssist: Boolean,
        val isStrikethrough: Boolean
    ) : LiveRealMatchEntity(id)

    companion object {
        private const val ID_TIMELINE_HEADER = "id:timelineHeader"
    }
}

data class TeamInfo(
    val name: String,
    val clubId: String,
    val score: Int,
    val goals: List<GoalInfo>
) {
    val shortName: String = name.take(3).toUpperCase(Locale.ROOT)
}

data class GoalInfo(
    val playerName: String,
    val goal: Int,
    val ownGoal: Int
)

const val SIDE_HOME = "home"
const val SIDE_AWAY = "away"
const val REASON_TACTICAL = "Tactical"
const val REASON_INJURY = "Injury"

@Suppress("unused")
enum class TimelineEventType(
    @DrawableRes val iconRes: Int,
    @ColorRes val color: Int?,
    @StringRes val details: Int?
) {
    GOAL(R.drawable.ic_soccer_24dp, null, null),
    PENALTY(R.drawable.ic_soccer_24dp, null, R.string.penalty),
    OWN(R.drawable.ic_soccer_24dp, R.color.red_500, R.string.own_goal),
    VAR(R.drawable.ic_soccer_24dp, null, R.string.`var`),
    YELLOW(R.drawable.ic_yellow_card_24, null, null),
    STRAIGHTRED(R.drawable.ic_red_card_24, null, null),
    SECONDYELLOW(R.drawable.ic_yellow_red_card_24, null, null),
    SUBSTITUTION(R.drawable.ic_substitution_24, null, null);

    companion object {
        fun getByName(name: String): TimelineEventType? {
            return values().firstOrNull { it.name == name.toUpperCase(Locale.ROOT) }
        }
    }
}
