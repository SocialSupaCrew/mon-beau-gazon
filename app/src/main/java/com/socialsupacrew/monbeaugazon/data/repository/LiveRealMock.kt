package com.socialsupacrew.monbeaugazon.data.repository



val liveRealMock = "{\n" +
        "  \"day\": 14,\n" +
        "  \"matches\": [\n" +
        "    {\n" +
        "      \"id\": \"1074951\",\n" +
        "      \"date\": \"2019-11-24T13:00:00Z\",\n" +
        "      \"period\": \"FullTime\",\n" +
        "      \"matchTime\": 94,\n" +
        "      \"quotationPreGame\": {\n" +
        "        \"Home\": 2.6,\n" +
        "        \"Draw\": 3,\n" +
        "        \"Away\": 2.95\n" +
        "      },\n" +
        "      \"matchData\": {\n" +
        "        \"home\": {\n" +
        "          \"goals\": [\n" +
        "            {\n" +
        "              \"playerId\": \"205992\",\n" +
        "              \"assistPlayerId\": \"244363\",\n" +
        "              \"type\": \"goal\",\n" +
        "              \"time\": \"76\",\n" +
        "              \"lastname\": \"Chimy Avila\"\n" +
        "            }\n" +
        "          ],\n" +
        "          \"bookings\": [\n" +
        "            {\n" +
        "              \"playerId\": \"130026\",\n" +
        "              \"type\": \"yellow\",\n" +
        "              \"time\": \"57\",\n" +
        "              \"lastname\": \"Rubén García\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"playerId\": \"51949\",\n" +
        "              \"type\": \"yellow\",\n" +
        "              \"time\": \"48\",\n" +
        "              \"lastname\": \"Lillo\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"playerId\": \"84700\",\n" +
        "              \"type\": \"yellow\",\n" +
        "              \"time\": \"32\",\n" +
        "              \"lastname\": \"Brasanac\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"playerId\": \"205992\",\n" +
        "              \"type\": \"yellow\",\n" +
        "              \"time\": \"2\",\n" +
        "              \"lastname\": \"Chimy Avila\"\n" +
        "            }\n" +
        "          ],\n" +
        "          \"substitutions\": [\n" +
        "            {\n" +
        "              \"subOff\": \"130026\",\n" +
        "              \"subOn\": \"244363\",\n" +
        "              \"reason\": \"Injury\",\n" +
        "              \"time\": \"71\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"subOff\": \"56449\",\n" +
        "              \"subOn\": \"39498\",\n" +
        "              \"reason\": \"Tactical\",\n" +
        "              \"time\": \"72\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"subOff\": \"84700\",\n" +
        "              \"subOn\": \"78874\",\n" +
        "              \"reason\": \"Tactical\",\n" +
        "              \"time\": \"78\"\n" +
        "            }\n" +
        "          ]\n" +
        "        },\n" +
        "        \"away\": {\n" +
        "          \"goals\": [\n" +
        "            {\n" +
        "              \"playerId\": \"194625\",\n" +
        "              \"assistPlayerId\": \"197314\",\n" +
        "              \"type\": \"goal\",\n" +
        "              \"time\": \"79\",\n" +
        "              \"lastname\": \"Kodro\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"playerId\": \"197334\",\n" +
        "              \"assistPlayerId\": \"439772\",\n" +
        "              \"type\": \"goal\",\n" +
        "              \"time\": \"21\",\n" +
        "              \"lastname\": \"Williams\"\n" +
        "            }\n" +
        "          ],\n" +
        "          \"bookings\": [\n" +
        "            {\n" +
        "              \"playerId\": \"42670\",\n" +
        "              \"type\": \"yellow\",\n" +
        "              \"time\": \"61\",\n" +
        "              \"lastname\": \"Yuri\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"playerId\": \"197334\",\n" +
        "              \"type\": \"yellow\",\n" +
        "              \"time\": \"58\",\n" +
        "              \"lastname\": \"Williams\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"playerId\": \"215684\",\n" +
        "              \"type\": \"yellow\",\n" +
        "              \"time\": \"52\",\n" +
        "              \"lastname\": \"Córdoba\"\n" +
        "            }\n" +
        "          ],\n" +
        "          \"substitutions\": [\n" +
        "            {\n" +
        "              \"subOff\": \"18498\",\n" +
        "              \"subOn\": \"194625\",\n" +
        "              \"reason\": \"Injury\",\n" +
        "              \"time\": \"45\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"subOff\": \"183462\",\n" +
        "              \"subOn\": \"42995\",\n" +
        "              \"reason\": \"Tactical\",\n" +
        "              \"time\": \"64\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"subOff\": \"439772\",\n" +
        "              \"subOn\": \"197314\",\n" +
        "              \"reason\": \"Tactical\",\n" +
        "              \"time\": \"74\"\n" +
        "            }\n" +
        "          ]\n" +
        "        }\n" +
        "      },\n" +
        "      \"definitiveRating\": false,\n" +
        "      \"home\": {\n" +
        "        \"id\": \"450\",\n" +
        "        \"club\": \"Osasuna\",\n" +
        "        \"players\": [\n" +
        "          {\n" +
        "            \"lastname\": \"Chimy Avila\",\n" +
        "            \"own_goals\": 0,\n" +
        "            \"goals\": 1\n" +
        "          }\n" +
        "        ],\n" +
        "        \"scoretmp\": \"1\"\n" +
        "      },\n" +
        "      \"away\": {\n" +
        "        \"id\": \"174\",\n" +
        "        \"club\": \"Bilbao\",\n" +
        "        \"players\": [\n" +
        "          {\n" +
        "            \"lastname\": \"Kodro\",\n" +
        "            \"own_goals\": 0,\n" +
        "            \"goals\": 1\n" +
        "          },\n" +
        "          {\n" +
        "            \"lastname\": \"Williams\",\n" +
        "            \"own_goals\": 0,\n" +
        "            \"goals\": 1\n" +
        "          }\n" +
        "        ],\n" +
        "        \"scoretmp\": \"2\"\n" +
        "      }\n" +
        "    },\n" +
        "    {\n" +
        "      \"id\": \"1074946\",\n" +
        "      \"date\": \"2019-11-24T15:00:00Z\",\n" +
        "      \"period\": \"FullTime\",\n" +
        "      \"matchTime\": 94,\n" +
        "      \"quotationPreGame\": {\n" +
        "        \"Home\": 1.86,\n" +
        "        \"Draw\": 3.3,\n" +
        "        \"Away\": 4.5\n" +
        "      },\n" +
        "      \"matchData\": {\n" +
        "        \"home\": {\n" +
        "          \"goals\": [],\n" +
        "          \"bookings\": [\n" +
        "            {\n" +
        "              \"playerId\": \"87026\",\n" +
        "              \"type\": \"yellow\",\n" +
        "              \"time\": \"88\",\n" +
        "              \"lastname\": \"Arbilla\"\n" +
        "            }\n" +
        "          ],\n" +
        "          \"substitutions\": [\n" +
        "            {\n" +
        "              \"subOff\": \"80791\",\n" +
        "              \"subOn\": \"86349\",\n" +
        "              \"reason\": \"Tactical\",\n" +
        "              \"time\": \"65\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"subOff\": \"59140\",\n" +
        "              \"subOn\": \"91393\",\n" +
        "              \"reason\": \"Tactical\",\n" +
        "              \"time\": \"81\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"subOff\": \"27672\",\n" +
        "              \"subOn\": \"17906\",\n" +
        "              \"reason\": \"Tactical\",\n" +
        "              \"time\": \"87\"\n" +
        "            }\n" +
        "          ]\n" +
        "        },\n" +
        "        \"away\": {\n" +
        "          \"goals\": [\n" +
        "            {\n" +
        "              \"playerId\": \"61316\",\n" +
        "              \"assistPlayerId\": \"131411\",\n" +
        "              \"type\": \"goal\",\n" +
        "              \"time\": \"90 +1\",\n" +
        "              \"lastname\": \"Joselu\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"playerId\": \"61316\",\n" +
        "              \"assistPlayerId\": \"79627\",\n" +
        "              \"type\": \"goal\",\n" +
        "              \"time\": \"85\",\n" +
        "              \"lastname\": \"Joselu\"\n" +
        "            }\n" +
        "          ],\n" +
        "          \"bookings\": [\n" +
        "            {\n" +
        "              \"playerId\": \"55179\",\n" +
        "              \"type\": \"yellow\",\n" +
        "              \"time\": \"80\",\n" +
        "              \"lastname\": \"Wakaso\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"playerId\": \"90728\",\n" +
        "              \"type\": \"yellow\",\n" +
        "              \"time\": \"37\",\n" +
        "              \"lastname\": \"Ximo Navarro\"\n" +
        "            }\n" +
        "          ],\n" +
        "          \"substitutions\": [\n" +
        "            {\n" +
        "              \"subOff\": \"490799\",\n" +
        "              \"subOn\": \"131411\",\n" +
        "              \"reason\": \"Tactical\",\n" +
        "              \"time\": \"65\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"subOff\": \"155851\",\n" +
        "              \"subOn\": \"61316\",\n" +
        "              \"reason\": \"Tactical\",\n" +
        "              \"time\": \"73\"\n" +
        "            },\n" +
        "            {\n" +
        "              \"subOff\": \"55179\",\n" +
        "              \"subOn\": \"86940\",\n" +
        "              \"reason\": \"Tactical\",\n" +
        "              \"time\": \"90 +2\"\n" +
        "            }\n" +
        "          ]\n" +
        "        }\n" +
        "      },\n" +
        "      \"definitiveRating\": false,\n" +
        "      \"home\": {\n" +
        "        \"id\": \"953\",\n" +
        "        \"club\": \"Eibar\",\n" +
        "        \"players\": [],\n" +
        "        \"scoretmp\": \"0\"\n" +
        "      },\n" +
        "      \"away\": {\n" +
        "        \"id\": \"173\",\n" +
        "        \"club\": \"Alavés\",\n" +
        "        \"players\": [\n" +
        "          {\n" +
        "            \"lastname\": \"Joselu\",\n" +
        "            \"own_goals\": 0,\n" +
        "            \"goals\": 2\n" +
        "          }\n" +
        "        ],\n" +
        "        \"scoretmp\": \"2\"\n" +
        "      }\n" +
        "    },\n" +
        "    {\n" +
        "      \"id\": \"1074953\",\n" +
        "      \"date\": \"2019-11-24T17:30:00Z\",\n" +
        "      \"period\": \"SecondHalf\",\n" +
        "      \"matchTime\": 45,\n" +
        "      \"quotationPreGame\": {\n" +
        "        \"Home\": 1.73,\n" +
        "        \"Draw\": 3.9,\n" +
        "        \"Away\": 4.6\n" +
        "      },\n" +
        "      \"matchData\": {\n" +
        "        \"home\": {\n" +
        "          \"goals\": [],\n" +
        "          \"bookings\": [\n" +
        "            {\n" +
        "              \"playerId\": \"54513\",\n" +
        "              \"type\": \"yellow\",\n" +
        "              \"time\": \"11\",\n" +
        "              \"lastname\": \"Iborra\"\n" +
        "            }\n" +
        "          ],\n" +
        "          \"substitutions\": []\n" +
        "        },\n" +
        "        \"away\": {\n" +
        "          \"goals\": [],\n" +
        "          \"bookings\": [],\n" +
        "          \"substitutions\": []\n" +
        "        }\n" +
        "      },\n" +
        "      \"definitiveRating\": false,\n" +
        "      \"home\": {\n" +
        "        \"id\": \"449\",\n" +
        "        \"club\": \"Villarreal\",\n" +
        "        \"players\": [],\n" +
        "        \"scoretmp\": \"0\"\n" +
        "      },\n" +
        "      \"away\": {\n" +
        "        \"id\": \"176\",\n" +
        "        \"club\": \"Celta\",\n" +
        "        \"players\": [],\n" +
        "        \"scoretmp\": \"0\"\n" +
        "      }\n" +
        "    },\n" +
        "    {\n" +
        "      \"id\": \"1074954\",\n" +
        "      \"date\": \"2019-11-24T20:00:00Z\",\n" +
        "      \"quotationPreGame\": {\n" +
        "        \"Home\": 4.3,\n" +
        "        \"Draw\": 3.55,\n" +
        "        \"Away\": 1.85\n" +
        "      },\n" +
        "      \"definitiveRating\": false,\n" +
        "      \"home\": {\n" +
        "        \"id\": \"192\",\n" +
        "        \"club\": \"Valladolid\",\n" +
        "        \"players\": []\n" +
        "      },\n" +
        "      \"away\": {\n" +
        "        \"id\": \"179\",\n" +
        "        \"club\": \"Sevilla\",\n" +
        "        \"players\": []\n" +
        "      }\n" +
        "    }\n" +
        "  ]\n" +
        "}"
