package com.socialsupacrew.monbeaugazon.data.repository.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Observable

@Dao
interface FriendDao {

    @Query("SELECT * FROM friend LIMIT 1")
    fun getFriend(): Friend

    @Query("SELECT * FROM friend WHERE leagueId=:leagueId")
    fun getFriends(leagueId: String): Observable<List<Friend>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFriend(friend: Friend)
}
