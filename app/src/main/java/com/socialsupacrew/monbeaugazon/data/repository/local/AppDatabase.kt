package com.socialsupacrew.monbeaugazon.data.repository.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [User::class, League::class, Friend::class, Player::class, Club::class], version = 1, exportSchema = false )
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun leagueDao(): LeagueDao
    abstract fun friendDao(): FriendDao
    abstract fun playerDao(): PlayerDao
    abstract fun clubDao(): ClubDao

    companion object {
        const val DATABASE_NAME = "MBG_database"
    }
}
