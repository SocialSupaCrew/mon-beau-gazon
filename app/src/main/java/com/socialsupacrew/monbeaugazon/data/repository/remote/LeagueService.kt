package com.socialsupacrew.monbeaugazon.data.repository.remote

import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import org.threeten.bp.LocalDateTime
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface LeagueService {
    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("/league/{leagueId}/calendar")
    fun getCalendar(@Path("leagueId") leagueId: String): Single<FullCalendarJson>

    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("/league/{leagueId}/calendar/{day}")
    fun getCalendar(@Path("leagueId") leagueId: String, @Path("day") day: Int): Single<FullCalendarJson>

    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("/league/{leagueId}/ranking")
    fun getRanking(@Path("leagueId") leagueId: String): Single<Ranking>
}

data class FullCalendarJson(
    @SerializedName("data") val data: DataCalendarJson
)

data class DataCalendarJson(
    @SerializedName("results") val results: CalendarJson
)

data class CalendarJson(
    @SerializedName("currentMatchDay") val currentMatchDay: Int,
    @SerializedName("maxMatchDay") val maxMatchDay: Int,
    @SerializedName("date") val date: LocalDateTime,
    @SerializedName("matches") val matches: List<MatchesCalendar>
)

data class MatchesCalendar(
    @SerializedName("id") val id: String,
    @SerializedName("teamHome") val teamHome: TeamCalendar,
    @SerializedName("teamAway") val teamAway: TeamCalendar
)

data class TeamCalendar(
    @SerializedName("id") val id: String,
    @SerializedName("user") val user: String,
    @SerializedName("name") val name: String,
    @SerializedName("jersey") val jersey: JerseyCalendar,
    @SerializedName("jerseyUrl") val jerseyUrl: String,
    @SerializedName("level") val level: Int,
    @SerializedName("score") val score: Int?
)

data class JerseyCalendar(
    @SerializedName("id") val id: Int,
    @SerializedName("sponsor") val sponsor: Int,
    @SerializedName("zones") val zones: Map<String, String>
)

data class Ranking(
    @SerializedName("ranking") val ranking: List<Team>,
    @SerializedName("teams") val teams: Map<String, TeamName>
)

data class Team(
    @SerializedName("teamid") val teamId: String,
    @SerializedName("rotaldo") val rotaldo: Boolean,
    @SerializedName("bonusUser") val bonusUser: Boolean,
    @SerializedName("series") val series: String,
    @SerializedName("played") val played: Int,
    @SerializedName("victory") val victory: Int,
    @SerializedName("draw") val draw: Int,
    @SerializedName("defeat") val defeat: Int,
    @SerializedName("goal") val goal: Int,
    @SerializedName("goalconceded") val goalConceded: Int,
    @SerializedName("difference") val difference: Int,
    @SerializedName("points") val points: Int,
    @SerializedName("rank") val rank: Int,
    var name: String, // not sent by the API
    var jerseyUrl: String // not sent by the API
)

data class TeamName(
    @SerializedName("name") val name: String,
    @SerializedName("abbr") val abbr: String,
    @SerializedName("jerseyUrl") val jerseyUrl: String
)
