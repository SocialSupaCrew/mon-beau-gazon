package com.socialsupacrew.monbeaugazon.data.interactor

import com.socialsupacrew.monbeaugazon.core.domain.BaseInteractor
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveMpgInteractor.Result
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveMpgEntity
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveLeagueJson
import com.socialsupacrew.monbeaugazon.utils.MbgLogger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GetLiveMpgInteractorImpl constructor(
    private val repository: LiveRepository,
    private val mapper: LiveMpgMapper
) : BaseInteractor<Result>(), GetLiveMpgInteractor {

    private var cache: List<LiveLeagueJson> = emptyList()
    private var lastSelectedMatchId: String = ""

    override fun execute() {
        liveData.value = Result.OnLoading
        repository.getLive()
            .map {
                cache = it
                mapper.map(it)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::success, ::error)
            .track()
    }

    override fun getWithSelection(matchId: String) {
        val newList = mapper.map(cache)
        newList.filterIsInstance<LiveMpgEntity.MatchLive>()
            .map {
                when {
                    lastSelectedMatchId == matchId && it.id == matchId -> {
                        it.isExpended = false
                        lastSelectedMatchId = ""
                    }
                    it.id == matchId -> {
                        it.isExpended = !it.isExpended
                        lastSelectedMatchId = matchId
                    }

                    else -> it.isExpended = false
                }
            }
        success(newList)
    }

    private fun success(teams: List<LiveMpgEntity>) {
        liveData.value = Result.OnSuccess(teams)
    }

    private fun error(throwable: Throwable) {
        MbgLogger.error(throwable)
        liveData.value = Result.OnError
    }
}
