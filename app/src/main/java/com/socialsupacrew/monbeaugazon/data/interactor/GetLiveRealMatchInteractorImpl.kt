package com.socialsupacrew.monbeaugazon.data.interactor

import com.socialsupacrew.monbeaugazon.core.domain.BaseInteractor
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveRealMatchInteractor.Result
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity
import com.socialsupacrew.monbeaugazon.utils.MbgLogger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GetLiveRealMatchInteractorImpl(
    private val repository: LiveRepository,
    private val mapper: LiveRealMatchMapper
) : BaseInteractor<Result>(), GetLiveRealMatchInteractor {

    override fun fetchMatch(matchId: String) {
        liveData.value = Result.OnLoading
        repository.getChampionshipMatch(matchId)
            .map { mapper.map(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::success, ::error)
            .track()
    }

    private fun success(matchInfo: List<LiveRealMatchEntity>) {
        liveData.value = Result.OnSuccess(matchInfo)
    }

    private fun error(throwable: Throwable) {
        MbgLogger.error(throwable)
        liveData.value = Result.OnError
    }
}
