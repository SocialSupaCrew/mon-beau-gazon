package com.socialsupacrew.monbeaugazon.data.repository.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import io.reactivex.Observable

@Dao
interface PlayerDao {

    @Query("SELECT * FROM player LIMIT 1")
    fun getPlayers(): Player

    @Transaction
    @Query("SELECT * FROM player WHERE friendId=:friendId AND leagueId=:leagueId")
    fun getPlayersForFriend(friendId: String, leagueId: String): Observable<List<PlayerAndClub>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPlayer(friend: Player)

}
