package com.socialsupacrew.monbeaugazon.data.interactor

import com.socialsupacrew.monbeaugazon.core.domain.Interactor
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveMpgInteractor.Result
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveMpgEntity

interface GetLiveMpgInteractor : Interactor<Result> {

    sealed class Result {
        data class OnSuccess(val teams: List<LiveMpgEntity>) : Result()
        object OnError : Result()
        object OnLoading : Result()
    }

    fun execute()
    fun getWithSelection(matchId: String)
}
