package com.socialsupacrew.monbeaugazon.data.repository.remote

import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface TeamService {
    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("/league/{leagueId}/teams")
    fun getTeams(@Path("leagueId") leagueId: String): Single<TeamsJson>
}

data class TeamsJson(
    @SerializedName("current_mpg_team") val currentMpgTeam: String,
    @SerializedName("teamsid") val teamsId: List<TeamsIdJson>,
    @SerializedName("teams") val teams: Map<String, FriendJson>
)

data class TeamsIdJson(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String
)

data class FriendJson(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("president") val president: String?,
    @SerializedName("stadium") val stadium: String,
    @SerializedName("players") val players: List<PlayerJson>?
)

data class PlayerJson(
    @SerializedName("id") val id: String,
    @SerializedName("club") val club: String,
    @SerializedName("firstname") val firstName: String?,
    @SerializedName("lastname") val lastname: String,
    @SerializedName("position") val position: Int,
    @SerializedName("ultraPosition") val ultraPosition: Int,
    @SerializedName("price_paid") val pricePaid: Int
)
