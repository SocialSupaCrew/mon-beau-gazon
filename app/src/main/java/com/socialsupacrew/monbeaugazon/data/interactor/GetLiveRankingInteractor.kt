package com.socialsupacrew.monbeaugazon.data.interactor

import com.socialsupacrew.monbeaugazon.core.domain.Interactor
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveRankingInteractor.Result
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity

interface GetLiveRankingInteractor : Interactor<Result> {

    sealed class Result {
        data class OnSuccess(val teams: List<LiveRankingEntity>) : Result()
        object OnError : Result()
    }

    fun execute(championshipId: Int)
}
