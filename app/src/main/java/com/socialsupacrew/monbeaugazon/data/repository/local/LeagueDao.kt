package com.socialsupacrew.monbeaugazon.data.repository.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Flowable
import io.reactivex.Observable

@Dao
interface LeagueDao {

    @Query("SELECT * FROM league")
    fun getLeagues(): Observable<List<League>>

    @Query("SELECT * FROM league WHERE id=:id")
    fun getLeague(id: String): Flowable<League>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLeague(league: League)

}
