package com.socialsupacrew.monbeaugazon.data.repository.local

import androidx.annotation.StringRes
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.socialsupacrew.monbeaugazon.R

@Entity(tableName = "club")
data class Club(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "against") val goalAgainst: Int,
    @ColumnInfo(name = "for") val goalFor: Int,
    @ColumnInfo(name = "drawn") val drawn: Int,
    @ColumnInfo(name = "lost") val lost: Int,
    @ColumnInfo(name = "won") val won: Int,
    @ColumnInfo(name = "points") val points: Int,
    @ColumnInfo(name = "rank") val rank: Int,
    @ColumnInfo(name = "played") val played: Int,
    @ColumnInfo(name = "goalAverage") val goalAverage: Int
)

@Suppress("unused")
enum class ClubJersey(val id: String, @StringRes val jersey: Int) {
    AJACCIO("1163", R.string.jersey_ajaccio),
    ALAVES("173", R.string.jersey_alaves),
    AMIENS("1430", R.string.jersey_amiens),
    ANGERS("2128", R.string.jersey_angers),
    ARSENAL("3", R.string.jersey_arsenal),
    ASTON_VILLA("7", R.string.jersey_aston_villa),
    ATALANTA("456", R.string.jersey_atalanta),
    ATLETICO("175", R.string.jersey_atletico),
    AUXERRE("138", R.string.jersey_auxerre),
    BARCELONA("178", R.string.jersey_barcelona),
    BENEVENTO("2000", R.string.jersey_benevento),
    BETIS("185", R.string.jersey_betis),
    BILBAO("174", R.string.jersey_bilbao),
    BOLOGNA("123", R.string.jersey_bologna),
    BORDEAUX("140", R.string.jersey_bordeaux),
    BOURNEMOUTH("91", R.string.jersey_bournemouth),
    BRESCIA("458", R.string.jersey_brescia),
    BREST("862", R.string.jersey_brest),
    BRIGHTON("36", R.string.jersey_brighton),
    BURNLEY("90", R.string.jersey_burnley),
    CADIX("1737", R.string.jersey_cadix),
    CAEN("1028", R.string.jersey_caen),
    CAGLIARI("124", R.string.jersey_cagliari),
    CELTA("176", R.string.jersey_celta),
    CHAMBLY("6783", R.string.jersey_chambly),
    CHATEAUROUX("8", R.string.jersey_chateauroux),
    CHELSEA("921", R.string.jersey_chelsea),
    CLERMONT("1983", R.string.jersey_clermont),
    CROTONE("744", R.string.jersey_crotone),
    CRYSTAL_PALACE("31", R.string.jersey_crystal_palace),
    DIJON("2130", R.string.jersey_dijon),
    DUNKERQUE("3288", R.string.jersey_dunkerque),
    EIBAR("953", R.string.jersey_eibar),
    ELCHE("954", R.string.jersey_elche),
    ESPANYOL("177", R.string.jersey_espanyol),
    EVERTON("11", R.string.jersey_everton),
    FIORENTINA("125", R.string.jersey_fiorentina),
    FULHAM("54", R.string.jersey_fulham),
    GENOA("990", R.string.jersey_genoa),
    GETAFE("1450", R.string.jersey_getafe),
    GRANADA("5683", R.string.jersey_granada),
    GRENOBLE("1272", R.string.jersey_grenoble),
    GUINGAMP("428", R.string.jersey_guingamp),
    HUESCA("2894", R.string.jersey_huesca),
    INTER("127", R.string.jersey_inter),
    JUVENTUS("128", R.string.jersey_juventus),
    LAZIO("129", R.string.jersey_lazio),
    LE_HAVRE("141", R.string.jersey_le_havre),
    LE_MANS("920", R.string.jersey_le_mans),
    LECCE("130", R.string.jersey_lecce),
    LEEDS("2", R.string.jersey_leeds),
    LEGANES("957", R.string.jersey_leganes),
    LEICESTER("13", R.string.jersey_leicester),
    LENS("142", R.string.jersey_lens),
    LEVANTE("855", R.string.jersey_levante),
    LILLE("429", R.string.jersey_lille),
    LIVERPOOL("14", R.string.jersey_liverpool),
    LORIENT("694", R.string.jersey_lorient),
    LYON("143", R.string.jersey_lyon),
    MALLORCA("181", R.string.jersey_mallorca),
    MAN_CITY("43", R.string.jersey_man_city),
    MAN_UNITED("1", R.string.jersey_man_united),
    MARSEILLE("144", R.string.jersey_marseille),
    METZ("145", R.string.jersey_metz),
    MILAN("120", R.string.jersey_milan),
    MONACO("146", R.string.jersey_monaco),
    MONTPELLIER("147", R.string.jersey_montpellier),
    NANCY("148", R.string.jersey_nancy),
    NANTES("430", R.string.jersey_nantes),
    NAPOLI("459", R.string.jersey_napoli),
    NEWCASTLE("4", R.string.jersey_newcastle),
    NICE("1395", R.string.jersey_nice),
    NIMES("2336", R.string.jersey_nimes),
    NIORT("1245", R.string.jersey_niort),
    NORWICH("45", R.string.jersey_norwich),
    ORLEANS("6785", R.string.jersey_orleans),
    OSASUNA("450", R.string.jersey_osasuna),
    PARIS("149", R.string.jersey_paris),
    PARIS_2("2338", R.string.jersey_paris_2),
    PARMA("131", R.string.jersey_parma),
    PAU("5420", R.string.jersey_pau),
    REAL_MADRID("186", R.string.jersey_real_madrid),
    REAL_SOCIEDAD("188", R.string.jersey_real_sociedad),
    REIMS("1423", R.string.jersey_reims),
    RENNES("150", R.string.jersey_rennes),
    RODEZ("3308", R.string.jersey_rodez),
    ROMA("121", R.string.jersey_roma),
    SAINT_ETIENNE("152", R.string.jersey_saint_etienne),
    SAMPDORIA("603", R.string.jersey_sampdoria),
    SASSUOLO("2182", R.string.jersey_sassuolo),
    SEVILLA("179", R.string.jersey_sevilla),
    SHEFFIELD("49", R.string.jersey_sheffield),
    SOCHAUX("693", R.string.jersey_sochaux),
    SOUTHAMPTON("20", R.string.jersey_southampton),
    SPAL("1454", R.string.jersey_spal),
    SPEZIA("2036", R.string.jersey_spezia),
    STRASBOURG("153", R.string.jersey_strasbourg),
    TORINO("135", R.string.jersey_torino),
    TOTTENHAM("6", R.string.jersey_tottenham),
    TOULOUSE("427", R.string.jersey_toulouse),
    TROYES("154", R.string.jersey_troyes),
    UDINESE("136", R.string.jersey_udinese),
    VALENCIA("191", R.string.jersey_valencia),
    VALENCIENNES("1996", R.string.jersey_valenciennes),
    VALLADOLID("192", R.string.jersey_valladolid),
    VERONA("126", R.string.jersey_verona),
    VILLARREAL("449", R.string.jersey_villarreal),
    WATFORD("57", R.string.jersey_watford),
    WEST_BROMWICH("35", R.string.jersey_west_bromwich),
    WEST_HAM("21", R.string.jersey_west_ham),
    WOLVERHAMPTON("39", R.string.jersey_wolverhampton);

    companion object {

        @StringRes
        fun getById(id: String): Int? {
            return values().firstOrNull { it.id == id }?.jersey
        }

        @StringRes
        fun getById(id: Int): Int? {
            return getById(id.toString())
        }
    }
}
