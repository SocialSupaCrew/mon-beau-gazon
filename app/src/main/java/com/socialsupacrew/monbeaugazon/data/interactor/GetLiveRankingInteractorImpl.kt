package com.socialsupacrew.monbeaugazon.data.interactor

import com.socialsupacrew.monbeaugazon.core.domain.BaseInteractor
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveRankingInteractor.Result
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity
import com.socialsupacrew.monbeaugazon.utils.MbgLogger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GetLiveRankingInteractorImpl constructor(
    private val repository: LiveRepository,
    private val mapper: LiveRankingMapper
) : BaseInteractor<Result>(), GetLiveRankingInteractor {

    override fun execute(championshipId: Int) {
        repository.getLiveRanking(championshipId)
            .map(mapper::map)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::success, ::error)
            .track()
    }

    private fun success(albums: List<LiveRankingEntity>) {
        liveData.value = Result.OnSuccess(albums)
    }

    private fun error(throwable: Throwable) {
        MbgLogger.error(throwable)
        liveData.value = Result.OnError
    }
}
