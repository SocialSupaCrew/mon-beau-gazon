package com.socialsupacrew.monbeaugazon.data.repository.remote

import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import org.threeten.bp.LocalDateTime
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface LiveService {
    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("/live")
    fun getLive(): Single<List<LiveLeagueJson>>

    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("/live")
    fun getEmptyLive(): Single<LiveEmptyJson>

    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("live/standing/{championshipId}")
    fun getLiveRanking(@Path("championshipId") championshipId: Int): Single<LiveRankingJson>

    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("/live/match/mpg_match_{leagueId}_{matchId}")
    fun getLiveMatch(
        @Path("leagueId") leagueId: String,
        @Path("matchId") matchId: String
    ): Single<LiveMatchJson>

    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("championship/{championshipId}/calendar/")
    fun getChampionshipCalendar(
        @Path("championshipId") championshipId: Int
    ): Single<ChampionshipJson>

    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("championship/{championshipId}/calendar/{day}")
    fun getChampionshipCalendar(
        @Path("championshipId") championshipId: Int,
        @Path("day") day: Int
    ): Single<ChampionshipJson>

    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("championship/match/{matchId}")
    fun getChampionshipMatch(@Path("matchId") matchId: String): Single<ChampionshipMatchJson>
}

data class LiveEmptyJson(
    @SerializedName("startDate") val startDate: LocalDateTime,
    @SerializedName("championship") val championship: Int,
    @SerializedName("nextLiveIn") val nextLiveIn: Long,
    @SerializedName("success") val success: String
)

data class LiveLeagueJson(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("mode") val mode: Int,
    @SerializedName("liveSubstitutes") val liveSubstitute: Boolean,
    @SerializedName("matches") val matches: List<LiveMatches>
)

data class LiveMatches(
    @SerializedName("id") val id: String,
    @SerializedName("home") val home: LiveTeam,
    @SerializedName("away") val away: LiveTeam
)

data class LiveTeam(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("jersey") val jersey: LiveJersey,
    @SerializedName("player") val player: List<LivePlayer>?
)

data class LiveJersey(
    @SerializedName("id") val id: Int,
    @SerializedName("sponsor") val sponsor: Int,
    @SerializedName("zones") val zones: Map<String, String>
)

data class LivePlayer(
    @SerializedName("lastname") val lastName: String,
    @SerializedName("own_goals") val ownGoals: Int,
    @SerializedName("goals") val goals: Int
)

data class LiveMatchJson(
    @SerializedName("stadium") val stadium: String,
    @SerializedName("dateMatch") val date: LocalDateTime,
    @SerializedName("rating") val rating: String,
    @SerializedName("teamHome") val teamHome: LiveMatchTeamJson,
    @SerializedName("teamAway") val teamAway: LiveMatchTeamJson,
    @SerializedName("players") val players: LiveMatchPlayersJson
)

data class LiveMatchTeamJson(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("jersey") val rating: LiveJersey,
    @SerializedName("abbr") val abbr: String,
    @SerializedName("coach") val coach: String,
    @SerializedName("star") val star: Boolean,
    @SerializedName("score") val score: Int,
    @SerializedName("composition") val composition: Int,
//        @SerializedName("substitutes") val substitutes: List<>,
    @SerializedName("mpguser") val mpgUser: String,
    @SerializedName("bonus") val bonus: LiveMatchBonusJson
)

data class LiveMatchBonusJson(
    @SerializedName("type") val type: Int,
    @SerializedName("playerid") val playerId: String
)

data class LiveMatchPlayersJson(
    @SerializedName("home") val home: List<LiveMatchPlayerJson>,
    @SerializedName("away") val away: List<LiveMatchPlayerJson>
)

data class LiveMatchPlayerJson(
    @SerializedName("playerId") val playerId: String,
    @SerializedName("number") val number: String,
    @SerializedName("name") val name: String?,
    @SerializedName("teamid") val teamId: Int,
    @SerializedName("goals") val goals: LiveMatchPlayerGoalsJson?,
    @SerializedName("redCard") val redCard: Int,
    @SerializedName("yellowCard") val yellowCard: Int,
    @SerializedName("firstname") val firstname: String?,
    @SerializedName("position") val position: Int,
    var note: Float?,
    var clubName: String?
)

data class LiveMatchPlayerGoalsJson(
    @SerializedName("goal") val goal: Int,
    @SerializedName("own_goal") val ownGoal: Int
)

data class LiveRankingJson(
    @SerializedName("success") val success: Boolean,
    @SerializedName("standing") val standing: List<LiveRankingStandingJson>
)

data class LiveRankingStandingJson(
    @SerializedName("against") val goalAgainst: Int,
    @SerializedName("for") val goalFor: Int,
    @SerializedName("drawn") val drawn: Int,
    @SerializedName("lost") val lost: Int,
    @SerializedName("won") val won: Int,
    @SerializedName("points") val points: Int,
    @SerializedName("rank") val rank: Int,
    @SerializedName("played") val played: Int,
    @SerializedName("goalAverage") val goalDiff: Int,
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String
)

data class ChampionshipJson(
    @SerializedName("day") val day: Int,
    @SerializedName("matches") val matches: List<ChampionshipCalendarMatchJson>
)

data class ChampionshipCalendarMatchJson(
    @SerializedName("id") val id: String,
    @SerializedName("date") val date: LocalDateTime,
    @SerializedName("period") val period: String?,
    @SerializedName("matchTime") val matchTime: Int?,
    @SerializedName("quotationPreGame") val quotationPreGame: ChampionshipQuotationPreGameJson,
    @SerializedName("matchData") val matchData: MatchDataJson?,
    @SerializedName("definitiveRating") val definitiveRating: Boolean,
    @SerializedName("home") val home: ChampionshipTeamJson,
    @SerializedName("away") val away: ChampionshipTeamJson
)

data class MatchDataJson(
    @SerializedName("home") val home: MatchDataTeamJson,
    @SerializedName("away") val away: MatchDataTeamJson
)

data class MatchDataTeamJson(
    @SerializedName("goals") val goals: List<MatchDataGoalsJson>?,
    @SerializedName("bookings") val bookings: List<MatchDataBookingsJson>?,
    @SerializedName("substitutions") val substitutions: List<MatchDataSubstitutionsJson>?
)

data class MatchDataGoalsJson(
    @SerializedName("playerId") val playerId: String,
    @SerializedName("assistPlayerId") val assistPlayerId: String,
    @SerializedName("type") val type: String,
    @SerializedName("time") val time: String,
    @SerializedName("lastname") val lastname: String
)

data class MatchDataBookingsJson(
    @SerializedName("playerId") val playerId: String,
    @SerializedName("type") val type: String,
    @SerializedName("time") val time: String,
    @SerializedName("lastname") val lastname: String
)

data class MatchDataSubstitutionsJson(
    @SerializedName("subOff") val subOff: String,
    @SerializedName("subOn") val subOn: String,
    @SerializedName("reason") val reason: String,
    @SerializedName("time") val time: String
)

data class ChampionshipTeamJson(
    @SerializedName("players") val players: List<ChampionshipPlayerJson>?,
    @SerializedName("club") val club: String,
    @SerializedName("id") val id: Int,
    @SerializedName("score") val score: Int?
)

data class ChampionshipPlayerJson(
    @SerializedName("lastname") val lastName: String,
    @SerializedName("own_goals") val ownGoal: Int,
    @SerializedName("goals") val goals: Int
)

data class ChampionshipMatchJson(
    @SerializedName("id") val id: String,
    @SerializedName("type") val type: String,
    @SerializedName("dateMatch") val dateMatch: LocalDateTime,
    @SerializedName("Home") val home: ChampionshipMatchTeamJson,
    @SerializedName("Away") val away: ChampionshipMatchTeamJson,
    @SerializedName("quotationPreGame") val quotationPreGame: ChampionshipQuotationPreGameJson,
    @SerializedName("quotations") val quotations: ChampionshipQuotationJson,
    @SerializedName("period") val period: String,
    @SerializedName("matchTime") val matchTime: String,
    @SerializedName("definitiveRating") val definitiveRating: String,
    @SerializedName("matchData") val matchData: ChampionshipMatchDataJson
)

data class ChampionshipMatchTeamJson(
    @SerializedName("id") val id: String,
    @SerializedName("club") val club: String,
    @SerializedName("players") val players: Map<String, ChampionshipMatchPlayerJson>,
    @SerializedName("score") val score: String
)

data class ChampionshipMatchPlayerJson(
    @SerializedName("info") val info: ChampionshipMatchPlayerInfoJson
)

data class ChampionshipMatchPlayerInfoJson(
    @SerializedName("idteam") val idTeam: String,
    @SerializedName("idplayer") val idPlayer: String,
    @SerializedName("lastname") val lastname: String,
    @SerializedName("position") val position: String,
    @SerializedName("formation_place") val formationPlace: Int,
    @SerializedName("formation_used") val formationUsed: String,
    @SerializedName("sub") val sub: Int,
    @SerializedName("goals") val goals: Int,
    @SerializedName("own_goals") val ownGoals: Int,
    @SerializedName("yellow_card") val yellowCard: Int,
    @SerializedName("red_card") val redCard: Int,
    @SerializedName("mins_played") val minsPlayed: Int,
    @SerializedName("note_final_2015") val noteFinal2015: Float
)

data class ChampionshipMatchDataJson(
    @SerializedName("timeline") val timeline: List<ChampionshipMatchTimelineEvent>
)

data class ChampionshipMatchTimelineEvent(
    @SerializedName("time") val time: String,
    @SerializedName("type") val type: String,
    @SerializedName("side") val side: String,
    @SerializedName("playerId") val playerId: String?,
    @SerializedName("lastname") val lastname: String?,
    @SerializedName("assistPlayerId") val assistPlayerId: String?,
    @SerializedName("assistLastname") val assistLastname: String?,
    @SerializedName("subOnLastname") val subOnLastname: String?,
    @SerializedName("subOffLastname") val subOffLastname: String?,
    @SerializedName("subOn") val subOn: String?,
    @SerializedName("subOff") val subOff: String?,
    @SerializedName("reason") val reason: String?
)

data class ChampionshipQuotationPreGameJson(
    @SerializedName("Home") val home: Float,
    @SerializedName("Draw") val draw: Float,
    @SerializedName("Away") val away: Float
)

data class ChampionshipQuotationJson(
    @SerializedName("away") val away: String,
    @SerializedName("home") val home: String,
    @SerializedName("draw") val draw: String,
    @SerializedName("event") val event: ChampionshipQuotationEventJson,
    @SerializedName("live") val live: Boolean
)

data class ChampionshipQuotationEventJson(
    @SerializedName("a") val a: String,
    @SerializedName("b") val b: String,
    @SerializedName("id") val id: String
)
