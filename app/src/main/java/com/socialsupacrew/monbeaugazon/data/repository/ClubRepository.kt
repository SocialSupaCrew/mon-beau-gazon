package com.socialsupacrew.monbeaugazon.data.repository

import com.socialsupacrew.monbeaugazon.data.repository.local.AppDatabase
import com.socialsupacrew.monbeaugazon.data.repository.local.Club
import com.socialsupacrew.monbeaugazon.data.repository.remote.ClubJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.ClubService
import com.socialsupacrew.monbeaugazon.data.repository.remote.FullClubJson
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class ClubRepository constructor(
    private val remote: ClubRemoteDataSource,
    private val local: ClubLocalDataSource
) {
    fun syncClubs(): Single<FullClubJson> {
        return remote.fetchClubs()
            .subscribeOn(Schedulers.io())
            .doOnSuccess { local.saveClubs(it.clubs) }
    }

    fun getClubById(id: String) = local.getClubById(id)

    companion object {
        private const val TAG = "ClubRepository"
    }
}

class ClubRemoteDataSource constructor(private val clubService: ClubService) {
    fun fetchClubs(): Single<FullClubJson> {
        return clubService.getClubs()
    }
}

class ClubLocalDataSource constructor(private val database: AppDatabase) {

    fun saveClubs(clubs: List<ClubJson>) {
        clubs.forEach { database.clubDao().insertClub(toEntity(it)) }
    }

    fun getClubs(): Single<List<Club>> {
        return database.clubDao().getClubs()
    }

    fun getClubById(id: String) = database.clubDao().getClubById(id)

    private fun toEntity(from: ClubJson): Club {
        return Club(
            from.id,
            from.name.trim(),
            from.goalAgainst,
            from.goalFor,
            from.drawn,
            from.lost,
            from.won,
            from.points,
            from.rank,
            from.played,
            from.goalAverage
        )
    }
}
