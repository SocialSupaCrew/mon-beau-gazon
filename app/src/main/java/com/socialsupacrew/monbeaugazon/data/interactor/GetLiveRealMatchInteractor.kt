package com.socialsupacrew.monbeaugazon.data.interactor

import com.socialsupacrew.monbeaugazon.core.domain.Interactor
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveRealMatchInteractor.Result
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity

interface GetLiveRealMatchInteractor : Interactor<Result> {

    sealed class Result {
        data class OnSuccess(val matchInfo: List<LiveRealMatchEntity>) : Result()
        object OnError : Result()
        object OnLoading : Result()
    }

    fun fetchMatch(matchId: String)
}
