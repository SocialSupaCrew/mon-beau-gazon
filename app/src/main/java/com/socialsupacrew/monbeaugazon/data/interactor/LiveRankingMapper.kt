package com.socialsupacrew.monbeaugazon.data.interactor

import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity.LiveRankingFooterEntity
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity.LiveRankingHeaderEntity
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity.LiveRankingTeamEntity
import com.socialsupacrew.monbeaugazon.data.repository.local.ClubJersey
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveRankingStandingJson

class LiveRankingMapper {

    fun map(from: List<LiveRankingStandingJson>): List<LiveRankingEntity> {
        val to: MutableList<LiveRankingEntity> = mutableListOf()
        to.add(LiveRankingHeaderEntity)
        from.forEach {
            to.add(
                LiveRankingTeamEntity(
                    it.id,
                    it.rank.toString(),
                    it.id,
                    null,
                    it.name,
                    it.points.toString(),
                    it.played.toString(),
                    it.won.toString(),
                    it.lost.toString(),
                    it.drawn.toString(),
                    it.goalFor.toString(),
                    it.goalAgainst.toString(),
                    it.goalDiff.toString()
                )
            )
        }
        to.add(LiveRankingFooterEntity)
        return to
    }
}
