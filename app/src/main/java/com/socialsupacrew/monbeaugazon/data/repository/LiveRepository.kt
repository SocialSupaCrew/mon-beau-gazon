package com.socialsupacrew.monbeaugazon.data.repository

import android.util.Log
import androidx.annotation.StringRes
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.FooterRealLive
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.GoalRealLive
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.GoalType
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.HeaderRealLive
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.MatchRealLive
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.Period
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.TeamRealLive
import com.socialsupacrew.monbeaugazon.data.repository.local.Championship
import com.socialsupacrew.monbeaugazon.data.repository.remote.ChampionshipCalendarMatchJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.ChampionshipJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.ChampionshipMatchJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveEmptyJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveLeagueJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveMatchJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveMatchPlayerJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveRankingJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveRankingStandingJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveService
import com.socialsupacrew.monbeaugazon.data.repository.remote.MatchDataGoalsJson
import io.reactivex.Single
import org.threeten.bp.format.DateTimeFormatter

class LiveRepository constructor(
    private val clubRepository: ClubRepository,
    private val remoteDataSource: LiveRemoteDataSource
) {

    fun getLive(): Single<List<LiveLeagueJson>> {
        return remoteDataSource.getLive()
    }

    fun getEmptyLive(): Single<LiveEmptyJson> {
        return remoteDataSource.getEmptyLive()
    }

    fun getLiveMatch(leagueId: String, matchId: String): Single<LiveMatchJson> {
        val liveMatch = remoteDataSource.getLiveMatch(leagueId, matchId)
        val championshipCalendar = remoteDataSource.getChampionshipCalendar()
        return liveMatch.zipWith(championshipCalendar, { liveMatchJson, championshipJson ->
            addInfoToPlayers(liveMatchJson.players.home, championshipJson)
            addInfoToPlayers(liveMatchJson.players.away, championshipJson)
            liveMatchJson
        })
    }

    fun getLiveRanking(championshipId: Int): Single<List<LiveRankingStandingJson>> {
        return remoteDataSource.getLiveRanking(championshipId)
            .flatMap {
                if (it.success) {
                    Single.fromCallable { it.standing }
                } else {
                    Single.error(Throwable("/live/standing/# : Api respond `success = false`"))
                }
            }
    }

    fun getLiveChampionship(
        championship: Championship,
        day: Int = -1
    ): Single<List<LiveRealEntity>> {
        return remoteDataSource
            .getChampionshipCalendar(championship.id, day)
            .map(this::toChampionshipEntities)
    }

    fun getChampionshipMatch(matchId: String): Single<ChampionshipMatchJson> {
        return remoteDataSource.getChampionshipMatch(matchId)
    }

    private fun addInfoToPlayers(
        players: List<LiveMatchPlayerJson>,
        championship: ChampionshipJson
    ) {
        addNoteToPlayers(players, championship)
        addClubToPlayers(players)
    }

    private fun addClubToPlayers(players: List<LiveMatchPlayerJson>) {
        players.filter { it.teamId != 0 }
            .forEach { player ->
                val teamId = player.teamId
                val club = clubRepository.getClubById(teamId.toString())
                player.clubName = club.name
            }
    }

    private fun addNoteToPlayers(
        players: List<LiveMatchPlayerJson>,
        championship: ChampionshipJson
    ) {
        players.forEach { player ->
            val teamId = player.teamId
            val match = championship.matches.find {
                it.home.id == teamId || it.away.id == teamId
            } ?: return

            remoteDataSource.getChampionshipMatch(match.id)
                .doOnSuccess { matchJson ->
                    val playerId = player.playerId.substringAfter("_")
                    val playerWithNote = matchJson.home.players[playerId]
                        ?: matchJson.away.players[playerId]
                    player.note = playerWithNote?.info?.noteFinal2015
                }
                .subscribe({},
                    { throwable -> Log.e(TAG, "error: ${throwable.message}") })
        }
    }

    private fun toChampionshipEntities(from: ChampionshipJson): List<LiveRealEntity> {
        val dateFormatter = DateTimeFormatter.ofPattern(DATE_PATTERN)
        val matches = from.matches
        val entityList = mutableListOf<LiveRealEntity>()
        val matchesByDate = matches.groupBy { it.date.toLocalDate() }
        matchesByDate.forEach {
            entityList.add(HeaderRealLive(it.key.toString(), it.key.format(dateFormatter)))
            it.value.forEach {
                entityList.add(toChampionshipMatchEntity(it))
            }
        }
        entityList.add(FooterRealLive)
        return entityList
    }

    private fun toChampionshipMatchEntity(from: ChampionshipCalendarMatchJson): MatchRealLive {
        val matchHour = from.date.format(DateTimeFormatter.ofPattern(HOUR_PATTERN))
        val teamHome = TeamRealLive(
            from.home.club,
            from.home.id,
            from.matchData?.home?.goals?.filter {
                GoalType.getByTypeId(it.type) != GoalType.VAR
            }?.size ?: 0,
            getRealGoals(from.matchData?.home?.goals)
        )

        val teamAway = TeamRealLive(
            from.away.club,
            from.away.id,
            from.matchData?.away?.goals?.filter {
                GoalType.getByTypeId(it.type) != GoalType.VAR
            }?.size ?: 0,
            getRealGoals(from.matchData?.away?.goals)
        )

        return MatchRealLive(
            from.id,
            matchHour,
            Period.getByTypeId(from.period),
            from.matchTime,
            teamHome,
            teamAway
        )
    }

    private fun getRealGoals(goals: List<MatchDataGoalsJson>?): List<GoalRealLive> {
        return goals?.map {
            GoalRealLive(it.lastname, it.time, GoalType.getByTypeId(it.type))
        } ?: emptyList()
    }

    sealed class LiveRealEntity(open val id: String) {
        data class HeaderRealLive(override val id: String, val date: String) : LiveRealEntity(id)
        object FooterRealLive : LiveRealEntity(Int.MAX_VALUE.toString())
        data class MatchRealLive(
            override val id: String,
            val hour: String,
            val period: Period,
            val time: Int?,
            val teamHome: TeamRealLive,
            val teamAway: TeamRealLive
        ) : LiveRealEntity(id)

        data class TeamRealLive(
            val name: String,
            val clubId: Int,
            val score: Int?,
            val goals: List<GoalRealLive>
        )

        data class GoalRealLive(val lastName: String, val goalTime: String, val type: GoalType)

        @Suppress("unused")
        enum class GoalType(val typeId: String, @StringRes val typeText: Int) {
            GOAL("goal", R.string.goal),
            VAR("var", R.string.`var`),
            CSC("own", R.string.own_goal),
            PENALTY("penalty", R.string.penalty),
            NULL("", 0);

            companion object {
                fun getByTypeId(typeId: String?): GoalType {
                    return values().find { it.typeId == typeId } ?: NULL
                }
            }
        }

        @Suppress("unused")
        enum class Period(val value: String) {
            FIRST_HALF("FirstHalf"),
            HALF_TIME("HalfTime"),
            SECOND_HALF("SecondHalf"),
            FULL_TIME("FullTime"),
            NULL("");

            companion object {
                fun getByTypeId(value: String?): Period {
                    return values().find { it.value == value } ?: NULL
                }
            }
        }
    }

    companion object {
        private const val TAG = "LiveRepository"
        private const val DATE_PATTERN = "EEEE d MMMM"
        private const val HOUR_PATTERN = "HH:mm"
    }
}

class LiveRemoteDataSource constructor(
    private val liveService: LiveService,
    private val gson: Gson
) {

    fun getLive(): Single<List<LiveLeagueJson>> {
//        return Single.error(Throwable("NoLive"))
//        return getLiveMock()
        return liveService.getLive()
    }

    fun getEmptyLive(): Single<LiveEmptyJson> {
//        return getLiveMockEmpty()
        return liveService.getEmptyLive()
    }

    fun getLiveMatch(leagueId: String, matchId: String): Single<LiveMatchJson> {
        return liveService.getLiveMatch(leagueId, matchId)
    }

    fun getLiveRanking(championshipId: Int): Single<LiveRankingJson> {
        return liveService.getLiveRanking(championshipId)
    }

    fun getChampionshipCalendar(championshipId: Int = 1, day: Int = -1): Single<ChampionshipJson> {
//        return getLiveRealMock()
        return if (day == -1) {
            liveService.getChampionshipCalendar(championshipId)
        } else {
            liveService.getChampionshipCalendar(championshipId, day)
        }
    }

    fun getChampionshipMatch(matchId: String): Single<ChampionshipMatchJson> {
        return liveService.getChampionshipMatch(matchId)
    }

    private fun getLiveMock(): Single<List<LiveLeagueJson>> {
        val typeList = object : TypeToken<List<LiveLeagueJson>>() {}.type
        return Single.just(gson.fromJson(liveMock, typeList))
    }

    private fun getLiveMockEmpty(): Single<LiveEmptyJson> {
        val typeList = object : TypeToken<LiveEmptyJson>() {}.type
        return Single.just(gson.fromJson(liveMockEmpty, typeList))
    }

    private fun getLiveRealMock(): Single<ChampionshipJson> {
        val typeList = object : TypeToken<ChampionshipJson>() {}.type
        return Single.just(gson.fromJson(liveRealMock, typeList))
    }

    private val liveMockEmpty = "{\n" +
            "  \"startDate\": \"2019-11-29T19:45:00Z\",\n" +
            "  \"championship\": 1,\n" +
            "  \"nextLiveIn\": 80390,\n" +
            "  \"success\": \"noLive\"\n" +
            "}"

    private val liveMock = "[\n" +
            "  {\n" +
            "    \"id\": \"LDMGPJ7\",\n" +
            "    \"name\": \"Ligue Test\",\n" +
            "    \"mode\": 1,\n" +
            "    \"liveSubstitutes\": false,\n" +
            "    \"matches\": [\n" +
            "      {\n" +
            "        \"id\": \"2_1_1\",\n" +
            "        \"home\": {\n" +
            "          \"id\": \"mpg_team_LDMGPJ7$\$mpg_user_1456093\",\n" +
            "          \"name\": \"SocialSupaTest\",\n" +
            "          \"jersey\": {\n" +
            "            \"id\": 0,\n" +
            "            \"sponsor\": 8,\n" +
            "            \"zones\": {\n" +
            "              \"z1\": \"#6c2096\"\n" +
            "            }\n" +
            "          },\n" +
            "          \"player\": [],\n" +
            "          \"mpgUserId\": \"mpg_user_1456093\"\n" +
            "        },\n" +
            "        \"away\": {\n" +
            "          \"id\": \"mpg_team_LDMGPJ7$\$mpg_user_163535\",\n" +
            "          \"name\": \"Aaa\",\n" +
            "          \"jersey\": {\n" +
            "            \"id\": 0,\n" +
            "            \"sponsor\": 1,\n" +
            "            \"zones\": {\n" +
            "              \"z1\": \"#FFFFFF\"\n" +
            "            }\n" +
            "          },\n" +
            "          \"player\": [\n" +
            "            {\n" +
            "              \"lastname\": \"Di María\",\n" +
            "              \"own_goals\": 0,\n" +
            "              \"goals\": 1\n" +
            "            },\n" +
            "            {\n" +
            "              \"lastname\": \"Pepe\",\n" +
            "              \"own_goals\": 0,\n" +
            "              \"goals\": 1\n" +
            "            },\n" +
            "            {\n" +
            "              \"lastname\": \"Cavani\",\n" +
            "              \"own_goals\": 0,\n" +
            "              \"goals\": 1\n" +
            "            }\n" +
            "          ],\n" +
            "          \"mpgUserId\": \"mpg_user_163535\"\n" +
            "        }\n" +
            "      }\n" +
            "    ]\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"KJSELZZ5\",\n" +
            "    \"name\": \"Ligue des gros nazes\",\n" +
            "    \"mode\": 1,\n" +
            "    \"liveSubstitutes\": false,\n" +
            "    \"matches\": [\n" +
            "      {\n" +
            "        \"id\": \"3_6_1\",\n" +
            "        \"home\": {\n" +
            "          \"id\": \"mpg_team_KJSELZZ5$\$mpg_user_1456093\",\n" +
            "          \"name\": \"SSC Football Club\",\n" +
            "          \"jersey\": {\n" +
            "            \"id\": 0,\n" +
            "            \"sponsor\": 2,\n" +
            "            \"zones\": {\n" +
            "              \"z1\": \"#ff8ebf\"\n" +
            "            }\n" +
            "          },\n" +
            "          \"player\": [\n" +
            "            {\n" +
            "              \"lastname\": \"Coulibaly\",\n" +
            "              \"own_goals\": 0,\n" +
            "              \"goals\": 1\n" +
            "            }\n" +
            "          ],\n" +
            "          \"mpgUserId\": \"mpg_user_1456093\"\n" +
            "        },\n" +
            "        \"away\": {\n" +
            "          \"id\": \"mpg_team_KJSELZZ5$\$mpg_user_1282470\",\n" +
            "          \"name\": \"Girl power\",\n" +
            "          \"jersey\": {\n" +
            "            \"id\": 0,\n" +
            "            \"sponsor\": 3,\n" +
            "            \"zones\": {\n" +
            "              \"z1\": \"#45c945\"\n" +
            "            }\n" +
            "          },\n" +
            "          \"player\": [\n" +
            "            {\n" +
            "              \"lastname\": \"Di María\",\n" +
            "              \"own_goals\": 0,\n" +
            "              \"goals\": 1\n" +
            "            }\n" +
            "          ],\n" +
            "          \"mpgUserId\": \"mpg_user_1282470\"\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"id\": \"3_6_2\",\n" +
            "        \"home\": {\n" +
            "          \"id\": \"mpg_team_KJSELZZ5$\$mpg_user_1124541\",\n" +
            "          \"name\": \"Moloko +\",\n" +
            "          \"jersey\": {\n" +
            "            \"id\": 0,\n" +
            "            \"sponsor\": 3,\n" +
            "            \"zones\": {\n" +
            "              \"z1\": \"#000000\"\n" +
            "            }\n" +
            "          },\n" +
            "          \"player\": [],\n" +
            "          \"mpgUserId\": \"mpg_user_1124541\"\n" +
            "        },\n" +
            "        \"away\": {\n" +
            "          \"id\": \"mpg_team_KJSELZZ5$\$mpg_user_163535\",\n" +
            "          \"name\": \"tchoin fc\",\n" +
            "          \"jersey\": {\n" +
            "            \"id\": 0,\n" +
            "            \"sponsor\": 1,\n" +
            "            \"zones\": {\n" +
            "              \"z1\": \"#ff8ebf\"\n" +
            "            }\n" +
            "          },\n" +
            "          \"player\": [\n" +
            "            {\n" +
            "              \"lastname\": \"Pepe\",\n" +
            "              \"own_goals\": 0,\n" +
            "              \"goals\": 1\n" +
            "            }\n" +
            "          ],\n" +
            "          \"mpgUserId\": \"mpg_user_163535\"\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"id\": \"3_6_3\",\n" +
            "        \"home\": {\n" +
            "          \"id\": \"mpg_team_KJSELZZ5$\$mpg_user_454277\",\n" +
            "          \"name\": \"LENS\",\n" +
            "          \"jersey\": {\n" +
            "            \"id\": 1,\n" +
            "            \"zones\": {\n" +
            "              \"z6\": \"#fff03f\",\n" +
            "              \"z2\": \"#ff2626\",\n" +
            "              \"z3\": \"#fff03f\",\n" +
            "              \"z4\": \"#ff2626\",\n" +
            "              \"z5\": \"#fff03f\",\n" +
            "              \"z1\": \"#ff2626\",\n" +
            "              \"z7\": \"#000000\"\n" +
            "            },\n" +
            "            \"sponsor\": 7\n" +
            "          },\n" +
            "          \"player\": [\n" +
            "            {\n" +
            "              \"lastname\": \"Bourigeaud\",\n" +
            "              \"own_goals\": 0,\n" +
            "              \"goals\": 1\n" +
            "            },\n" +
            "            {\n" +
            "              \"lastname\": \"Sanson\",\n" +
            "              \"own_goals\": 0,\n" +
            "              \"goals\": 1\n" +
            "            },\n" +
            "            {\n" +
            "              \"lastname\": \"Cavani\",\n" +
            "              \"own_goals\": 0,\n" +
            "              \"goals\": 1\n" +
            "            }\n" +
            "          ],\n" +
            "          \"mpgUserId\": \"mpg_user_454277\"\n" +
            "        },\n" +
            "        \"away\": {\n" +
            "          \"id\": \"mpg_team_KJSELZZ5$\$mpg_user_31538\",\n" +
            "          \"name\": \"Fc Avé Maria\",\n" +
            "          \"jersey\": {\n" +
            "            \"id\": 0,\n" +
            "            \"sponsor\": 3,\n" +
            "            \"zones\": {\n" +
            "              \"z1\": \"#4183e8\"\n" +
            "            }\n" +
            "          },\n" +
            "          \"player\": [\n" +
            "            {\n" +
            "              \"lastname\": \"Jonathan Ikone\",\n" +
            "              \"own_goals\": 0,\n" +
            "              \"goals\": 1\n" +
            "            },\n" +
            "            {\n" +
            "              \"lastname\": \"Mbappé\",\n" +
            "              \"own_goals\": 0,\n" +
            "              \"goals\": 1\n" +
            "            }\n" +
            "          ],\n" +
            "          \"mpgUserId\": \"mpg_user_31538\"\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"id\": \"3_6_4\",\n" +
            "        \"home\": {\n" +
            "          \"id\": \"mpg_team_KJSELZZ5$\$mpg_user_449424\",\n" +
            "          \"name\": \"Fc Bieng\",\n" +
            "          \"jersey\": {\n" +
            "            \"id\": 5,\n" +
            "            \"zones\": {\n" +
            "              \"z8\": \"#FFFFFF\",\n" +
            "              \"z2\": \"#45C945\",\n" +
            "              \"z3\": \"#4054CC\",\n" +
            "              \"z4\": \"#FFFFFF\",\n" +
            "              \"z5\": \"#45C945\",\n" +
            "              \"z6\": \"#4054CC\",\n" +
            "              \"z7\": \"#4054CC\",\n" +
            "              \"z1\": \"#4054cc\",\n" +
            "              \"z9\": \"#45C945\"\n" +
            "            },\n" +
            "            \"sponsor\": 3\n" +
            "          },\n" +
            "          \"player\": [\n" +
            "            {\n" +
            "              \"lastname\": \"Falcao\",\n" +
            "              \"own_goals\": 0,\n" +
            "              \"goals\": 1\n" +
            "            }\n" +
            "          ],\n" +
            "          \"mpgUserId\": \"mpg_user_449424\"\n" +
            "        },\n" +
            "        \"away\": {\n" +
            "          \"id\": \"mpg_team_KJSELZZ5$\$mpg_user_603968\",\n" +
            "          \"name\": \"German Porn\",\n" +
            "          \"jersey\": {\n" +
            "            \"id\": 0,\n" +
            "            \"sponsor\": 4,\n" +
            "            \"zones\": {\n" +
            "              \"z1\": \"#ff2626\"\n" +
            "            }\n" +
            "          },\n" +
            "          \"player\": [],\n" +
            "          \"mpgUserId\": \"mpg_user_603968\",\n" +
            "          \"targetMan\": true\n" +
            "        }\n" +
            "      }\n" +
            "    ]\n" +
            "  }\n" +
            "]"
}
