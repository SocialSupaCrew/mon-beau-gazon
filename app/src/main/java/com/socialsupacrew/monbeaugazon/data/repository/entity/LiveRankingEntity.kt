package com.socialsupacrew.monbeaugazon.data.repository.entity

import androidx.annotation.StringRes

sealed class LiveRankingEntity(open val id: String) {
    object LiveRankingHeaderEntity : LiveRankingEntity("")
    data class LiveRankingTeamEntity(
        override val id: String,
        val rank: String,
        val clubId: String?,
        val jerseyUrl: String?,
        val name: String,
        val points: String,
        val played: String,
        val won: String,
        val lost: String,
        val drawn: String,
        val goalFor: String,
        val goalAgainst: String,
        val goalDiff: String
    ) : LiveRankingEntity(id)

    object LiveRankingFooterEntity : LiveRankingEntity(Int.MAX_VALUE.toString())
}
