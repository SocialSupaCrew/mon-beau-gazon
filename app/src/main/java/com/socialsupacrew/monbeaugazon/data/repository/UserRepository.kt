package com.socialsupacrew.monbeaugazon.data.repository

import android.util.Log
import com.socialsupacrew.monbeaugazon.Entry
import com.socialsupacrew.monbeaugazon.Preferences
import com.socialsupacrew.monbeaugazon.data.repository.local.AppDatabase
import com.socialsupacrew.monbeaugazon.data.repository.local.League
import com.socialsupacrew.monbeaugazon.data.repository.remote.DashboardJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.SignInUserBody
import com.socialsupacrew.monbeaugazon.data.repository.remote.SignInUserJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.UserService
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class UserRepository constructor(
    private val remoteDataSource: UserRemoteDataSource,
    private val localDataSource: UserLocalDataSource
) {

    fun signInUser(signInUserBody: SignInUserBody): Observable<SignInUserJson> {
        return remoteDataSource.signInUser(signInUserBody)
    }

    fun getUser() {

    }

    fun saveUser() {

    }

    fun getLeagues(): Observable<List<League>> {
        remoteDataSource.getDashboard()
            .subscribeOn(Schedulers.io())
            .subscribe(localDataSource::saveLeagues) {
                Log.e(TAG, "$it")
            }
        return localDataSource.getLeagues()
    }

    fun disconnect(): Completable = localDataSource.disconnect()

    companion object {
        private const val TAG = "UserRepository"
    }
}

class UserRemoteDataSource constructor(private val userService: UserService) {

    fun signInUser(signInUserBody: SignInUserBody): Observable<SignInUserJson> {
        return userService.signIn(signInUserBody).toObservable()
    }

    fun getDashboard(): Single<List<League>> {
        return userService.getDashBoard()
            .map(this::toEntity)
    }

    private fun toEntity(from: DashboardJson): List<League> {
        return from.data.leagues.map {
            League(
                it.id,
                it.adminMpgUserId,
                it.currentMpgUser,
                it.name,
                it.leagueStatus,
                it.championship,
                it.mode,
                it.teamStatus,
                it.url
            )
        }
    }
}

class UserLocalDataSource constructor(
    private val preferences: Preferences,
    private val database: AppDatabase
) {

    fun disconnect(): Completable {
        return Completable.create {
            preferences.setString(Entry.TOKEN, null)
            preferences.setString(Entry.OID, null)
            database.clearAllTables()
            it.onComplete()
        }
    }

    fun getLeagues(): Observable<List<League>> {
        return database.leagueDao().getLeagues()
    }

    fun saveLeagues(leagues: List<League>) {
        leagues.forEach {
            database.leagueDao().insertLeague(it)
        }
    }
}
