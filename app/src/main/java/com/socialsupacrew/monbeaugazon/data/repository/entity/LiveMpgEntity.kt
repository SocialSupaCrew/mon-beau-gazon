package com.socialsupacrew.monbeaugazon.data.repository.entity

sealed class LiveMpgEntity(open val id: String) {
    data class HeaderLive(override val id: String, val leagueName: String) : LiveMpgEntity(id)
    object FooterLive : LiveMpgEntity(Int.MAX_VALUE.toString())
    data class MatchLive(
        override val id: String,
        val leagueId: String,
        val teamHome: TeamLive,
        val teamAway: TeamLive,
        var isExpended: Boolean
    ) : LiveMpgEntity(id)

    data class TeamLive(val name: String, val score: Int, val players: List<PlayerLive>)
    data class PlayerLive(val lastName: String, val ownGoal: Int, val goal: Int)
}
