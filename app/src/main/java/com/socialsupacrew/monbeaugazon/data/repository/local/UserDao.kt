package com.socialsupacrew.monbeaugazon.data.repository.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserDao {

    @Query("SELECT * FROM user LIMIT 1")
    fun getUser(): User

    @Insert
    fun insertUser(user: User)


}
