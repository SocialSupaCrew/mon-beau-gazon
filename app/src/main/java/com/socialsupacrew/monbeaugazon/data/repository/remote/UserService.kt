package com.socialsupacrew.monbeaugazon.data.repository.remote

import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface UserService {
    @POST("/user/signIn")
    fun signIn(@Body json: SignInUserBody): Single<SignInUserJson>

    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("/user")
    fun getUser(): Single<UserJson>

    @Headers(HeaderAnnotations.AUTHENTICATED)
    @GET("/user/dashboard")
    fun getDashBoard(): Single<DashboardJson>
}

data class SignInUserBody(
    @SerializedName("email") val mail: String,
    @SerializedName("password") val password: String,
    @SerializedName("language") val language: String = "fr-FR"
)

data class SignInUserJson(
    @SerializedName("token") val token: String,
    @SerializedName("userId") val userId: String,
    @SerializedName("language") val language: String,
    @SerializedName("action") val action: String,
    @SerializedName("onboarded") val onboarded: Boolean,
    @SerializedName("oid") val oid: String,
    @SerializedName("createdAt") val createdAt: String
)

data class UserJson(
    @SerializedName("id") val id: String,
    @SerializedName("email") val email: String,
    @SerializedName("firstname") val firstname: String,
    @SerializedName("lastname") val lastname: String,
    @SerializedName("country") val country: String,
    @SerializedName("teamId") val teamId: Int,
    @SerializedName("optin_nl") val optin_nl: Int,
    @SerializedName("optin_partner") val optin_partner: Int,
    @SerializedName("jerseySkin") val jerseySkin: List<String>,
    @SerializedName("money") val money: Int,
    @SerializedName("onboarded") val onboarded: Boolean,
    @SerializedName("oid") val oid: String,
    @SerializedName("bounce") val bounce: Int,
    @SerializedName("optinNlMpp") val optinNlMpp: Int,
    @SerializedName("pushNlMpp") val pushNlMpp: Int,
    @SerializedName("onboardingStatus") val onboardingStatus: Int
)

data class DashboardJson(
    @SerializedName("data") val data: DashboardDataJson
)

data class DashboardDataJson(
    @SerializedName("follow") val follow: List<Any>,
    @SerializedName("leagues") val leagues: List<DashboardLeagueJson>
)

data class DashboardLeagueJson(
    @SerializedName("id") val id: String,
    @SerializedName("admin_mpg_user_id") val adminMpgUserId: String,
    @SerializedName("current_mpg_user") val currentMpgUser: String,
    @SerializedName("name") val name: String,
    @SerializedName("leagueStatus") val leagueStatus : Int,
    @SerializedName("championship") val championship: Int,
    @SerializedName("mode") val mode: Int,
    @SerializedName("teamStatus") val teamStatus : Int,
    @SerializedName("url") val url: String
)
