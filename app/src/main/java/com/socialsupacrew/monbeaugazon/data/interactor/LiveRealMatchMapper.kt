package com.socialsupacrew.monbeaugazon.data.interactor

import com.socialsupacrew.monbeaugazon.data.repository.entity.GoalInfo
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity.LiveRealMatchHeader
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity.TimelineHeader
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity.TimelineItem
import com.socialsupacrew.monbeaugazon.data.repository.entity.SIDE_AWAY
import com.socialsupacrew.monbeaugazon.data.repository.entity.SIDE_HOME
import com.socialsupacrew.monbeaugazon.data.repository.entity.TeamInfo
import com.socialsupacrew.monbeaugazon.data.repository.entity.TimelineEventType
import com.socialsupacrew.monbeaugazon.data.repository.remote.ChampionshipMatchJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.ChampionshipMatchTimelineEvent
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

class LiveRealMatchMapper {

    fun map(from: ChampionshipMatchJson): List<LiveRealMatchEntity> {
        val items: MutableList<LiveRealMatchEntity> = mutableListOf()

        val timelineEvents = computeTimelineEvents(from.id, from.matchData.timeline)

        val homeGoals = getGoals(timelineEvents, SIDE_HOME)
        val awayGoals = getGoals(timelineEvents, SIDE_AWAY)

        val homeTeam = TeamInfo(from.home.club, from.home.id, from.home.score.toInt(), homeGoals)
        val awayTeam = TeamInfo(from.away.club, from.away.id, from.away.score.toInt(), awayGoals)

        val formattedDate = getFormattedDateMatch(from.dateMatch)

        val header = LiveRealMatchHeader(from.id, homeTeam, awayTeam, formattedDate)
        items.add(header)
        items.add(TimelineHeader)
        items.addAll(timelineEvents)
        return items
    }

    private fun computeTimelineEvents(
        matchId: String,
        timeline: List<ChampionshipMatchTimelineEvent>
    ): List<TimelineItem> {
        val timelineItems = mutableListOf<TimelineItem>()
        timeline.forEach {
            val type = TimelineEventType.getByName(it.type)
                ?: throw IllegalArgumentException("matchId($matchId): Unknown type of timeline event: ${it.type}")
            val playerName = when (type) {
                TimelineEventType.SUBSTITUTION -> it.subOnLastname!!
                else -> it.lastname!!
            }
            val secondPlayerName = when (type) {
                TimelineEventType.GOAL -> it.assistLastname
                TimelineEventType.SUBSTITUTION -> it.subOffLastname
                else -> null
            }

            val reason = when (type) {
                TimelineEventType.SUBSTITUTION -> it.reason
                else -> null
            }

            timelineItems.add(
                TimelineItem(
                    it.hashCode().toString(),
                    it.side,
                    "${it.time}'",
                    type,
                    playerName,
                    secondPlayerName,
                    reason,
                    type.details,
                    type == TimelineEventType.GOAL && !it.assistLastname.isNullOrEmpty(),
                    type == TimelineEventType.VAR
                )
            )
        }
        return timelineItems
    }

    private fun getGoals(timelineEvent: List<TimelineItem>, side: String): List<GoalInfo> {
        val goalsByPlayers = timelineEvent
            .filter { it.type == TimelineEventType.GOAL || it.type == TimelineEventType.OWN }
            .filter { it.side == side }
            .groupBy { it.playerName }

        val goalInfo = mutableListOf<GoalInfo>()
        goalsByPlayers.forEach { entry ->
            val player = GoalInfo(
                entry.key,
                entry.value.count { it.type == TimelineEventType.GOAL },
                entry.value.count { it.type == TimelineEventType.OWN }
            )
            goalInfo.add(player)
        }
        return goalInfo
    }

    private fun getFormattedDateMatch(dateMatch: LocalDateTime): String {
        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern(DATE_MATCH_PATTERN)
        return dateMatch.format(formatter)
    }

    companion object {
        private const val DATE_MATCH_PATTERN = "EEE d MMM - HH:mm"
    }
}
