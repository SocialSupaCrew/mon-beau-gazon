package com.socialsupacrew.monbeaugazon.data.interactor

import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveMpgEntity
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveLeagueJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveMatches
import com.socialsupacrew.monbeaugazon.data.repository.remote.LivePlayer

class LiveMpgMapper {

    fun map(from: List<LiveLeagueJson>): List<LiveMpgEntity> {
        val entityList = mutableListOf<LiveMpgEntity>()
        from.forEach {
            entityList.add(
                LiveMpgEntity.HeaderLive(
                    it.id,
                    it.name
                )
            )
            it.matches.forEach { liveMatches ->
                entityList.add(toMatchEntity(it.id, liveMatches))
            }
        }
        entityList.add(LiveMpgEntity.FooterLive)
        return entityList
    }

    private fun toMatchEntity(leagueId: String, from: LiveMatches): LiveMpgEntity.MatchLive {
        val scoreHome = computeHomeScore(from)
        val playersOwnGoalForHome = from.away.player?.filter { it.ownGoals > 0 } ?: emptyList()
        val playersForHome = from.home.player?.filterNot { it.ownGoals > 0 } ?: emptyList()
        val totalPlayersHome = (playersForHome + playersOwnGoalForHome).map { toPlayerEntity(it) }
        val teamHome = LiveMpgEntity.TeamLive(
            from.home.name,
            scoreHome,
            totalPlayersHome
        )

        val scoreAway = computeAwayScore(from)
        val playersOwnGoalForAway = from.home.player?.filter { it.ownGoals > 0 } ?: emptyList()
        val playersForAway = from.away.player?.filterNot { it.ownGoals > 0 } ?: emptyList()
        val totalPlayersAway = (playersForAway + playersOwnGoalForAway).map { toPlayerEntity(it) }
        val teamAway = LiveMpgEntity.TeamLive(
            from.away.name,
            scoreAway,
            totalPlayersAway
        )

        return LiveMpgEntity.MatchLive(
            from.id,
            leagueId,
            teamHome,
            teamAway,
            false
        )
    }

    private fun computeHomeScore(match: LiveMatches): Int {
        return (match.home.player?.sumBy { it.goals } ?: 0) +
                (match.away.player?.sumBy { it.ownGoals } ?: 0)
    }

    private fun computeAwayScore(match: LiveMatches): Int {
        return (match.away.player?.sumBy { it.goals } ?: 0) +
                (match.home.player?.sumBy { it.ownGoals } ?: 0)
    }

    private fun toPlayerEntity(from: LivePlayer): LiveMpgEntity.PlayerLive {
        return LiveMpgEntity.PlayerLive(
            from.lastName,
            from.ownGoals,
            from.goals
        )
    }
}
