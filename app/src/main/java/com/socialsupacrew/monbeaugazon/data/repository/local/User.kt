package com.socialsupacrew.monbeaugazon.data.repository.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class User(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "email") val email: String,
    @ColumnInfo(name = "firstname") val firstname: String,
    @ColumnInfo(name = "lastname") val lastname: String,
    @ColumnInfo(name = "country") val country: String,
    @ColumnInfo(name = "teamId") val teamId: Int,
    @ColumnInfo(name = "optin_nl") val optinNl: Int,
    @ColumnInfo(name = "optin_partner") val optinPartner: Int,
    @ColumnInfo(name = "jerseySkin") val jerseySkin: String,
    @ColumnInfo(name = "money") val money: Int,
    @ColumnInfo(name = "onboarded") val onboarded: Boolean,
    @ColumnInfo(name = "oid") val oid: String,
    @ColumnInfo(name = "bounce") val bounce: Int,
    @ColumnInfo(name = "optinNlMpp") val optinNlMpp: Int,
    @ColumnInfo(name = "pushNlMpp") val pushNlMpp: Int,
    @ColumnInfo(name = "onboardingStatus") val onboardingStatus: Int
)

