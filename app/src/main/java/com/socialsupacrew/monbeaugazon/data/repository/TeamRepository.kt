package com.socialsupacrew.monbeaugazon.data.repository

import android.util.Log
import com.socialsupacrew.monbeaugazon.data.repository.local.AppDatabase
import com.socialsupacrew.monbeaugazon.data.repository.local.Friend
import com.socialsupacrew.monbeaugazon.data.repository.local.Player
import com.socialsupacrew.monbeaugazon.data.repository.local.PlayerAndClub
import com.socialsupacrew.monbeaugazon.data.repository.remote.FriendJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.PlayerJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.TeamService
import com.socialsupacrew.monbeaugazon.data.repository.remote.TeamsJson
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class TeamRepository constructor(
    private val remoteDataSource: TeamRemoteDataSource,
    private val localDataSource: TeamLocalDataSource
) {
    fun syncTeams(leagueId: String) {
        remoteDataSource.fetchTeams(leagueId)
            .subscribeOn(Schedulers.io())
            .doOnSuccess { localDataSource.saveTeams(it.teams) }
            .subscribe({}, { Log.e(TAG, "error: ${it.message}") })
    }

    fun getTeam(leagueId: String): Observable<List<Friend>> {
        return localDataSource.getTeam(leagueId)
    }

    fun getPlayers(friendId: String, leagueId: String): Observable<List<PlayerAndClub>> {
        return localDataSource.getPlayers(friendId, leagueId)
    }

    companion object {
        private const val TAG = "TeamRepository"
    }
}

class TeamRemoteDataSource constructor(private val teamService: TeamService) {

    fun fetchTeams(leagueId: String): Single<TeamsJson> {
        return teamService.getTeams(leagueId)
    }
}

class TeamLocalDataSource constructor(private val database: AppDatabase) {

    fun saveTeams(result: Map<String, FriendJson>) {
        result.forEach { (_, friends) -> saveFriend(friends) }
    }

    private fun saveFriend(friend: FriendJson) {
        val leagueId = friend.id.substringBefore("$$").substringAfterLast("_")
        val friendId = friend.id.substringAfter("$$")
        val newFriend = Friend(
            friend.id,
            friend.name,
            friend.president,
            friend.stadium,
            leagueId
        )
        database.friendDao().insertFriend(newFriend)
        friend.players?.forEach { savePlayer(it, friend.id, leagueId) }
    }

    private fun savePlayer(
        player: PlayerJson,
        friendId: String,
        leagueId: String
    ) {
        val club = database.clubDao().getClubWithClubName(player.club)
        val fakeId = player.id + "$$" + leagueId
        val newPlayer = Player(
            fakeId,
            player.id,
            player.firstName,
            player.lastname,
            player.position,
            player.ultraPosition,
            player.pricePaid,
            friendId,
            leagueId,
            club.id
        )
        database.playerDao().insertPlayer(newPlayer)
    }

    fun getTeam(leagueId: String): Observable<List<Friend>> {
        return database.friendDao().getFriends(leagueId)
    }

    fun getPlayers(friendId: String, leagueId: String): Observable<List<PlayerAndClub>> {
        return database.playerDao().getPlayersForFriend(friendId, leagueId)
    }

    companion object {
        private const val TAG = "TeamLocalDataSource"
    }
}
