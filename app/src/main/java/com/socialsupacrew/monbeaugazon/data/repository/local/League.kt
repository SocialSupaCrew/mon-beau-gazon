package com.socialsupacrew.monbeaugazon.data.repository.local

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.socialsupacrew.monbeaugazon.R

@Entity(tableName = "league")
data class League(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "adminMpgUserId") val adminMpgUserId: String,
    @ColumnInfo(name = "currentMpgUser") val currentMpgUser: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "leagueStatus") val leagueStatus: Int,
    @ColumnInfo(name = "championship") val championship: Int,
    @ColumnInfo(name = "mode") val mode: Int,
    @ColumnInfo(name = "teamStatus") val teamStatus: Int,
    @ColumnInfo(name = "url") val url: String
)

enum class LeagueStatus(val statusId: Int) {
    PREPARING(1),
    UNKNOWN(2),
    MERCATO(3),
    IN_PROGRESS(4),
    FINISHED(5),
    MERCATO_RELAUNCH(7);
}

enum class Championship(val id: Int, @DrawableRes val flag: Int, @StringRes val championshipName: Int) {
    MPG(0, R.drawable.ic_outline_whatshot_24, R.string.championship_name_mpg),
    FRANCE(1, R.drawable.flag_fr, R.string.championship_name_fr),
    ENGLAND(2, R.drawable.flag_en, R.string.championship_name_en),
    SPAIN(3, R.drawable.flag_sp, R.string.championship_name_sp),
    FRANCE_2(4, R.drawable.flag_fr_2, R.string.championship_name_fr_2),
    ITALY(5, R.drawable.flag_it, R.string.championship_name_it);

    companion object {
        fun getChampionshipById(id: Int): Championship {
            return values().first { it.id == id }
        }
    }
}
