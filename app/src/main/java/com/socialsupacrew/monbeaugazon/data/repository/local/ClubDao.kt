package com.socialsupacrew.monbeaugazon.data.repository.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single

@Dao
interface ClubDao {
    @Query("SELECT * FROM club")
    fun getClubs(): Single<List<Club>>

    @Query("SELECT * FROM club WHERE name=:clubName LIMIT 1")
    fun getClubWithClubName(clubName: String): Club

    @Query("SELECT * FROM club WHERE id=:id LIMIT 1")
    fun getClubById(id: String): Club

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertClub(club: Club)
}
