package com.socialsupacrew.monbeaugazon.data.repository.remote

import android.util.Log
import com.socialsupacrew.monbeaugazon.Entry
import com.socialsupacrew.monbeaugazon.Preferences
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class RetrofitInterceptor constructor(private val preferences: Preferences) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val requestBuilder = request.newBuilder()
            .header("Accept", "application/json")
            .header("client-version", CLIENT_VERSION)
            .header("Content-Type", "application/json")
            .header("DNT", "1")
            .header("fromhost", "mpg")
            .header("Origin", "https://mpg.football")
            .header("platform", "web")
            .header("Referer", "https://mpg.football")
            .header("User-Agent:", USER_AGENT)

        val headerAnnotations = HeaderAnnotations.getCustomAnnotation(request)
        addAuthorizationHeader(requestBuilder, request.url, headerAnnotations)

        request = requestBuilder.build()

        return chain.proceed(request)
    }

    private fun addAuthorizationHeader(
        requestBuilder: Request.Builder,
        url: HttpUrl,
        headerAnnotations: List<String>
    ) {
        if (!headerAnnotations.contains(HeaderAnnotations.AUTHENTICATED_VALUE)) {
            return
        }
        requestBuilder.removeHeader(HeaderAnnotations.HEADER_NAME)

        val token = preferences.getString(Entry.TOKEN)
        if (token == null) {
            if (headerAnnotations.contains(HeaderAnnotations.AUTHENTICATED_VALUE)) {
                throw MissingAuthorizationException(url)
            }
            Log.e(TAG, "Null token: ${url.encodedPath}")
            return
        }
        requestBuilder.header("Authorization", token)
    }

    companion object {
        private const val TAG = "RetrofitInterceptor"

        const val CLIENT_VERSION = "6.9.1"
        const val USER_AGENT =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36"
    }

    class MissingAuthorizationException(url: HttpUrl) :
        RuntimeException("authorization token needed: ${url.encodedPath}")
}
