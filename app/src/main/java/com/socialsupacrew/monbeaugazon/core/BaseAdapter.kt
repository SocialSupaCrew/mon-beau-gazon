package com.socialsupacrew.monbeaugazon.core

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T, VH : RecyclerView.ViewHolder>(
    diffCallback: DiffUtil.ItemCallback<T>
) : ListAdapter<T, VH>(diffCallback) {

    var items: List<T> = emptyList()
        set(value) {
            submitList(value)
            field = value
        }

    override fun getItemCount(): Int = items.size
}
