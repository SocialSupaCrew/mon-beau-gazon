package com.socialsupacrew.monbeaugazon.core

import androidx.viewpager.widget.ViewPager

abstract class OnViewPagerPageChangeListener: ViewPager.OnPageChangeListener {
    override fun onPageScrollStateChanged(state: Int) {
        // Do nothing
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        // Do nothing
    }

    override fun onPageSelected(position: Int) {
        // Do nothing
    }
}
