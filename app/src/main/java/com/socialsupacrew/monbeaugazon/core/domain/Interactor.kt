package com.socialsupacrew.monbeaugazon.core.domain

import androidx.lifecycle.LiveData

interface Interactor<T> {

    fun getLiveData(): LiveData<T>

    fun cleanUp()
}
