package com.socialsupacrew.monbeaugazon

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.socialsupacrew.monbeaugazon.ui.dashboard.DashboardActivity
import com.socialsupacrew.monbeaugazon.ui.signin.SignInActivity
import org.koin.android.ext.android.inject

class LauncherActivity : AppCompatActivity() {

    private val preferences: Preferences by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val token = preferences.getString(Entry.TOKEN)

        startActivity(
            if (token == null) {
                SignInActivity.newIntent(this)
            } else {
                DashboardActivity.newIntent(this)
            }
        )
        finish()
    }
}
