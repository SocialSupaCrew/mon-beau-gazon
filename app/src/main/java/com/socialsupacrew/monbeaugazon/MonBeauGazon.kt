package com.socialsupacrew.monbeaugazon

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MonBeauGazon : Application() {
    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)

        startKoin {
            androidLogger()
            androidContext(this@MonBeauGazon)
            androidFileProperties()
            modules(listOf(viewModelModule, dataModule, networkModule))
        }
    }
}
