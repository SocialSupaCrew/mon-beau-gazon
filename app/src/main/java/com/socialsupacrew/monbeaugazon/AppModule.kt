package com.socialsupacrew.monbeaugazon

import android.app.Application
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.socialsupacrew.monbeaugazon.data.interactor.GetEmptyLiveMpgInteractor
import com.socialsupacrew.monbeaugazon.data.interactor.GetEmptyLiveMpgInteractorImpl
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveMpgInteractor
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveMpgInteractorImpl
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveRankingInteractor
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveRankingInteractorImpl
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveRealMatchInteractor
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveRealMatchInteractorImpl
import com.socialsupacrew.monbeaugazon.data.interactor.LiveMpgMapper
import com.socialsupacrew.monbeaugazon.data.interactor.LiveRankingMapper
import com.socialsupacrew.monbeaugazon.data.interactor.LiveRealMatchMapper
import com.socialsupacrew.monbeaugazon.data.repository.ClubLocalDataSource
import com.socialsupacrew.monbeaugazon.data.repository.ClubRemoteDataSource
import com.socialsupacrew.monbeaugazon.data.repository.ClubRepository
import com.socialsupacrew.monbeaugazon.data.repository.LeagueLocalDataSource
import com.socialsupacrew.monbeaugazon.data.repository.LeagueRemoteDataSource
import com.socialsupacrew.monbeaugazon.data.repository.LeagueRepository
import com.socialsupacrew.monbeaugazon.data.repository.LiveRemoteDataSource
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository
import com.socialsupacrew.monbeaugazon.data.repository.TeamLocalDataSource
import com.socialsupacrew.monbeaugazon.data.repository.TeamRemoteDataSource
import com.socialsupacrew.monbeaugazon.data.repository.TeamRepository
import com.socialsupacrew.monbeaugazon.data.repository.UserLocalDataSource
import com.socialsupacrew.monbeaugazon.data.repository.UserRemoteDataSource
import com.socialsupacrew.monbeaugazon.data.repository.UserRepository
import com.socialsupacrew.monbeaugazon.data.repository.local.AppDatabase
import com.socialsupacrew.monbeaugazon.data.repository.remote.ClubService
import com.socialsupacrew.monbeaugazon.data.repository.remote.LeagueService
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveService
import com.socialsupacrew.monbeaugazon.data.repository.remote.LocalDateTimeConverter
import com.socialsupacrew.monbeaugazon.data.repository.remote.RetrofitInterceptor
import com.socialsupacrew.monbeaugazon.data.repository.remote.TeamService
import com.socialsupacrew.monbeaugazon.data.repository.remote.UserService
import com.socialsupacrew.monbeaugazon.ui.live.LiveMpgViewModel
import com.socialsupacrew.monbeaugazon.ui.live.LiveMpgViewModelImpl
import com.socialsupacrew.monbeaugazon.ui.live.ranking.LiveRankingViewModel
import com.socialsupacrew.monbeaugazon.ui.live.ranking.LiveRankingViewModelImpl
import com.socialsupacrew.monbeaugazon.ui.live.realmatch.LiveRealMatchViewModel
import com.socialsupacrew.monbeaugazon.ui.live.realmatch.LiveRealMatchViewModelImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import org.threeten.bp.LocalDateTime
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL = "https://api.monpetitgazon.com"

val viewModelModule = module {
    viewModel<LiveRankingViewModel> { (championshipId: Int) ->
        LiveRankingViewModelImpl(MediatorLiveData(), MutableLiveData(), get(), championshipId)
    }
    viewModel<LiveMpgViewModel> {
        LiveMpgViewModelImpl(MediatorLiveData(), MutableLiveData(), get(), get())
    }
    viewModel<LiveRealMatchViewModel> { (matchId: String) ->
        LiveRealMatchViewModelImpl(MediatorLiveData(), MutableLiveData(), get(), matchId)
    }
}

val dataModule = module {
    factory { LiveRankingMapper() }
    factory { LiveMpgMapper() }
    factory { LiveRealMatchMapper() }

    factory<GetLiveRankingInteractor> { GetLiveRankingInteractorImpl(get(), get()) }
    factory<GetLiveMpgInteractor> { GetLiveMpgInteractorImpl(get(), get()) }
    factory<GetEmptyLiveMpgInteractor> { GetEmptyLiveMpgInteractorImpl(get()) }
    factory<GetLiveRealMatchInteractor> { GetLiveRealMatchInteractorImpl(get(), get()) }

    single { UserRepository(get(), get()) }
    single { LeagueRepository(get(), get()) }
    single { TeamRepository(get(), get()) }
    single { ClubRepository(get(), get()) }

    single { UserRemoteDataSource(get()) }
    single { UserLocalDataSource(get(), get()) }

    single { LeagueRemoteDataSource(get()) }
    single { LeagueLocalDataSource(get()) }

    single { TeamRemoteDataSource(get()) }
    single { TeamLocalDataSource(get()) }
    single { LiveRepository(get(), get()) }
    single { LiveRemoteDataSource(get(), get()) }

    single { ClubRemoteDataSource(get()) }
    single { ClubLocalDataSource(get()) }

    single { Preferences(get()) }
    single { roomDataBase(get()) }
}

val networkModule = module {
    single { userService(get()) }
    single { leagueService(get()) }
    single { clubService(get()) }
    single { teamService(get()) }
    single { liveService(get()) }

    single { retrofit(get(), get()) }
    single { httpClient(get(), get()) }
    single { retrofitInterceptor(get()) }
    single { httpLoggingInterceptor(get()) }
    single { httpLoggingLevel() }
    single { gson() }
}

private fun roomDataBase(application: Application) =
    Room.databaseBuilder(application, AppDatabase::class.java, AppDatabase.DATABASE_NAME).build()

private fun retrofit(client: OkHttpClient, gson: Gson) = Retrofit.Builder()
    .client(client)
    .baseUrl(BASE_URL)
    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    .addConverterFactory(GsonConverterFactory.create(gson))
    .build()

private fun httpClient(
    httpLoggingInterceptor: HttpLoggingInterceptor,
    retrofitInterceptor: RetrofitInterceptor
) =
    OkHttpClient.Builder()
        .addInterceptor(retrofitInterceptor)
        .addInterceptor(httpLoggingInterceptor)
        .build()

private fun retrofitInterceptor(preferences: Preferences) = RetrofitInterceptor(preferences)

private fun httpLoggingInterceptor(level: HttpLoggingInterceptor.Level) =
    HttpLoggingInterceptor().also { it.level = level }

private fun httpLoggingLevel(): HttpLoggingInterceptor.Level =
    if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

private fun gson(): Gson = GsonBuilder().let {
    it.registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeConverter())
    it.create()
}

private fun userService(retrofit: Retrofit) = retrofit.create(UserService::class.java)
private fun leagueService(retrofit: Retrofit) = retrofit.create(LeagueService::class.java)
private fun teamService(retrofit: Retrofit) = retrofit.create(TeamService::class.java)
private fun liveService(retrofit: Retrofit) = retrofit.create(LiveService::class.java)
private fun clubService(retrofit: Retrofit) = retrofit.create(ClubService::class.java)
