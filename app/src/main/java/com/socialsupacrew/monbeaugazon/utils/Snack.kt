package com.socialsupacrew.monbeaugazon.utils

import android.view.View
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar

object Snack {

    fun displayError(view: View, @StringRes resId: Int) {
        displayError(view, view.context.getString(resId))
    }

    private fun displayError(view: View, text: String) {
        display(view, text)
    }

    fun displaySuccess(view: View, @StringRes resId: Int) {
        displaySuccess(view, view.context.getString(resId))
    }

    private fun displaySuccess(view: View, text: String) {
        display(view, text)
    }

    private fun display(view: View?, text: String) {
        view?.let {
            Snackbar.make(it, text, Snackbar.LENGTH_LONG).show()
        }
    }
}
