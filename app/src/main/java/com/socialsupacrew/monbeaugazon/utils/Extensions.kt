package com.socialsupacrew.monbeaugazon.utils

import android.app.Activity
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.icu.text.MessageFormat
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ImageView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView

fun <T : View> Activity.bindView(id: Int) = lazy(LazyThreadSafetyMode.NONE) { findViewById<T>(id) }

fun <T : View> Activity.bindViews(ids: List<Int>) = lazy(LazyThreadSafetyMode.NONE) {
    ids.map { findViewById<T>(it) }
}

fun <T : View> Activity.bindViews(vararg ids: Int) = lazy(LazyThreadSafetyMode.NONE) {
    ids.map { findViewById<T>(it) }
}

fun <T : View> Fragment.bindView(id: Int) = lazy(LazyThreadSafetyMode.NONE) {
    view!!.findViewById<T>(id)
}

fun <T : View> View.bindView(id: Int) = lazy(LazyThreadSafetyMode.NONE) {
    findViewById<T>(id)
}

fun <T : View> ViewGroup.bindView(id: Int) = lazy(LazyThreadSafetyMode.NONE) {
    findViewById<T>(id)
}

fun <T : View> RecyclerView.ViewHolder.bindView(id: Int) = lazy(LazyThreadSafetyMode.NONE) {
    itemView.findViewById<T>(id)
}

val Int.toPx: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

val Int.toDp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()

fun Number?.getOrdinal(): String? {
    if (this == null) return null

    val format = "{0,ordinal}"

    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        MessageFormat.format(format, this)
    } else {
        this.toString()
    }
}

fun Drawable.toBitmap(): Bitmap {
    if (this is BitmapDrawable) {
        return this.bitmap
    }

    val bitmap =
        Bitmap.createBitmap(this.intrinsicWidth, this.intrinsicHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    this.setBounds(0, 0, canvas.width, canvas.height)
    this.draw(canvas)

    return bitmap
}

fun ImageView.setColor(@ColorRes color: Int) {
    setColorFilter(ContextCompat.getColor(context, color))
}

inline fun View.doOnGlobalLayout(crossinline action: (view: View) -> Unit) {
    val vto = viewTreeObserver
    vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            action(this@doOnGlobalLayout)
            when {
                vto.isAlive -> vto.removeOnGlobalLayoutListener(this)
                else -> viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        }
    })
}
