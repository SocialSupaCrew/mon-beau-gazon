package com.socialsupacrew.monbeaugazon.utils

/**
 * Calls the specified function [block] if [Any] is null.
 */
infix fun Any?.ifNull(block: () -> Unit) {
    if (this == null) block()
}
