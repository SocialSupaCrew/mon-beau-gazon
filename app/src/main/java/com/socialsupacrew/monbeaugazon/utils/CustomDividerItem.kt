package com.socialsupacrew.monbeaugazon.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.ui.live.realmatch.LiveRealMatchAdapter

class CustomDividerItem(
    context: Context,
    private val nbHeader: Int = 0
) : RecyclerView.ItemDecoration() {

    private val divider = ContextCompat.getDrawable(context, R.drawable.list_divider)!!

//    init {
//        ContextCompat.getDrawable(context, R.drawable.list_divider)?.let { setDrawable(it) }
//    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        parent.adapter?.let { adapter ->
            val childAdapterPosition = parent.getChildAdapterPosition(view)
                .let { if (it == RecyclerView.NO_POSITION) return else it }

            when (adapter.getItemViewType(childAdapterPosition)) {
                LiveRealMatchAdapter.ViewType.TIMELINE_ITEM.id -> super.getItemOffsets(
                    outRect,
                    view,
                    parent,
                    state
                )
                else -> outRect.setEmpty()
            }

            if (childAdapterPosition == adapter.itemCount - 1) {
                outRect.setEmpty()
            }
        }

//        val position = parent.getChildAdapterPosition(view)
//        // hide the divider for headers and the last child
//        parent.adapter?.let {
//
//            when (it.getItemViewType(position)) {
//                LiveRealMatchAdapter.ViewType.HEADER.id,
//                LiveRealMatchAdapter.ViewType.TIMELINE_HEADER.id -> {
//                    outRect.setEmpty()
//                    return@let
//                }
//                LiveRealMatchAdapter.ViewType.TIMELINE_ITEM.id -> {
//                    // Continue
//                }
//                else -> {
//                    // Continue
//                }
//            }
//
//            when (position) {
//                it.itemCount - 1 -> outRect.setEmpty()
//                else -> super.getItemOffsets(outRect, view, parent, state)
//            }
//        }
    }

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        parent.adapter?.let { adapter ->
            parent.children.forEach { view ->
                val childAdapterPosition = parent.getChildAdapterPosition(view)
                    .let { if (it == RecyclerView.NO_POSITION) return else it }

                when (adapter.getItemViewType(childAdapterPosition)) {
                    LiveRealMatchAdapter.ViewType.TIMELINE_ITEM.id ->
                        divider.drawSeparator(view, parent, canvas)
                    else -> Unit
                }
            }
        }
    }

    private fun Drawable.drawSeparator(view: View, parent: RecyclerView, canvas: Canvas) =
        apply {
            val left = view.right
            val top = parent.paddingTop
            val right = left + intrinsicWidth
            val bottom = top + intrinsicHeight - parent.paddingBottom
            bounds = Rect(left, top, right, bottom)
            draw(canvas)
        }
}
