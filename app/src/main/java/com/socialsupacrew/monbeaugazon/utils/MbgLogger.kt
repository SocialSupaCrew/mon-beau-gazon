package com.socialsupacrew.monbeaugazon.utils

import com.google.firebase.crashlytics.FirebaseCrashlytics

object MbgLogger {
    fun debug(message: String) {
        MbgCrashlytics.log(message)
    }

    fun error(throwable: Throwable) {
        MbgCrashlytics.logException(throwable)
    }
}

private class MbgCrashlytics {
    companion object {
        internal fun log(message: String) {
            FirebaseCrashlytics.getInstance().log(message)
        }

        internal fun logException(throwable: Throwable) {
            FirebaseCrashlytics.getInstance().recordException(throwable)
        }
    }
}
