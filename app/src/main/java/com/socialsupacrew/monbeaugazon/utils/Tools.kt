package com.socialsupacrew.monbeaugazon.utils

import android.view.KeyEvent
import android.view.inputmethod.EditorInfo

fun isEnterEvent(actionId: Int, event: KeyEvent?): Boolean {
    return actionId == EditorInfo.IME_ACTION_DONE
            || event != null && event.action == KeyEvent.ACTION_DOWN && event.keyCode == KeyEvent.KEYCODE_ENTER
}
