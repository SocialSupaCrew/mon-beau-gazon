package com.socialsupacrew.monbeaugazon.design

import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.socialsupacrew.monbeaugazon.R

fun AppCompatActivity.setupToolbar(
    toolbar: Toolbar,
    @StringRes title: Int = R.string.app_name,
    showHome: Boolean = false
) {
    setupToolbar(toolbar, getString(title), showHome)
}

fun AppCompatActivity.setupToolbar(
    toolbar: Toolbar,
    title: String,
    showHome: Boolean = false
) {
    toolbar.title = title
    setSupportActionBar(toolbar)
    supportActionBar?.run {
        setDisplayHomeAsUpEnabled(showHome)
    }
}
