package com.socialsupacrew.monbeaugazon.ui.live

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.Group
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.core.BaseActivity
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveMatchPlayerJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveMatchTeamJson
import com.socialsupacrew.monbeaugazon.design.setupToolbar
import com.socialsupacrew.monbeaugazon.ui.component.PlayersRow
import com.socialsupacrew.monbeaugazon.utils.bindView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject

class LiveMatchActivity : BaseActivity() {

    private val liveRepository: LiveRepository by inject()

    private var liveDisposable: Disposable? = null

    private val toolbar: Toolbar by bindView(R.id.toolbar_live_match)
    private val loading: ProgressBar by bindView(R.id.loading)
    private val nameHome: TextView by bindView(R.id.name_home)
    private val nameAway: TextView by bindView(R.id.name_away)
    private val scoreHome: TextView by bindView(R.id.score_home)
    private val scoreAway: TextView by bindView(R.id.score_away)
    private val fieldGroup: Group by bindView(R.id.live_group)
    private val homeBenchRow: PlayersRow by bindView(R.id.home_bench_row)
    private val homeGoalkeeperRow: PlayersRow by bindView(R.id.home_goalkeeper_row)
    private val homeDefendersRow: PlayersRow by bindView(R.id.home_defenders_row)
    private val homeMidfieldersRow: PlayersRow by bindView(R.id.home_midfielders_row)
    private val homeForwardsRow: PlayersRow by bindView(R.id.home_forwards_row)
    private val awayForwardsRow: PlayersRow by bindView(R.id.away_forwards_row)
    private val awayMidfieldersRow: PlayersRow by bindView(R.id.away_midfielders_row)
    private val awayDefendersRow: PlayersRow by bindView(R.id.away_defenders_row)
    private val awayGoalkeeperRow: PlayersRow by bindView(R.id.away_goalkeeper_row)
    private val awayBenchRow: PlayersRow by bindView(R.id.away_bench_row)

    private val leagueId: String by lazy { intent.getStringExtra(EXTRA_LEAGUE_ID) ?: "" }
    private val matchId: String by lazy { intent.getStringExtra(EXTRA_MATCH_ID) ?: "" }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_match)

        setupToolbar(toolbar, showHome = true)

        startLoading()
        getLiveMatch()
    }

    private fun getLiveMatch() {
        liveDisposable?.dispose()
        liveDisposable = liveRepository.getLiveMatch(leagueId, matchId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d(TAG, it.toString())
                stopLoading()
                displayResult(it.teamHome, it.teamAway)
                displayPlayers(
                    Composition.getById(it.teamHome.composition),
                    it.players.home,
                    Composition.getById(it.teamAway.composition),
                    it.players.away
                )
            }, {
                Log.e(TAG, "error: ${it.message}")
            })
    }

    private fun startLoading() {
        loading.visibility = VISIBLE
        fieldGroup.visibility = GONE
    }

    private fun stopLoading() {
        loading.visibility = GONE
        fieldGroup.visibility = VISIBLE
    }

    private fun displayResult(
        teamHome: LiveMatchTeamJson,
        teamAway: LiveMatchTeamJson
    ) {
        nameHome.text = teamHome.name
        nameAway.text = teamAway.name
        scoreHome.text = teamHome.score.toString()
        scoreAway.text = teamAway.score.toString()
    }

    private fun displayPlayers(
        homeCompo: Composition,
        homePlayers: List<LiveMatchPlayerJson>,
        awayCompo: Composition,
        awayPlayers: List<LiveMatchPlayerJson>
    ) {
        homeBenchRow.addPlayers(getBench(homePlayers), home = true, isBench = true)
        homeGoalkeeperRow.addPlayers(getGoalkeeper(homePlayers), true)
        homeDefendersRow.addPlayers(getDefenders(homePlayers, homeCompo), true)
        homeMidfieldersRow.addPlayers(getMidfielders(homePlayers, homeCompo), true)
        homeForwardsRow.addPlayers(getForwards(homePlayers, homeCompo), true)
        awayForwardsRow.addPlayers(getForwards(awayPlayers, awayCompo), false)
        awayMidfieldersRow.addPlayers(getMidfielders(awayPlayers, awayCompo), false)
        awayDefendersRow.addPlayers(getDefenders(awayPlayers, awayCompo), false)
        awayGoalkeeperRow.addPlayers(getGoalkeeper(awayPlayers), false)
        awayBenchRow.addPlayers(getBench(awayPlayers), home = true, isBench = true)
    }

    private fun getBench(players: List<LiveMatchPlayerJson>): List<LiveMatchPlayerJson> {
        return players.filter { it.number.toInt() > 11 && it.name != null}
    }

    private fun getGoalkeeper(players: List<LiveMatchPlayerJson>): List<LiveMatchPlayerJson> {
        return players.take(1)
    }

    private fun getDefenders(
        players: List<LiveMatchPlayerJson>,
        compo: Composition
    ): List<LiveMatchPlayerJson> {
        return players.filter { it.position == 2 }.take(compo.defenders)
    }

    private fun getMidfielders(
        players: List<LiveMatchPlayerJson>,
        compo: Composition
    ): List<LiveMatchPlayerJson> {
        return players.filter { it.position == 3 }.take(compo.midfielders)
    }

    private fun getForwards(
        players: List<LiveMatchPlayerJson>,
        compo: Composition
    ): List<LiveMatchPlayerJson> {
        return players.filter { it.position == 4 }.take(compo.forwards)
    }

    override fun onDestroy() {
        liveDisposable?.dispose()
        super.onDestroy()
    }

    companion object {
        private const val TAG = "LiveActivity"
        private const val EXTRA_LEAGUE_ID = "extra:leagueId"
        private const val EXTRA_MATCH_ID = "extra:matchId"

        fun newIntent(context: Context, leagueId: String, matchId: String): Intent {
            return Intent(context, LiveMatchActivity::class.java).apply {
                putExtra(EXTRA_LEAGUE_ID, leagueId)
                putExtra(EXTRA_MATCH_ID, matchId)
            }
        }
    }

    @Suppress("unused")
    enum class Composition(
        val id: Int,
        val defenders: Int,
        val midfielders: Int,
        val forwards: Int
    ) {
        CQU(541, 5, 4, 1),
        CTD(532, 5, 3, 2),
        QCU(451, 4, 5, 1),
        QQD(442, 4, 4, 2),
        QTT(433, 4, 3, 3),
        TCD(352, 3, 5, 2),
        TQT(343, 3, 4, 3);

        companion object {
            fun getById(id: Int): Composition {
                for (value in values()) {
                    if (value.id == id)
                        return value
                }
                return QTT
            }
        }
    }
}
