package com.socialsupacrew.monbeaugazon.ui.league

import android.graphics.Typeface
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.remote.MatchesCalendar
import com.socialsupacrew.monbeaugazon.ui.component.Jersey
import com.socialsupacrew.monbeaugazon.utils.bindView

class CalendarHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val nameHome: TextView by bindView(R.id.name_home)
    private val nameAway: TextView by bindView(R.id.name_away)
    private val scoreHome: TextView by bindView(R.id.score_home)
    private val scoreAway: TextView by bindView(R.id.score_away)
    private val jerseyHome: Jersey by bindView(R.id.jersey_home)
    private val jerseyAway: Jersey by bindView(R.id.jersey_away)

    fun bindView(matches: MatchesCalendar) {
        resetNameAndScore()
        nameHome.text = matches.teamHome.name
        nameAway.text = matches.teamAway.name
        matches.teamHome.score?.let { scoreHome.text = it.toString() }
        matches.teamAway.score?.let { scoreAway.text = it.toString() }
        bindJersey(matches.teamHome.jerseyUrl, matches.teamAway.jerseyUrl)
        if (isMatchesOver(matches)) {
            bindNameStyle(matches.teamHome.score!!, matches.teamAway.score!!)
        }
    }

    private fun resetNameAndScore() {
        scoreHome.text = null
        scoreAway.text = null
        nameHome.setTextAppearance(R.style.TextAppearance_MBG_Subtitle2)
        nameAway.setTextAppearance(R.style.TextAppearance_MBG_Subtitle2)
    }

    private fun bindNameStyle(scoreHome: Int, scoreAway: Int) {
        when {
            homeHasWin(scoreHome, scoreAway) -> {
                nameHome.setTextAppearance(R.style.TextAppearance_MBG_Subtitle2_Bold)
            }
            awayHasWin(scoreHome, scoreAway) -> {
                nameAway.setTextAppearance(R.style.TextAppearance_MBG_Subtitle2_Bold)
            }
        }
    }

    private fun bindJersey(jerseyUrlHome: String, jerseyUrlAway: String) {
        jerseyHome.setUrl(jerseyUrlHome)
        jerseyAway.setUrl(jerseyUrlAway)
    }

    private fun isMatchesOver(matches: MatchesCalendar): Boolean {
        return matches.teamHome.score != null && matches.teamAway.score != null
    }

    private fun homeHasWin(scoreHome: Int, scoreAway: Int): Boolean {
        return scoreHome > scoreAway
    }

    private fun awayHasWin(scoreHome: Int, scoreAway: Int): Boolean {
        return scoreHome < scoreAway
    }

    companion object {
        private const val WHITE = "#ffffff"
        private const val WHITE_SHORT = "#fff"
        private const val BLACK = "#000000"
    }
}
