package com.socialsupacrew.monbeaugazon.ui.about

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.socialsupacrew.monbeaugazon.BuildConfig
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.core.BaseActivity
import com.socialsupacrew.monbeaugazon.design.setupToolbar
import com.socialsupacrew.monbeaugazon.utils.bindView

class AboutActivity : BaseActivity() {

    private val toolbar: Toolbar by bindView(R.id.toolbar_about)
    private val appVersion: TextView by bindView(R.id.app_version)
    private val txtFreepik: TextView by bindView(R.id.txt_freepik)
    private val txtEucalyp: TextView by bindView(R.id.txt_eucalyp)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        setupToolbar(toolbar, showHome = true)

        setupVersion()
        setupLink()
    }

    private fun setupVersion() {
        appVersion.text =
            getString(R.string.app_version, BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE)
    }

    private fun setupLink() {
        txtFreepik.paintFlags = txtFreepik.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        txtEucalyp.paintFlags = txtEucalyp.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        txtFreepik.setOnClickListener { launchWebLink(URL_FREEPIK) }
        txtEucalyp.setOnClickListener { launchWebLink(URL_EUCALYP) }
    }

    private fun launchWebLink(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(url)
        })
    }

    companion object {
        private const val URL_FREEPIK = "https://www.freepik.com/" // Llama
        private const val URL_EUCALYP = "https://www.flaticon.com/authors/eucalyp" // Shirt

        fun newIntent(context: Context): Intent {
            return Intent(context, AboutActivity::class.java)
        }
    }
}
