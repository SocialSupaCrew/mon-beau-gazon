package com.socialsupacrew.monbeaugazon.ui.live.ranking

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.core.BaseActivity
import com.socialsupacrew.monbeaugazon.core.EventPathObserver
import com.socialsupacrew.monbeaugazon.data.repository.local.Championship
import com.socialsupacrew.monbeaugazon.design.setupToolbar
import com.socialsupacrew.monbeaugazon.ui.live.ranking.LiveRankingViewModel.Path
import com.socialsupacrew.monbeaugazon.ui.live.ranking.LiveRankingViewModel.State
import com.socialsupacrew.monbeaugazon.utils.bindView
import kotlinx.android.synthetic.main.activity_live_ranking.rankingList
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class LiveRankingActivity : BaseActivity() {

    private val championshipId: Int by lazy { intent.getIntExtra(EXTRA_CHAMPIONSHIP_ID, 0) }

    private val viewModel: LiveRankingViewModel by viewModel { parametersOf(championshipId) }

    private val adapter: LiveRankingAdapter by lazy { LiveRankingAdapter(viewModel) }

    private val toolbar: Toolbar by bindView(R.id.toolbar_live_ranking)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_ranking)

        setupToolbar(
            toolbar,
            Championship.getChampionshipById(championshipId).championshipName,
            true
        )

        viewModel.getState().observe(owner = this) { onStateChanged(it) }
        viewModel.getNavigation().observe(this, EventPathObserver { navigateTo(it as Path) })
        viewModel.fetchTeams()

        rankingList.layoutManager = LinearLayoutManager(this)
        rankingList.adapter = adapter
    }

    private fun onStateChanged(state: State) = when (state) {
        is State.TeamsLoaded -> adapter.items = state.teams
        State.TeamsError -> showEmptyState()
    }

    private fun navigateTo(path: Path) = when (path) {
        is Path.TeamDetail -> {
            // Do nothing
        }
    }

    private fun showEmptyState() {

    }

    companion object {
        private const val EXTRA_CHAMPIONSHIP_ID = "extra:championshipId"

        fun newIntent(context: Context, championshipId: Int) =
            Intent(context, LiveRankingActivity::class.java).apply {
                putExtra(EXTRA_CHAMPIONSHIP_ID, championshipId)
            }
    }
}
