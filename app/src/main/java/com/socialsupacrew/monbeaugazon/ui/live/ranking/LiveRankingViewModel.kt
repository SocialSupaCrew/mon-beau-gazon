package com.socialsupacrew.monbeaugazon.ui.live.ranking

import com.socialsupacrew.monbeaugazon.core.BaseViewModel
import com.socialsupacrew.monbeaugazon.core.EventPath
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity
import com.socialsupacrew.monbeaugazon.ui.live.ranking.LiveRankingViewModel.Path
import com.socialsupacrew.monbeaugazon.ui.live.ranking.LiveRankingViewModel.State

abstract class LiveRankingViewModel : BaseViewModel<State, Path>(), LiveRankingHolder.Listener {

    sealed class State {
        data class TeamsLoaded(val teams: List<LiveRankingEntity>) : State()
        object TeamsError : State()
    }

    sealed class Path : EventPath.Path {
        data class TeamDetail(val teamId: Int) : Path()
    }

    abstract fun fetchTeams()
}
