package com.socialsupacrew.monbeaugazon.ui.live

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.Group
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.core.EventPathObserver
import com.socialsupacrew.monbeaugazon.data.repository.local.Championship
import com.socialsupacrew.monbeaugazon.ui.component.Countdown
import com.socialsupacrew.monbeaugazon.ui.live.LiveActivity.RecyclerViewListener
import com.socialsupacrew.monbeaugazon.ui.live.LiveMpgViewModel.Path
import com.socialsupacrew.monbeaugazon.utils.bindView
import io.reactivex.disposables.Disposable
import org.koin.android.viewmodel.ext.android.viewModel

class LiveMpgFragment : LiveFragment() {

    private val viewModel: LiveMpgViewModel by viewModel()

    private val adapter: LiveMpgAdapter by lazy { LiveMpgAdapter(viewModel) }

    private var liveDisposable: Disposable? = null
    private var emptyLiveDisposable: Disposable? = null

    private val liveList: RecyclerView by bindView(R.id.live_list)
    private val countdown: Countdown by bindView(R.id.countdown)
    private val emptyState: Group by bindView(R.id.empty_state)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_live_mpg, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getState().observe(this, ::onStateChange)
        viewModel.getNavigation().observe(this, EventPathObserver { navigateTo(it as Path) })

        val layoutManager = LinearLayoutManager(view.context)
        layoutManager.orientation = RecyclerView.VERTICAL
        liveList.layoutManager = layoutManager
        liveList.adapter = adapter
    }

    private fun onStateChange(state: LiveMpgViewModel.State) {
        return when (state) {
            is LiveMpgViewModel.State.LiveLoaded -> {
                stopRefreshing()
                emptyState.isGone = true
                liveList.isVisible = true
                adapter.items = state.teams
            }
            is LiveMpgViewModel.State.EmptyLiveLoaded -> {
                stopRefreshing()
                emptyState.isVisible = true
                liveList.isGone = true
                countdown.setTime(state.time) { viewModel.fetchLive() }
            }
            LiveMpgViewModel.State.LiveLoading -> {
                // TODO: Use something else to show loading state
                showRefreshing()
            }
            LiveMpgViewModel.State.LiveError -> stopRefreshing()
        }
    }

    private fun navigateTo(path: Path) = when (path) {
        is Path.MatchDetail -> {
            context?.let {
                startActivity(LiveMatchActivity.newIntent(it, path.leagueId, path.matchId))
            }
        }
    }

    override fun getLive() {
        viewModel.fetchLive()
    }

    override fun newInstance(
        championship: Championship,
        recyclerViewListener: RecyclerViewListener?
    ): LiveFragment {
        return LiveMpgFragment()
    }

    override fun onDestroy() {
        liveDisposable?.dispose()
        emptyLiveDisposable?.dispose()
        super.onDestroy()
    }

    companion object {
        private const val TAG = "LiveMpgFragment"
    }
}
