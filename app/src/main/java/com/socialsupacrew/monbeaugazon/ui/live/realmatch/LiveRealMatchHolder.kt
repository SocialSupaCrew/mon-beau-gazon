package com.socialsupacrew.monbeaugazon.ui.live.realmatch

import android.annotation.SuppressLint
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.TooltipCompat
import androidx.constraintlayout.widget.Group
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity.LiveRealMatchHeader
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity.TimelineHeader
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity.TimelineItem
import com.socialsupacrew.monbeaugazon.data.repository.entity.REASON_INJURY
import com.socialsupacrew.monbeaugazon.data.repository.entity.SIDE_AWAY
import com.socialsupacrew.monbeaugazon.data.repository.entity.SIDE_HOME
import com.socialsupacrew.monbeaugazon.ui.component.Jersey
import com.socialsupacrew.monbeaugazon.utils.bindView
import com.socialsupacrew.monbeaugazon.utils.setColor

sealed class LiveRealMatchHolder(view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bind(entity: LiveRealMatchEntity)
}

class LiveRealMatchHeaderHolder(view: View) : LiveRealMatchHolder(view) {

    private val jerseyHome: Jersey by bindView(R.id.jersey_home)
    private val jerseyAway: Jersey by bindView(R.id.jersey_away)
    private val scoreHome: TextView by bindView(R.id.score_home)
    private val scoreAway: TextView by bindView(R.id.score_away)
    private val shortNameHome: TextView by bindView(R.id.short_name_home)
    private val shortNameAway: TextView by bindView(R.id.short_name_away)
    private val matchDate: TextView by bindView(R.id.match_date)

    constructor(parent: ViewGroup) : this(
        LayoutInflater.from(parent.context).inflate(R.layout.live_real_match_header, parent, false)
    )

    override fun bind(entity: LiveRealMatchEntity) {
        entity as LiveRealMatchHeader

        jerseyHome.setClub(entity.homeTeam.clubId)
        jerseyAway.setClub(entity.awayTeam.clubId)

        TooltipCompat.setTooltipText(jerseyHome, entity.homeTeam.name)
        TooltipCompat.setTooltipText(jerseyAway, entity.awayTeam.name)

        scoreHome.text = entity.homeTeam.score.toString()
        scoreAway.text = entity.awayTeam.score.toString()

        shortNameHome.text = entity.homeTeam.shortName
        shortNameAway.text = entity.awayTeam.shortName

        matchDate.text = entity.dateMatch
    }
}

class LiveRealMatchTimelineHeaderHolder(view: View) : LiveRealMatchHolder(view) {

    constructor(parent: ViewGroup) : this(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.live_real_match_timeline_header, parent, false)
    )

    override fun bind(entity: LiveRealMatchEntity) {
        entity as TimelineHeader
    }
}

@SuppressLint("SetTextI18n")
class LiveRealMatchTimelineItemHolder(view: View) : LiveRealMatchHolder(view) {

    private val homeTime: TextView by bindView(R.id.home_time)
    private val awayTime: TextView by bindView(R.id.away_time)
    private val homeIcon: ImageView by bindView(R.id.home_icon)
    private val awayIcon: ImageView by bindView(R.id.away_icon)
    private val homePlayerName: TextView by bindView(R.id.home_player_name)
    private val awayPlayerName: TextView by bindView(R.id.away_player_name)
    private val homeDetails: TextView by bindView(R.id.home_details)
    private val awayDetails: TextView by bindView(R.id.away_details)
    private val homeSecondPlayerName: TextView by bindView(R.id.home_second_player_name)
    private val awaySecondPlayerName: TextView by bindView(R.id.away_second_player_name)
    private val homeAssist: TextView by bindView(R.id.home_assist)
    private val awayAssist: TextView by bindView(R.id.away_assist)
    private val homeInjuryIcon: ImageView by bindView(R.id.home_injury_icon)
    private val awayInjuryIcon: ImageView by bindView(R.id.away_injury_icon)
    private val groupHome: Group by bindView(R.id.group_home)
    private val groupAway: Group by bindView(R.id.group_away)

    constructor(parent: ViewGroup) : this(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.live_real_match_timeline_item, parent, false)
    )

    override fun bind(entity: LiveRealMatchEntity) {
        entity as TimelineItem

        resetViews()

        when (entity.side) {
            SIDE_HOME -> setupHome(entity)
            SIDE_AWAY -> setupAway(entity)
            else -> throw IllegalArgumentException("Unknown side type: ${entity.side}")
        }
    }

    private fun setupHome(entity: TimelineItem) {
        groupAway.isGone = true

        homeTime.text = entity.time

        homeIcon.setImageResource(entity.type.iconRes)

        entity.type.color?.let {
            homeIcon.setColor(it)
        }

        if (entity.details != null) {
            homeDetails.isVisible = true
            homeDetails.text = "(${itemView.context.getString(entity.details)})"
        }

        homePlayerName.text = entity.playerName

        if (entity.isStrikethrough) {
            homePlayerName.paintFlags = homePlayerName.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        }

        entity.secondPlayerName?.let {
            homeSecondPlayerName.isVisible = true
            homeSecondPlayerName.text = it
        }

        if (entity.isAssist) {
            homeAssist.isVisible = true
        }

        homeInjuryIcon.isVisible = entity.reason == REASON_INJURY
    }

    private fun setupAway(entity: TimelineItem) {
        groupHome.isGone = true

        awayTime.text = entity.time

        awayIcon.setImageResource(entity.type.iconRes)

        entity.type.color?.let {
            awayIcon.setColor(it)
        }

        if (entity.details != null) {
            awayDetails.isVisible = true
            awayDetails.text = "(${itemView.context.getString(entity.details)})"
        }

        awayPlayerName.text = entity.playerName

        if (entity.isStrikethrough) {
            awayPlayerName.paintFlags = awayPlayerName.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        }

        entity.secondPlayerName?.let {
            awaySecondPlayerName.isVisible = true
            awaySecondPlayerName.text = it
        }

        if (entity.isAssist) {
            awayAssist.isVisible = true
        }

        awayInjuryIcon.isVisible = entity.reason == REASON_INJURY
    }

    private fun resetViews() {
        homeIcon.clearColorFilter()
        awayIcon.clearColorFilter()
        homeDetails.isGone = true
        awayDetails.isGone = true
        homeSecondPlayerName.isGone = true
        awaySecondPlayerName.isGone = true
        homeAssist.isGone = true
        awayAssist.isGone = true
        homeInjuryIcon.isGone = true
        awayInjuryIcon.isGone = true
        groupHome.isVisible = true
        groupAway.isVisible = true
    }
}
