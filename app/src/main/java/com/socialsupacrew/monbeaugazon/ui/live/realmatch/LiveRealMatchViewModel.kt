package com.socialsupacrew.monbeaugazon.ui.live.realmatch

import com.socialsupacrew.monbeaugazon.core.BaseViewModel
import com.socialsupacrew.monbeaugazon.core.EventPath
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity
import com.socialsupacrew.monbeaugazon.ui.live.realmatch.LiveRealMatchViewModel.Path
import com.socialsupacrew.monbeaugazon.ui.live.realmatch.LiveRealMatchViewModel.State

abstract class LiveRealMatchViewModel : BaseViewModel<State, Path>() {
    sealed class State {
        data class LiveRealMatchLoaded(val matchInfo: List<LiveRealMatchEntity>) : State()
        object LiveRealMatchLoading : State()
        object LiveRealMatchError : State()
    }

    sealed class Path : EventPath.Path

    abstract fun fetchMatch(matchId: String)
}
