package com.socialsupacrew.monbeaugazon.ui.live.realmatch

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.socialsupacrew.monbeaugazon.core.BaseAdapter
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity.LiveRealMatchHeader
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity.TimelineHeader
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRealMatchEntity.TimelineItem
import com.socialsupacrew.monbeaugazon.ui.live.realmatch.LiveRealMatchAdapter.ViewType.HEADER
import com.socialsupacrew.monbeaugazon.ui.live.realmatch.LiveRealMatchAdapter.ViewType.TIMELINE_HEADER
import com.socialsupacrew.monbeaugazon.ui.live.realmatch.LiveRealMatchAdapter.ViewType.TIMELINE_ITEM

class LiveRealMatchAdapter : BaseAdapter<LiveRealMatchEntity, LiveRealMatchHolder>(DIFF_CALLBACK) {

    override fun getItemViewType(position: Int): Int =
        when (items[position]) {
            is LiveRealMatchHeader -> HEADER.id
            TimelineHeader -> TIMELINE_HEADER.id
            is TimelineItem -> TIMELINE_ITEM.id
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LiveRealMatchHolder =
        when (viewType) {
            HEADER.id -> LiveRealMatchHeaderHolder(parent)
            TIMELINE_HEADER.id -> LiveRealMatchTimelineHeaderHolder(parent)
            TIMELINE_ITEM.id -> LiveRealMatchTimelineItemHolder(parent)
            else -> throw NoSuchElementException("$viewType doesn't match with any type")
        }

    override fun onBindViewHolder(holder: LiveRealMatchHolder, position: Int) {
        holder.bind(items[position])
    }

    enum class ViewType {
        HEADER,
        TIMELINE_HEADER,
        TIMELINE_ITEM;

        val id = ordinal
    }

    companion object {
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<LiveRealMatchEntity> =
            object : DiffUtil.ItemCallback<LiveRealMatchEntity>() {
                override fun areItemsTheSame(
                    oldItem: LiveRealMatchEntity,
                    newItem: LiveRealMatchEntity
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: LiveRealMatchEntity,
                    newItem: LiveRealMatchEntity
                ): Boolean {
                    return oldItem.hashCode() == newItem.hashCode()
                }
            }
    }
}
