package com.socialsupacrew.monbeaugazon.ui.dashboard

import android.content.Context
import android.content.Intent
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.drawable.AdaptiveIconDrawable
import android.graphics.drawable.Icon
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.core.BaseActivity
import com.socialsupacrew.monbeaugazon.data.repository.ClubRepository
import com.socialsupacrew.monbeaugazon.data.repository.TeamRepository
import com.socialsupacrew.monbeaugazon.data.repository.UserRepository
import com.socialsupacrew.monbeaugazon.data.repository.local.League
import com.socialsupacrew.monbeaugazon.data.repository.local.LeagueStatus
import com.socialsupacrew.monbeaugazon.design.setupToolbar
import com.socialsupacrew.monbeaugazon.ui.about.AboutActivity
import com.socialsupacrew.monbeaugazon.ui.league.LeagueActivity
import com.socialsupacrew.monbeaugazon.ui.live.LiveActivity
import com.socialsupacrew.monbeaugazon.ui.signin.SignInActivity
import com.socialsupacrew.monbeaugazon.utils.bindView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_dashboard.topAppBar
import org.koin.android.ext.android.inject

class DashboardActivity : BaseActivity(), DashboardHolder.Listener {

    private val userRepository: UserRepository by inject()
    private val teamRepository: TeamRepository by inject()
    private val clubRepository: ClubRepository by inject()

    private var userDisposable: Disposable? = null
    private var clubDisposable: Disposable? = null

    private val leaguesList by bindView<RecyclerView>(R.id.leagueList)

    private val adapter: DashboardAdapter by lazy { DashboardAdapter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        setupToolbar(topAppBar)

        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = RecyclerView.VERTICAL
        leaguesList.layoutManager = linearLayoutManager
        leaguesList.adapter = adapter

        syncClubs()
//        Log.d(TAG, "database path: ${getDatabasePath("MBG_database.db").absolutePath}")
    }

    private fun syncClubs() {
        clubDisposable?.dispose()
        clubDisposable = clubRepository.syncClubs()
            .subscribe({ getLeagues() }, { Log.e(TAG, "error ${it.message}") })
    }

    private fun getLeagues() {
        userDisposable?.dispose()
        userDisposable = userRepository.getLeagues()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                displayLeagues(it)
                getTeams(it)
            }, { Log.e(TAG, it.message, it) })
    }

    private fun getTeams(leagues: List<League>) {
        leagues.forEach {
            teamRepository.syncTeams(it.id)
        }
    }

    private fun displayLeagues(leagues: List<League>) {
        adapter.leagues = leagues
        onLeaguesDisplayed(
            leagues.filter { it.leagueStatus == LeagueStatus.IN_PROGRESS.statusId }
                .take(3).map { it.id to it.name })
    }

    private fun onLeaguesDisplayed(leagues: List<Pair<String, String>>) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            setUpShortcuts(leagues)
        }
    }

    @RequiresApi(Build.VERSION_CODES.N_MR1)
    private fun setUpShortcuts(leagues: List<Pair<String, String>>) {
        val shortcutManager = getSystemService<ShortcutManager>(ShortcutManager::class.java)
        shortcutManager.removeAllDynamicShortcuts()

        val shortcuts = leagues.map {
            ShortcutInfo.Builder(this, it.first)
                .setShortLabel(it.second)
                .setLongLabel(it.second)
                .setIcon(getIcon())
                .setIntent(LeagueActivity.newIntent(this, it.first))
                .build()
        }

        shortcutManager.addDynamicShortcuts(shortcuts)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getIcon(): Icon {
        // TODO: Fix icon, ref:
        //  https://github.com/kabouzeid/Phonograph/blob/master/app/src/main/java/com/kabouzeid/gramophone/appshortcuts/AppShortcutIconGenerator.java
        val background = ContextCompat.getDrawable(this, R.drawable.ic_shortcut_background)
        val foreground = ContextCompat.getDrawable(this, R.drawable.ic_soccer_green_24dp)

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val adaptiveIconDrawable = AdaptiveIconDrawable(background, foreground)
            Icon.createWithAdaptiveBitmap(adaptiveIconDrawable.toBitmap())
        } else {
            val layerDrawable = LayerDrawable(arrayOf(background, foreground))
            Icon.createWithBitmap(layerDrawable.toBitmap())
        }
    }

    override fun onCardClicked(id: String) {
        startActivity(LeagueActivity.newIntent(this, id))
    }

    override fun onDestroy() {
        userDisposable?.dispose()
        clubDisposable?.dispose()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.dashboard_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.disconnect -> {
                disconnect()
                true
            }
            R.id.live -> {
                launchLive()
                true
            }
            R.id.about -> {
                launchAbout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun disconnect() {
        userDisposable?.dispose()
        userDisposable = userRepository.disconnect()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnComplete {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                    getSystemService<ShortcutManager>(ShortcutManager::class.java)
                        .removeAllDynamicShortcuts()
                }
                startActivity(SignInActivity.newIntent(this))
                finish()
            }
            .subscribe({ Log.d(TAG, "Disconnected") }, { Log.e(TAG, it.message, it) })
    }

    private fun launchLive() {
        startActivity(LiveActivity.newIntent(this))
    }

    private fun launchAbout() {
        startActivity(AboutActivity.newIntent(this))
    }

    companion object {
        private const val TAG = "DashboardActivity"
        private const val ADAPTIVE_ICON_SIZE_DP = 108
        private const val ADAPTIVE_ICON_OUTER_SIDES_DP = 18

        fun newIntent(context: Context): Intent {
            return Intent(context, DashboardActivity::class.java)
        }
    }
}
