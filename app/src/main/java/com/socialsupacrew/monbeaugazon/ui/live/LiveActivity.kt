package com.socialsupacrew.monbeaugazon.ui.live

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.core.BaseActivity
import com.socialsupacrew.monbeaugazon.core.OnViewPagerPageChangeListener
import com.socialsupacrew.monbeaugazon.data.repository.local.Championship
import com.socialsupacrew.monbeaugazon.design.setupToolbar
import com.socialsupacrew.monbeaugazon.ui.live.ranking.LiveRankingActivity
import com.socialsupacrew.monbeaugazon.utils.bindView

class LiveActivity : BaseActivity() {

    private val viewPager: ViewPager by bindView(R.id.live_view_pager)
    private val toolbar: Toolbar by bindView(R.id.toolbar_live)
    private val tabLayout: TabLayout by bindView(R.id.live_tabs)
    private val standingFab: ExtendedFloatingActionButton by bindView(R.id.standing_fab)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live)

        setupToolbar(toolbar, R.string.live, true)

        viewPager.offscreenPageLimit = 5
        viewPager.adapter =
            LiveViewPagerAdapter(this, supportFragmentManager, RecyclerViewListener())
        viewPager.addOnPageChangeListener(object : OnViewPagerPageChangeListener() {
            override fun onPageSelected(position: Int) {
                if (position == 0) standingFab.hide()
                else standingFab.show()
            }
        })
        tabLayout.setupWithViewPager(viewPager)

        standingFab.setOnClickListener {
            val championshipId = LiveTabs.getByPosition(viewPager.currentItem).championship.id
            startActivity(LiveRankingActivity.newIntent(this, championshipId))
        }
    }

    inner class RecyclerViewListener : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if ((recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition() == 0) {
                standingFab.extend()
            } else {
                standingFab.shrink()
            }
            super.onScrolled(recyclerView, dx, dy)
        }
    }

    companion object {
        private const val TAG = "LiveActivity"

        fun newIntent(context: Context): Intent {
            return Intent(context, LiveActivity::class.java)
        }
    }
}

@Suppress("unused")
enum class LiveTabs(val fragment: LiveFragment, val championship: Championship) {
    MPG(LiveMpgFragment(), Championship.MPG),
    FRANCE(LiveRealFragment(), Championship.FRANCE),
    FRANCE_2(LiveRealFragment(), Championship.FRANCE_2),
    ENGLAND(LiveRealFragment(), Championship.ENGLAND),
    SPAIN(LiveRealFragment(), Championship.SPAIN),
    ITALY(LiveRealFragment(), Championship.ITALY);

    companion object {
        fun getByPosition(position: Int): LiveTabs {
            return values()[position]
        }
    }
}
