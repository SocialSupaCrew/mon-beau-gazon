package com.socialsupacrew.monbeaugazon.ui.league

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.LeagueRepository
import com.socialsupacrew.monbeaugazon.data.repository.TeamRepository
import io.reactivex.disposables.Disposable
import org.koin.android.ext.android.inject

class TrainerFragment : Fragment() {

    private val leagueRepository: LeagueRepository by inject()
    private val teamRepository: TeamRepository by inject()

    private var leagueDisposable: Disposable? = null
    private var teamDisposable: Disposable? = null

    private val leagueId: String by lazy { arguments?.get("leagueId") as String }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_trainer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        fetch()
    }

//    private fun fetch() {
//        leagueDisposable?.dispose()
//        leagueDisposable = leagueRepository.getLeague(leagueId)
//            .subscribeOn(Schedulers.io())
//            .subscribe({
//                fetch2("mpg_team_$leagueId$$${it.currentMpgUser}")
//            }, { Log.e(TAG, "error: ${it.message}") })
//    }

//    private fun fetch2(userId: String) {
//        teamDisposable?.dispose()
//        teamDisposable = teamRepository.getPlayers(userId, leagueId)
//            .subscribeOn(Schedulers.io())
//            .subscribe({
//                displayJersey(it[0])
//            }, { Log.e(TAG, "error: ${it.message}") })
//    }

//    private fun displayJersey(playerAndClub: PlayerAndClub) {
//        Log.d(TAG, playerAndClub.toString())
//        val jerseyBase64 = getString(ClubJersey.getById(playerAndClub.club.id)!!)
//        val decodedString = Base64.decode(jerseyBase64, Base64.DEFAULT)
//        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
//        Insert Image
//        jersey.setImageBitmap(decodedByte)
//    }

    override fun onDestroy() {
        leagueDisposable?.dispose()
        teamDisposable?.dispose()
        super.onDestroy()
    }

    companion object {
        private const val TAG = "TrainerFragment"

        fun newInstance(leagueId: String): TrainerFragment {
            val fragment = TrainerFragment()
            val args = Bundle()
            args.putString("leagueId", leagueId)
            fragment.arguments = args
            return fragment
        }
    }
}
