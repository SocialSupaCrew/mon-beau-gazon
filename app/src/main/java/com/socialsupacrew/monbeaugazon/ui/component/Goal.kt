package com.socialsupacrew.monbeaugazon.ui.component

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.utils.setColor
import com.socialsupacrew.monbeaugazon.utils.toPx

class Goal(context: Context, attributeSet: AttributeSet) : LinearLayout(context, attributeSet) {

    init {
        orientation = HORIZONTAL
    }

    fun setGoal(goals: List<Boolean>) {
        if (goals.isEmpty()) return

        goals.forEach {
            val goalIcon = ImageView(context).apply {
                setImageResource(R.drawable.ic_soccer_24dp)
                background = ContextCompat.getDrawable(context, R.drawable.bg_ball)
                setColor(if (it) R.color.red_500 else R.color.light_blue_700)
                layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
                    .apply {
                        width = 12.toPx
                        height = 12.toPx
                    }
            }
            addView(goalIcon)
        }
    }
}
