package com.socialsupacrew.monbeaugazon.ui.component

import android.content.Context
import android.os.CountDownTimer
import android.util.AttributeSet
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.utils.bindView
import java.text.DecimalFormat

class Countdown(context: Context, attributeSet: AttributeSet) :
    ConstraintLayout(context, attributeSet) {
    private val dayValue: TextView by bindView(R.id.day_value)
    private val hourValue: TextView by bindView(R.id.hour_value)
    private val minuteValue: TextView by bindView(R.id.minute_value)
    private val secondValue: TextView by bindView(R.id.second_value)

    lateinit var countDownTimer: CountDownTimer

    init {
        inflate(context, R.layout.countdown, this)
    }

    fun setTime(time: Long, onFinish: () -> Unit) {
        if (::countDownTimer.isInitialized) countDownTimer.cancel()

        countDownTimer = object : CountDownTimer(time * 1000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val f = DecimalFormat("00")
                val days = millisUntilFinished / 86400000
                val hours = millisUntilFinished / 3600000 % 24
                val minutes = millisUntilFinished / 60000 % 60
                val seconds = millisUntilFinished / 1000 % 60

                dayValue.text = f.format(days)
                hourValue.text = f.format(hours)
                minuteValue.text = f.format(minutes)
                secondValue.text = f.format(seconds)
            }

            override fun onFinish() {
                onFinish.invoke()
            }
        }

        countDownTimer.start()
    }
}
