package com.socialsupacrew.monbeaugazon.ui.component

import android.content.Context
import android.graphics.BitmapFactory
import android.util.AttributeSet
import android.util.Base64
import androidx.annotation.StringRes
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.local.ClubJersey

class Jersey(
    context: Context,
    attributeSet: AttributeSet
) : AppCompatImageView(context, attributeSet) {

    fun setClubOrUrl(clubId: String?, url: String?) = when {
        clubId != null -> setClub(clubId)
        url != null -> setUrl(url)
        else -> setPlaceholder()
    }

    fun setClub(id: String) {
        val jerseyString = ClubJersey.getById(id) ?: return
        setJersey(jerseyString)
    }

    fun setClub(id: Int) {
        val jerseyString = ClubJersey.getById(id) ?: return
        setJersey(jerseyString)
    }

    fun setUrl(url: String) {
        Glide.with(context)
            .load(url)
            .placeholder(R.drawable.ic_jersey_black_24)
            .into(this)
    }

    private fun setJersey(@StringRes jersey: Int) {
        val jerseyBase64 = context.getString(jersey)
        val decodedString = Base64.decode(jerseyBase64, Base64.DEFAULT)
        val bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        setImageBitmap(bitmap)
    }

    private fun setPlaceholder() {
        setImageResource(R.drawable.ic_jersey_black_24)
    }
}
