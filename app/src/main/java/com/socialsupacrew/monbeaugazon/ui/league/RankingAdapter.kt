package com.socialsupacrew.monbeaugazon.ui.league

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.remote.Team

class RankingAdapter : RecyclerView.Adapter<RankingHolder>() {

    private val teams = mutableListOf<Team>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RankingHolder {
        val inflater = LayoutInflater.from(parent.context)

        if (viewType == ViewType.ITEM.getId()) {
            val view = inflater.inflate(R.layout.ranking_item, parent, false)
            return RankingHolder(view)
        }
        throw NoSuchElementException("$viewType doesn't match with any type")
    }

    override fun getItemCount(): Int {
        return teams.size
    }

    override fun onBindViewHolder(holder: RankingHolder, position: Int) {
        holder.bindView(getItem(position))
    }

    private fun getItem(position: Int): Team {
        return teams[position]
    }

    fun updateRankings(newTeams: List<Team>) {
        val diffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun getOldListSize(): Int {
                return itemCount
            }

            override fun getNewListSize(): Int {
                return newTeams.size
            }

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldTeam = getItem(oldItemPosition)
                val newTeam = newTeams[newItemPosition]
                return oldTeam.teamId == newTeam.teamId
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldTeam = getItem(oldItemPosition)
                val newTeam = newTeams[newItemPosition]
                return oldTeam.hashCode() == newTeam.hashCode()
            }
        })

        teams.clear()
        teams.addAll(newTeams)
        diffResult.dispatchUpdatesTo(this)
    }

    enum class ViewType {
        ITEM;

        fun getId(): Int {
            return this.ordinal
        }
    }
}
