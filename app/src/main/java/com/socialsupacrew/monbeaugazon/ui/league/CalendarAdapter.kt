package com.socialsupacrew.monbeaugazon.ui.league

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.remote.MatchesCalendar

class CalendarAdapter : RecyclerView.Adapter<CalendarHolder>() {

    var matches: List<MatchesCalendar> = emptyList()
        set(value) {
            DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun getOldListSize(): Int = field.size

                override fun getNewListSize(): Int = value.size

                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                    field[oldItemPosition].id == value[newItemPosition].id

                override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                    field[oldItemPosition] == value[newItemPosition]
            }, true)
                .let {
                    field = value
                    it.dispatchUpdatesTo(this)
                }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarHolder {
        val inflater = LayoutInflater.from(parent.context)

        if (viewType == ViewType.ITEM.getId()) {
            val view = inflater.inflate(R.layout.calendar_item, parent, false)
            return CalendarHolder(view)
        }
        throw NoSuchElementException("$viewType doesn't match with any type")
    }

    override fun getItemCount(): Int {
        return matches.size
    }

    override fun onBindViewHolder(holder: CalendarHolder, position: Int) {
        holder.bindView(getItem(position))
    }

    private fun getItem(position: Int): MatchesCalendar {
        return matches[position]
    }

    enum class ViewType {
        ITEM;

        fun getId(): Int {
            return this.ordinal
        }
    }
}
