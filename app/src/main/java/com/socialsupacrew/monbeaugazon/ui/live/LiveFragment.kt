package com.socialsupacrew.monbeaugazon.ui.live

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.local.Championship
import com.socialsupacrew.monbeaugazon.ui.live.LiveActivity.RecyclerViewListener
import com.socialsupacrew.monbeaugazon.utils.bindView

abstract class LiveFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private val swipeRefreshLayout: SwipeRefreshLayout by bindView(R.id.swipe_refresh_layout)

    abstract fun newInstance(
        championship: Championship,
        recyclerViewListener: RecyclerViewListener? = null
    ): LiveFragment

    abstract fun getLive()

    fun stopRefreshing() {
        swipeRefreshLayout.isRefreshing = false
    }

    fun showRefreshing() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipeRefreshLayout.setOnRefreshListener(this)
    }

    override fun onRefresh() {
        getLive()
    }

    companion object {
        internal const val ARG_CHAMPIONSHIP_ID = "arg:championshipId"
    }
}
