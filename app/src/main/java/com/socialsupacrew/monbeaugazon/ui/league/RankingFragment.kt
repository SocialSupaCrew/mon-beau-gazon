package com.socialsupacrew.monbeaugazon.ui.league

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.LeagueRepository
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity
import com.socialsupacrew.monbeaugazon.ui.live.ranking.LiveRankingAdapter
import com.socialsupacrew.monbeaugazon.ui.live.ranking.LiveRankingHolder
import com.socialsupacrew.monbeaugazon.utils.bindView
import com.socialsupacrew.monbeaugazon.utils.ifNull
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject

class RankingFragment : Fragment(), LiveRankingHolder.Listener {

    private val leagueRepository: LeagueRepository by inject()

    private var rankingDisposable: Disposable? = null

    private val leagueId: String by lazy { arguments?.get("leagueId") as String }
    private val rankingList by bindView<RecyclerView>(R.id.ranking_list)
    private var adapter: LiveRankingAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_ranking, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(view.context)
        rankingList.layoutManager = layoutManager

        fetchRanking()
    }

    override fun onClicked(entity: LiveRankingEntity) {
        // TODO("not implemented")
    }

    private fun fetchRanking() {
        rankingDisposable?.dispose()
        rankingDisposable = leagueRepository.getRanking(leagueId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                displayRanking(it)
            }, { Log.d(TAG, "$it") })
    }

    private fun displayRanking(teams: List<LiveRankingEntity>) {
        adapter.ifNull {
            adapter = LiveRankingAdapter(this)
            rankingList.adapter = adapter
        }
        adapter?.items = teams
    }

    override fun onDestroy() {
        rankingDisposable?.dispose()
        super.onDestroy()
    }

    companion object {
        private const val TAG = "RankingFragment"

        fun newInstance(leagueId: String): RankingFragment {
            val fragment = RankingFragment()
            val args = Bundle()
            args.putString("leagueId", leagueId)
            fragment.arguments = args
            return fragment
        }
    }
}
