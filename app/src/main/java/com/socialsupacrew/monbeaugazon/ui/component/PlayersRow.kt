package com.socialsupacrew.monbeaugazon.ui.component

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.remote.LiveMatchPlayerJson
import com.socialsupacrew.monbeaugazon.ui.component.Player.Place.AWAY
import com.socialsupacrew.monbeaugazon.ui.component.Player.Place.HOME

class PlayersRow(context: Context, private val attributeSet: AttributeSet) :
    ConstraintLayout(context, attributeSet) {

    private var position = Position.NONE

    init {
        context.obtainStyledAttributes(attributeSet, R.styleable.PlayersRow, 0, 0)
            .apply {
                val positionId = getInt(R.styleable.PlayersRow_position, Position.NONE.ordinal)
                position = Position.getById(positionId)

                recycle()
            }
    }

    fun addPlayers(players: List<LiveMatchPlayerJson>, home: Boolean, isBench: Boolean = false) {
        players.forEach { player ->
            val playerView = Player(context, attributeSet).apply {
                id = player.hashCode()
                setJersey(player.teamId)
                setName(player.name ?: UNKNOWN_PLAYER_NAME)
                setNote(player.note)
                setPlace(if (home) HOME else AWAY)
                setClub(player.clubName)
                setToolType(player.name ?: UNKNOWN_PLAYER_NAME, player.clubName)
                player.goals?.let {
                    val goals = mutableListOf<Boolean>()
                    repeat(it.goal) { goals.add(false) }
                    repeat(it.ownGoal) { goals.add(true) }
                    setGoal(goals)
                }
                if (isBench) setSize(Player.Size.SMALL)

                layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
            }

            addView(playerView)
        }

        val constraintSet = ConstraintSet()
        constraintSet.clone(this)
        if (players.size > 1) {
            constraintSet.createHorizontalChain(
                ConstraintSet.PARENT_ID, ConstraintSet.LEFT,
                ConstraintSet.PARENT_ID, ConstraintSet.RIGHT,
                players.map { it.hashCode() }.toIntArray(),
                null,
                ConstraintSet.CHAIN_SPREAD
            )
        } else if (players.size == 1) {
            constraintSet.connect(
                players[0].hashCode(),
                ConstraintSet.START,
                ConstraintSet.PARENT_ID,
                ConstraintSet.START
            )
            constraintSet.connect(
                players[0].hashCode(),
                ConstraintSet.END,
                ConstraintSet.PARENT_ID,
                ConstraintSet.END
            )
        }

        constraintSet.applyTo(this)
    }

    companion object {
        private const val UNKNOWN_PLAYER_NAME = "Rotaldo"
    }
}

@Suppress("unused")
enum class Position {
    NONE,
    GOALKEEPER,
    DEFENDERS,
    MIDFIELDERS,
    FORWARDS;

    companion object {
        internal fun getById(id: Int): Position = values()[id]
    }
}
