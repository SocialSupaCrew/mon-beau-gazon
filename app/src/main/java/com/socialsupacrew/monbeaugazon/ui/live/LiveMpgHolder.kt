package com.socialsupacrew.monbeaugazon.ui.live

import android.animation.ValueAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.constraintlayout.widget.Group
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.google.android.material.card.MaterialCardView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveMpgEntity
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveMpgEntity.FooterLive
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveMpgEntity.HeaderLive
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveMpgEntity.MatchLive
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveMpgEntity.PlayerLive
import com.socialsupacrew.monbeaugazon.utils.bindView
import com.socialsupacrew.monbeaugazon.utils.doOnGlobalLayout
import com.socialsupacrew.monbeaugazon.utils.setColor
import com.socialsupacrew.monbeaugazon.utils.toPx
import kotlin.math.max

sealed class LiveMpgHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun bindView(entity: LiveMpgEntity)
    abstract fun updateExpanded(isExpanded: Boolean)

    class LiveMpgHeaderHolder(itemView: View) : LiveMpgHolder(itemView) {

        constructor(parent: ViewGroup) : this(
            LayoutInflater.from(parent.context).inflate(R.layout.live_header, parent, false)
        )

        private val leagueName by bindView<TextView>(R.id.league_name)

        override fun bindView(entity: LiveMpgEntity) {
            entity as HeaderLive
            leagueName.text = entity.leagueName
        }

        override fun updateExpanded(isExpanded: Boolean) {
            // Do nothing
        }
    }

    class LiveMpgItemHolder(itemView: View, private val listener: Listener) :
        LiveMpgHolder(itemView) {

        constructor(parent: ViewGroup, listener: Listener) : this(
            LayoutInflater.from(parent.context).inflate(R.layout.live_item, parent, false),
            listener
        )

        private val nameHome: TextView by bindView(R.id.name_home)
        private val scoreHome: TextView by bindView(R.id.score_home)
        private val scoreAway: TextView by bindView(R.id.score_away)
        private val nameAway: TextView by bindView(R.id.name_away)
        private val separator: TextView by bindView(R.id.team_separator)

        private val playerHomeContainer: LinearLayout by bindView(R.id.container_player_home)
        private val playerAwayContainer: LinearLayout by bindView(R.id.container_player_away)
        private val btnDetails: MaterialButton by bindView(R.id.btn_details)
        private val detailsGroup: Group by bindView(R.id.details_group)

        private var detailsHeight: Int = 0
        private var itemOriginalHeight: Int = 0

        private val expandAnimation: ValueAnimator by lazy { expandAnimation() }

        override fun bindView(entity: LiveMpgEntity) {
            entity as MatchLive
            nameHome.text = entity.teamHome.name
            scoreHome.text = entity.teamHome.score.toString()
            scoreAway.text = entity.teamAway.score.toString()
            nameAway.text = entity.teamAway.name

            resetViews()
            bindPlayers(entity)

            detailsGroup.doOnGlobalLayout {
                detailsHeight =
                    max(playerHomeContainer.height, playerAwayContainer.height) + btnDetails.height
                detailsGroup.isGone = true
            }

            itemView.setOnClickListener { listener.onItemClicked(entity.id) }
            separator.setOnClickListener { listener.onTeamClicked(entity.leagueId, entity.id) }
            btnDetails.setOnClickListener { listener.onTeamClicked(entity.leagueId, entity.id) }
        }

        override fun updateExpanded(isExpanded: Boolean) {
            if (isExpanded) expandAnimation.start()
            else expandAnimation.reverse()
        }

        private fun resetViews() {
            playerHomeContainer.removeAllViews()
            playerAwayContainer.removeAllViews()
            playerHomeContainer.updatePadding(bottom = 0)
            playerAwayContainer.updatePadding(bottom = 0)
        }

        private fun expandAnimation(): ValueAnimator {
            val animator = ValueAnimator.ofFloat(0f, 1f)
            val layoutParams = itemView.layoutParams as ViewGroup.MarginLayoutParams
            val actualHeight = itemView.measuredHeight
            itemOriginalHeight = actualHeight
            detailsGroup.isVisible = true

            animator.addUpdateListener {
                val value = it.animatedValue as Float

                layoutParams.marginStart = (8.toPx * value).toInt()
                layoutParams.marginEnd = (8.toPx * value).toInt()
                layoutParams.height =
                    if (value == 1f) ViewGroup.LayoutParams.WRAP_CONTENT else (detailsHeight * value).toInt() + actualHeight
                (itemView as MaterialCardView).cardElevation = 8.toPx * value
                itemView.layoutParams = layoutParams
                itemView.requestLayout()
            }
            return animator
        }

        private fun bindPlayers(entity: MatchLive) {
            if (entity.teamHome.players.isEmpty() && entity.teamAway.players.isEmpty()) return

            playerHomeContainer.updatePadding(bottom = 8.toPx)
            playerAwayContainer.updatePadding(bottom = 8.toPx)

            val inflater = LayoutInflater.from(itemView.context)
            entity.teamHome.players.forEach {
                val view = bindPlayerAndBalls(it, TeamAttribute.HOME, inflater)
                playerHomeContainer.addView(view)
            }
            entity.teamAway.players.forEach {
                val view = bindPlayerAndBalls(it, TeamAttribute.AWAY, inflater)
                playerAwayContainer.addView(view)
            }
        }

        private fun bindPlayerAndBalls(
            player: PlayerLive,
            team: TeamAttribute,
            inflater: LayoutInflater
        ): View {
            val view = inflater.inflate(team.livePlayerLayout, null, false)
            val name = view.findViewById<TextView>(R.id.player_name)
            name.text = player.lastName

            val ballContainer = view.findViewById<LinearLayout>(team.ballContainerId)
            ballContainer.removeAllViews()

            for (i in 0 until player.goal) {
                val ball = inflater.inflate(R.layout.live_ball, ballContainer, false)
                val ballImg = ball.findViewById<ImageView>(R.id.live_ball)
                ballImg.setColor(R.color.light_blue_700)
                ballContainer.addView(ball)
            }
            for (i in 0 until player.ownGoal) {
                val ball = inflater.inflate(R.layout.live_ball, ballContainer, false)
                val ballImg = ball.findViewById<ImageView>(R.id.live_ball)
                ballImg.setColor(R.color.red_500)
                ballContainer.addView(ball)
            }
            return view
        }

        enum class TeamAttribute(
            @LayoutRes val livePlayerLayout: Int,
            @IdRes val ballContainerId: Int
        ) {
            HOME(R.layout.live_player_home, R.id.container_ball_home),
            AWAY(R.layout.live_player_away, R.id.container_ball_away)
        }

        interface Listener {
            fun onTeamClicked(leagueId: String, matchId: String)
            fun onItemClicked(matchId: String)
        }
    }

    class LiveMpgFooterHolder(itemView: View) : LiveMpgHolder(itemView) {

        constructor(parent: ViewGroup) : this(
            LayoutInflater.from(parent.context).inflate(R.layout.item_live_footer, parent, false)
        )

        override fun bindView(entity: LiveMpgEntity) {
            entity as FooterLive
        }

        override fun updateExpanded(isExpanded: Boolean) {
            // Do nothing
        }
    }
}
