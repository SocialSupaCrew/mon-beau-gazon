package com.socialsupacrew.monbeaugazon.ui.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.local.League

class DashboardAdapter(private val listener: DashboardHolder.Listener) :
    RecyclerView.Adapter<DashboardHolder>() {

    var leagues: List<League> = emptyList()
        set(value) {
            DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun getOldListSize(): Int = field.size

                override fun getNewListSize(): Int = value.size

                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                    field[oldItemPosition].id == value[newItemPosition].id

                override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                    field[oldItemPosition] == value[newItemPosition]
            }, true)
                .let {
                    field = value
                    it.dispatchUpdatesTo(this)
                }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardHolder {
        val inflater = LayoutInflater.from(parent.context)

        if (viewType == ViewType.ITEM.getId()) {
            val view = inflater.inflate(R.layout.item_league, parent, false)
            return DashboardHolder(view, listener)
        }
        throw NoSuchElementException("$viewType doesn't match with any type")
    }

    override fun onBindViewHolder(holder: DashboardHolder, position: Int) {
        holder.bindView(getItem(position))
    }

    override fun getItemCount(): Int {
        return leagues.size
    }

    private fun getItem(position: Int): League {
        return leagues[position]
    }

    enum class ViewType {
        ITEM;

        fun getId(): Int {
            return this.ordinal
        }
    }
}
