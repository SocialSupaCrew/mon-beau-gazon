package com.socialsupacrew.monbeaugazon.ui.component

import android.content.Context
import android.graphics.BitmapFactory
import android.util.AttributeSet
import android.util.Base64
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StyleRes
import androidx.appcompat.widget.TooltipCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.local.ClubJersey
import com.socialsupacrew.monbeaugazon.utils.bindView
import com.socialsupacrew.monbeaugazon.utils.toPx

class Player(context: Context, attributeSet: AttributeSet) :
    ConstraintLayout(context, attributeSet) {

    private val jersey: Jersey by bindView(R.id.jersey)
    private val note: TextView by bindView(R.id.note)
    private val name: TextView by bindView(R.id.name)
    private val goal: Goal by bindView(R.id.goal)

    private lateinit var place: Place

    init {
        inflate(context, R.layout.player, this)

        context.obtainStyledAttributes(attributeSet, R.styleable.Player, 0, 0)
            .apply {
                val placeId = getInt(R.styleable.Player_place, Place.HOME.ordinal)
                setPlace(Place.getById(placeId))
                recycle()
            }
    }

    fun setJersey(clubId: Int) {
        jersey.setClub(clubId)
    }

    fun setNote(newNote: Float?) {
        if (newNote == null || newNote == 0f) return
        note.visibility = View.VISIBLE

        val noteToDisplay = newNote.toString().substringBefore(".0")
        note.text = noteToDisplay
    }

    fun setName(newName: String) {
        name.text = newName
    }

    fun setGoal(goals: List<Boolean>) {
        goal.setGoal(goals)
    }

    fun setPlace(place: Place) {
        this.place = place
        note.setBackgroundResource(place.backgroundRes)
        note.setTextColor(ContextCompat.getColor(context, place.textColorRes))
    }

    fun setClub(clubName: String?) {
        jersey.contentDescription = clubName
    }

    fun setToolType(name: String, clubName: String?) {
        TooltipCompat.setTooltipText(jersey, "$name - $clubName")
    }

    fun setSize(size: Size) {
        jersey.layoutParams.width = size.jerseySize
        jersey.layoutParams.height = size.jerseySize
        name.setTextAppearance(size.nameStyle)
        name.maxWidth = size.nameMaxWidth
        name.setTextColor(name.context.getColor(R.color.white))
        note.layoutParams.width = size.noteSize
        note.layoutParams.height = size.noteSize
        note.setTextAppearance(size.noteStyle)
        note.setTextColor(note.context.getColor(place.textColorRes))
    }

    enum class Place(@DrawableRes val backgroundRes: Int, @ColorRes val textColorRes: Int) {
        HOME(R.drawable.bg_note_home, R.color.white),
        AWAY(R.drawable.bg_note_away, R.color.blue_600);

        companion object {
            internal fun getById(ordinal: Int): Place {
                return values()[ordinal]
            }
        }
    }

    @Suppress("unused")
    enum class Size(
        val jerseySize: Int,
        @StyleRes val nameStyle: Int,
        val nameMaxWidth: Int,
        @StyleRes val noteStyle: Int,
        val noteSize: Int
    ) {
        SMALL(
            32.toPx,
            R.style.TextAppearance_MBG_Caption,
            50.toPx,
            R.style.TextAppearance_MBG_Caption_Small,
            18.toPx
        ),
        NORMAL(
            48.toPx,
            R.style.TextAppearance_MBG_Subtitle2,
            80.toPx,
            R.style.TextAppearance_MBG_Caption,
            22.toPx
        )
    }
}
