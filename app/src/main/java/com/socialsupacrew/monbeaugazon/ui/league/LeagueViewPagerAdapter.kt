package com.socialsupacrew.monbeaugazon.ui.league

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class LeagueViewPagerAdapter(context: Context, fragmentManager: FragmentManager, leagueId: String) :
    FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val fragmentList = mutableListOf<Fragment>()
    private val fragmentTitleList = mutableListOf<String>()

    override fun getItem(position: Int): Fragment = fragmentList[position]
    override fun getCount(): Int = fragmentList.size
    override fun getPageTitle(position: Int): CharSequence? = fragmentTitleList[position]

    init {
        LeagueTabs.values().forEach {
            when (it.fragment) {
                is CalendarFragment -> fragmentList.add(CalendarFragment.newInstance(leagueId))
                is RankingFragment -> fragmentList.add(RankingFragment.newInstance(leagueId))
                is TrainerFragment -> fragmentList.add(TrainerFragment.newInstance(leagueId))
                else -> throw IllegalArgumentException("Unknown fragment: ${it.fragment}")
            }
            fragmentTitleList.add(context.getString(it.title))
        }
    }
}
