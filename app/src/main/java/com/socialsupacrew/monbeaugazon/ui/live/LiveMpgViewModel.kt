package com.socialsupacrew.monbeaugazon.ui.live

import com.socialsupacrew.monbeaugazon.core.BaseViewModel
import com.socialsupacrew.monbeaugazon.core.EventPath
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveMpgEntity
import com.socialsupacrew.monbeaugazon.ui.live.LiveMpgHolder.LiveMpgItemHolder
import com.socialsupacrew.monbeaugazon.ui.live.LiveMpgViewModel.Path
import com.socialsupacrew.monbeaugazon.ui.live.LiveMpgViewModel.State

abstract class LiveMpgViewModel : BaseViewModel<State, Path>(), LiveMpgItemHolder.Listener {

    sealed class State {
        data class LiveLoaded(val teams: List<LiveMpgEntity>) : State()
        data class EmptyLiveLoaded(val time: Long) : State()
        object LiveLoading : State()
        object LiveError : State()
    }

    sealed class Path : EventPath.Path {
        data class MatchDetail(val leagueId: String, val matchId: String) : Path()
    }

    abstract fun fetchLive()
    abstract fun fetchEmptyLive()
}
