package com.socialsupacrew.monbeaugazon.ui.league

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.remote.Team
import com.socialsupacrew.monbeaugazon.utils.bindView

class RankingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val position by bindView<TextView>(R.id.rank_position)
    private val name by bindView<TextView>(R.id.rank_team_name)
    private val points by bindView<TextView>(R.id.rank_point)
    private val played by bindView<TextView>(R.id.rank_played)
    private val win by bindView<TextView>(R.id.rank_win)
    private val draw by bindView<TextView>(R.id.rank_draw)
    private val lost by bindView<TextView>(R.id.rank_lost)
    private val diff by bindView<TextView>(R.id.rank_diff)

    fun bindView(team: Team) {
        position.text = team.rank.toString()
        name.text = team.name
        points.text = team.points.toString()
        played.text = team.played.toString()
        win.text = team.victory.toString()
        draw.text = team.draw.toString()
        lost.text = team.defeat.toString()
        diff.text = team.difference.toString()
    }
}
