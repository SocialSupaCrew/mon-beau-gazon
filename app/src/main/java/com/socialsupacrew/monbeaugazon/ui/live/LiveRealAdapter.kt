package com.socialsupacrew.monbeaugazon.ui.live

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.HeaderRealLive
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.MatchRealLive
import com.socialsupacrew.monbeaugazon.ui.live.LiveRealHolder.*

class LiveRealAdapter(
    private val listener: LiveRealItemHolder.Listener
) : RecyclerView.Adapter<LiveRealHolder>() {

    private val live = mutableListOf<LiveRealEntity>()

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is HeaderRealLive -> ViewType.HEADER.getId()
            is MatchRealLive -> ViewType.ITEM.getId()
            LiveRealEntity.FooterRealLive -> ViewType.FOOTER.getId()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LiveRealHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            ViewType.HEADER.getId() -> {
                val view = inflater.inflate(R.layout.live_championship_header, parent, false)
                LiveRealHeaderHolder(view)
            }
            ViewType.ITEM.getId() -> {
                val view = inflater.inflate(R.layout.live_championship_item, parent, false)
                LiveRealItemHolder(view, listener)
            }
            ViewType.FOOTER.getId() -> {
                val view = inflater.inflate(R.layout.live_championship_footer, parent, false)
                LiveRealFooterHolder(view)
            }
            else -> throw NoSuchElementException("$viewType doesn't match with any type")
        }
    }

    override fun getItemCount(): Int {
        return live.size
    }

    override fun onBindViewHolder(holder: LiveRealHolder, position: Int) {
        holder.bindView(getItem(position))
    }

    private fun getItem(position: Int): LiveRealEntity {
        return live[position]
    }

    fun updateLiveRealEntities(newLiveEntities: List<LiveRealEntity>) {
        val diffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun getOldListSize(): Int {
                return itemCount
            }

            override fun getNewListSize(): Int {
                return newLiveEntities.size
            }

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldLiveEntity = getItem(oldItemPosition)
                val newLiveEntity = newLiveEntities[newItemPosition]
                return oldLiveEntity.id == newLiveEntity.id
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldLiveEntity = getItem(oldItemPosition)
                val newLiveEntity = newLiveEntities[newItemPosition]
                return oldLiveEntity.hashCode() == newLiveEntity.hashCode()
            }
        })

        live.clear()
        live.addAll(newLiveEntities)
        diffResult.dispatchUpdatesTo(this)
    }

//    override fun onClick(view: View?) {
//        view?.let {
//            val holder = it.tag
//            if (holder is LiveHeaderHolder) return
//            holder as LiveItemHolder
//            val position = holder.adapterPosition
//
//            if (expandedItemPosition > 0) {
//                notifyItemChanged(expandedItemPosition)
//            }
//
//            expandedItemPosition = if (expandedItemPosition != position) {
//                position
//            } else {
//                -1
//            }
//
//            notifyItemChanged(position)
//
//        }
//    }

    enum class ViewType {
        HEADER,
        ITEM,
        FOOTER;

        fun getId(): Int {
            return this.ordinal
        }
    }
}
