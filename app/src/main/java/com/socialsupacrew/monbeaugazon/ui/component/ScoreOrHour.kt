package com.socialsupacrew.monbeaugazon.ui.component

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.utils.bindView

class ScoreOrHour(
    context: Context,
    attributeSet: AttributeSet
) : ConstraintLayout(context, attributeSet) {

    private val hourTv: TextView by bindView(R.id.hour)
    private val scoreHomeTv: TextView by bindView(R.id.score_home)
    private val scoreAwayTv: TextView by bindView(R.id.score_away)
    private val timeTv: TextView by bindView(R.id.time)
    private val scoreContainer: ConstraintLayout by bindView(R.id.score_container)
    private val hourGroup: Group by bindView(R.id.hour_group)
    private val scoreGroup: Group by bindView(R.id.score_group)
    private val timeGroup: Group by bindView(R.id.time_group)

    init {
        inflate(context, R.layout.score_or_hour, this)
        hourGroup.visibility = View.GONE
        scoreGroup.visibility = View.GONE
        timeGroup.visibility = View.GONE
    }

    fun reset() {
        hourGroup.visibility = View.GONE
        scoreGroup.visibility = View.GONE
        timeGroup.visibility = View.GONE
    }

    fun setHour(hour: String) {
        hourGroup.visibility = View.VISIBLE
        scoreContainer.background = null
        hourTv.text = hour
    }

    fun setScore(scoreHome: Int, scoreAway: Int) {
        scoreGroup.visibility = View.VISIBLE
        scoreContainer.setBackgroundResource(R.drawable.bg_live_score)
        scoreHomeTv.text = scoreHome.toString()
        scoreAwayTv.text = scoreAway.toString()
    }

    @SuppressLint("SetTextI18n")
    fun setTime(time: Int) {
        timeGroup.visibility = View.VISIBLE
        timeTv.text = "$time'"
    }
}
