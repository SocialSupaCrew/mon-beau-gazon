package com.socialsupacrew.monbeaugazon.ui.live.ranking

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.socialsupacrew.monbeaugazon.core.EventPath
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveRankingInteractor
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity

class LiveRankingViewModelImpl(
    private val state: MediatorLiveData<State>,
    private val navigation: MutableLiveData<EventPath<Path>>,
    private val getLiveRankingInteractor: GetLiveRankingInteractor,
    private val championshipId: Int
) : LiveRankingViewModel() {

    init {
        state.addSource(getLiveRankingInteractor.getLiveData(), ::onFetchLiveRankingResult)
    }

    override fun onCleared() {
        getLiveRankingInteractor.cleanUp()
        super.onCleared()
    }

    override fun getState(): LiveData<State> = state

    override fun getNavigation(): LiveData<EventPath<Path>> = navigation

    override fun fetchTeams() {
        getLiveRankingInteractor.execute(championshipId)
    }

    override fun navigateTo(path: Path) {
        navigation.value = EventPath(path)
    }

    override fun onClicked(entity: LiveRankingEntity) {
        // Do nothing
    }

    private fun onFetchLiveRankingResult(result: GetLiveRankingInteractor.Result) {
        state.value = when (result) {
            is GetLiveRankingInteractor.Result.OnSuccess -> State.TeamsLoaded(result.teams)
            GetLiveRankingInteractor.Result.OnError -> State.TeamsError
        }
    }
}
