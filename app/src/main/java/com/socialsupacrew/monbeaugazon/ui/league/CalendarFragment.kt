package com.socialsupacrew.monbeaugazon.ui.league

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.LeagueRepository
import com.socialsupacrew.monbeaugazon.data.repository.remote.CalendarJson
import com.socialsupacrew.monbeaugazon.data.repository.remote.MatchesCalendar
import com.socialsupacrew.monbeaugazon.utils.MbgLogger
import com.socialsupacrew.monbeaugazon.utils.bindView
import com.socialsupacrew.monbeaugazon.utils.getOrdinal
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject
import org.threeten.bp.format.DateTimeFormatter

class CalendarFragment : Fragment() {

    private val leagueRepository: LeagueRepository by inject()

    private var currentDayDisposable: Disposable? = null
    private var otherDayDisposable: Disposable? = null
    private val leagueId: String by lazy { arguments?.get(ARGS_LEAGUE_ID) as String }

    private val matchesList: RecyclerView by bindView(R.id.calendar_list)
    private val adapter: CalendarAdapter by lazy { CalendarAdapter() }

    private var currentDay: Int = 0

    private val calendarPrevious: Button by bindView(R.id.calendar_previous)
    private val calendarNext: Button by bindView(R.id.calendar_next)
    private val calendarSelect: Spinner by bindView(R.id.calendar_date_select)
    private val calendarDate: TextView by bindView(R.id.calendar_date)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_calendar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(view.context)
        matchesList.layoutManager = layoutManager
        matchesList.adapter = adapter

        calendarPrevious.setOnClickListener { fetchPreviousDay() }
        calendarNext.setOnClickListener { fetchNextDay() }

        currentDayDisposable?.dispose()
        currentDayDisposable = leagueRepository.getCalendar(leagueId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                currentDay = it.currentMatchDay
                displayHeaderInfo(it)
                displayMatches(it.matches)
                setUpSelect(it.maxMatchDay)
            }, { MbgLogger.error(it) })
    }

    private fun fetchPreviousDay() {
        fetchDay(currentDay - 1)
    }

    private fun fetchNextDay() {
        fetchDay(currentDay + 1)
    }

    private fun fetchDay(day: Int) {
        otherDayDisposable?.dispose()
        otherDayDisposable = leagueRepository.getCalendar(leagueId, day)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                currentDay = it.currentMatchDay
                displayHeaderInfo(it)
                displayMatches(it.matches)
                updateSelect()
            }, { MbgLogger.error(it) })
    }

    private fun displayHeaderInfo(calendar: CalendarJson) {
        val formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy")
        calendarDate.text = calendar.date.format(formatter)
    }

    private fun displayMatches(matches: List<MatchesCalendar>) {
        adapter.matches = matches
    }

    private fun setUpSelect(maxMatchDay: Int) {
        val matchDayList = mutableListOf<String>()
        for (i in 1..maxMatchDay) {
            matchDayList.add(getString(R.string.match_day, i.getOrdinal()))
        }

        context?.let {
            val dataAdapter = ArrayAdapter(it, R.layout.custom_spinner_item, matchDayList)
            dataAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item)
            calendarSelect.adapter = dataAdapter

            calendarSelect.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    // Do nothing
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    fetchDay(position + 1)
                }
            }
        }
        updateSelect()
    }

    private fun updateSelect() {
        calendarSelect.setSelection(currentDay - 1)
    }

    override fun onDestroy() {
        currentDayDisposable?.dispose()
        otherDayDisposable?.dispose()
        super.onDestroy()
    }

    companion object {
        private const val ARGS_LEAGUE_ID = "args:leagueId"

        fun newInstance(leagueId: String): CalendarFragment {
            val fragment = CalendarFragment()
            val args = Bundle()
            args.putString(ARGS_LEAGUE_ID, leagueId)
            fragment.arguments = args
            return fragment
        }
    }
}
