package com.socialsupacrew.monbeaugazon.ui.live

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity
import com.socialsupacrew.monbeaugazon.data.repository.local.Championship
import com.socialsupacrew.monbeaugazon.ui.live.LiveActivity.RecyclerViewListener
import com.socialsupacrew.monbeaugazon.ui.live.LiveRealHolder.LiveRealItemHolder
import com.socialsupacrew.monbeaugazon.ui.live.realmatch.LiveRealMatchDialog
import com.socialsupacrew.monbeaugazon.utils.bindView
import com.socialsupacrew.monbeaugazon.utils.ifNull
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject

class LiveRealFragment : LiveFragment(), LiveRealItemHolder.Listener {

    private val liveRepository: LiveRepository by inject()
    private var championshipDisposable: Disposable? = null
    private val championship: Championship by lazy {
        Championship.getChampionshipById(arguments?.get(ARG_CHAMPIONSHIP_ID) as Int)
    }

    private var adapter: LiveRealAdapter? = null

    private val liveList: RecyclerView by bindView(R.id.live_championship_list)
    private lateinit var recyclerViewListener: RecyclerViewListener

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_live_championship, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getLive()

        val layoutManager = LinearLayoutManager(view.context)
        layoutManager.orientation = RecyclerView.VERTICAL
        liveList.layoutManager = layoutManager
        if (::recyclerViewListener.isInitialized) {
            liveList.addOnScrollListener(recyclerViewListener)
        }
    }

    override fun getLive() {
        championshipDisposable?.dispose()
        championshipDisposable = liveRepository.getLiveChampionship(championship)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                stopRefreshing()
                displayLive(it)
            }, { Log.e(TAG, "error: $it") })
    }

    private fun displayLive(entities: List<LiveRealEntity>) {
        adapter.ifNull {
            adapter = LiveRealAdapter(this)
            liveList.adapter = adapter
        }
        adapter?.updateLiveRealEntities(entities)
    }

    override fun onMatchClicked(matchId: String) {
        fragmentManager?.let {
            LiveRealMatchDialog.newInstance(matchId).show(it, "matchId")
        }
    }

    override fun newInstance(
        championship: Championship,
        recyclerViewListener: RecyclerViewListener?
    ): LiveRealFragment {
        return LiveRealFragment().apply {
            val bundle = Bundle()
            bundle.putInt(ARG_CHAMPIONSHIP_ID, championship.id)
            arguments = bundle
            this.recyclerViewListener = requireNotNull(recyclerViewListener)
        }
    }

    override fun onDestroy() {
        championshipDisposable?.dispose()
        super.onDestroy()
    }

    companion object {
        private const val TAG = "LiveFrance1Fragment"
    }
}
