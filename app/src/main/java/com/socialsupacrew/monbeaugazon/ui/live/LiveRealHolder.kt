package com.socialsupacrew.monbeaugazon.ui.live

import android.annotation.SuppressLint
import android.graphics.Paint
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.FooterRealLive
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.GoalRealLive
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.GoalType
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.HeaderRealLive
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.MatchRealLive
import com.socialsupacrew.monbeaugazon.data.repository.LiveRepository.LiveRealEntity.Period
import com.socialsupacrew.monbeaugazon.ui.component.Jersey
import com.socialsupacrew.monbeaugazon.ui.component.ScoreOrHour
import com.socialsupacrew.monbeaugazon.utils.bindView

sealed class LiveRealHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun bindView(entity: LiveRealEntity)

    class LiveRealHeaderHolder(itemView: View) : LiveRealHolder(itemView) {

        private val date by bindView<TextView>(R.id.date)

        override fun bindView(entity: LiveRealEntity) {
            entity as HeaderRealLive
            date.text = entity.date
        }
    }

    class LiveRealItemHolder(
        itemView: View,
        private val listener: Listener
    ) : LiveRealHolder(itemView) {

        private val nameHome: TextView by bindView(R.id.name_home)
        private val nameAway: TextView by bindView(R.id.name_away)
        private val jerseyHome: Jersey by bindView(R.id.jersey_home)
        private val jerseyAway: Jersey by bindView(R.id.jersey_away)
        private val scoreOrHour: ScoreOrHour by bindView(R.id.score_or_hour)
        private val playerHomeContainer: LinearLayout by bindView(R.id.container_player_home)
        private val playerAwayContainer: LinearLayout by bindView(R.id.container_player_away)

//        private val btnDetails by bindView<MaterialButton>(R.id.btn_details)
//        private val detailsGroup by bindView<Group>(R.id.details_group)

        override fun bindView(entity: LiveRealEntity) {
            entity as MatchRealLive
            resetView()

            nameHome.text = entity.teamHome.name
            nameAway.text = entity.teamAway.name
            jerseyHome.setClub(entity.teamHome.clubId)
            jerseyAway.setClub(entity.teamAway.clubId)

            when (entity.period) {
                Period.FULL_TIME -> {
                    bindScore(entity)
                    bindWinner(entity)
                    bindGoals(entity)
                }
                Period.FIRST_HALF,
                Period.HALF_TIME,
                Period.SECOND_HALF -> {
                    bindScore(entity)
                    bindWinner(entity)
                    entity.time?.let { bindTime(it) }
                    bindGoals(entity)
                }
                Period.NULL -> {
                    bindHour(entity.hour)
                }
            }

            itemView.setOnClickListener { listener.onMatchClicked(entity.id) }
        }

        private fun resetView() {
            playerHomeContainer.removeAllViews()
            playerAwayContainer.removeAllViews()
            nameHome.setTextAppearance(R.style.TextAppearance_MBG_Subtitle1)
            nameAway.setTextAppearance(R.style.TextAppearance_MBG_Subtitle1)
            scoreOrHour.reset()
        }

        private fun bindHour(hour: String) {
            scoreOrHour.setHour(hour)
        }

        private fun bindScore(entity: MatchRealLive) {
            val scoreHome = entity.teamHome.score as Int
            val scoreAway = entity.teamAway.score as Int
            scoreOrHour.setScore(scoreHome, scoreAway)
        }

        private fun bindTime(time: Int) {
            scoreOrHour.setTime(time)
        }

        private fun bindWinner(entity: MatchRealLive) {
            val scoreHome = entity.teamHome.score as Int
            val scoreAway = entity.teamAway.score as Int
            when {
                homeHasWin(scoreHome, scoreAway) -> {
                    nameHome.setTextAppearance(R.style.TextAppearance_MBG_Subtitle1_Bold)
                }
                awayHasWin(scoreHome, scoreAway) -> {
                    nameAway.setTextAppearance(R.style.TextAppearance_MBG_Subtitle1_Bold)
                }
            }
        }

        private fun bindGoals(entity: MatchRealLive) {
            if (entity.teamHome.goals.isEmpty() && entity.teamAway.goals.isEmpty()) return

            val inflater = LayoutInflater.from(itemView.context)

            playerHomeContainer.visibility =
                if (entity.teamHome.goals.isEmpty()) View.GONE
                else View.VISIBLE

            entity.teamHome.goals.forEach {
                val view = getGoalView(it, TeamAttribute.HOME, inflater)
                playerHomeContainer.addView(view)
            }

            playerAwayContainer.visibility =
                if (entity.teamAway.goals.isEmpty()) View.GONE
                else View.VISIBLE

            entity.teamAway.goals.forEach {
                val view = getGoalView(it, TeamAttribute.AWAY, inflater)
                playerAwayContainer.addView(view)
            }
        }

        @SuppressLint("SetTextI18n")
        private fun getGoalView(
            goal: GoalRealLive,
            team: TeamAttribute,
            inflater: LayoutInflater
        ): View {
            val view = inflater.inflate(team.livePlayerLayout, null, false)
            val time: TextView = view.findViewById(R.id.goal_time)
            val name: TextView = view.findViewById(R.id.player_name)
            val type: TextView = view.findViewById(R.id.goal_type)

            time.text = "${goal.goalTime}'"
            name.text = goal.lastName
            val goalType = "(${view.context.getString(goal.type.typeText)})"

            when (goal.type) {
                GoalType.VAR -> {
                    type.visibility = View.VISIBLE
                    type.text = goalType
                    name.paintFlags = name.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                }
                GoalType.CSC,
                GoalType.PENALTY -> {
                    type.visibility = View.VISIBLE
                    type.text = goalType
                }
                GoalType.GOAL,
                GoalType.NULL -> {
                    type.visibility = View.GONE
                }
            }
            return view
        }

        private fun homeHasWin(scoreHome: Int, scoreAway: Int): Boolean {
            return scoreHome > scoreAway
        }

        private fun awayHasWin(scoreHome: Int, scoreAway: Int): Boolean {
            return scoreHome < scoreAway
        }

        enum class TeamAttribute(@LayoutRes val livePlayerLayout: Int) {
            HOME(R.layout.live_real_player_home),
            AWAY(R.layout.live_real_player_away)
        }

        interface Listener {
            fun onMatchClicked(matchId: String)
        }
    }

    class LiveRealFooterHolder(itemView: View) : LiveRealHolder(itemView) {

        override fun bindView(entity: LiveRealEntity) {
            entity as FooterRealLive
        }
    }
}
