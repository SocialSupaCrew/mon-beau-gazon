package com.socialsupacrew.monbeaugazon.ui.live

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.socialsupacrew.monbeaugazon.ui.live.LiveActivity.RecyclerViewListener

class LiveViewPagerAdapter(
    context: Context,
    fragmentManager: FragmentManager,
    recyclerViewListener: RecyclerViewListener
) : FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val fragmentList = mutableListOf<Fragment>()
    private val fragmentTitleList = mutableListOf<String>()

    override fun getItem(position: Int): Fragment = fragmentList[position]
    override fun getCount(): Int = fragmentList.size
    override fun getPageTitle(position: Int): CharSequence? = fragmentTitleList[position]

    init {
        LiveTabs.values().forEach {
            fragmentList.add(it.fragment.newInstance(it.championship, recyclerViewListener))
            fragmentTitleList.add(context.getString(it.championship.championshipName))
        }
    }
}
