package com.socialsupacrew.monbeaugazon.ui.live

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.socialsupacrew.monbeaugazon.core.BaseAdapter
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveMpgEntity
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveMpgEntity.FooterLive
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveMpgEntity.HeaderLive
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveMpgEntity.MatchLive
import com.socialsupacrew.monbeaugazon.ui.live.LiveMpgHolder.LiveMpgFooterHolder
import com.socialsupacrew.monbeaugazon.ui.live.LiveMpgHolder.LiveMpgHeaderHolder
import com.socialsupacrew.monbeaugazon.ui.live.LiveMpgHolder.LiveMpgItemHolder

class LiveMpgAdapter(
    private val listener: LiveMpgItemHolder.Listener
) : BaseAdapter<LiveMpgEntity, LiveMpgHolder>(DIFF_CALLBACK) {

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is HeaderLive -> ViewType.HEADER.id
            is MatchLive -> ViewType.ITEM.id
            is FooterLive -> ViewType.FOOTER.id
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LiveMpgHolder {
        return when (viewType) {
            ViewType.HEADER.id -> LiveMpgHeaderHolder(parent)
            ViewType.ITEM.id -> LiveMpgItemHolder(parent, listener)
            ViewType.FOOTER.id -> LiveMpgFooterHolder(parent)
            else -> throw NoSuchElementException("$viewType doesn't match with any type")
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: LiveMpgHolder, position: Int) {
        holder.bindView(getItem(position))
    }

    override fun onBindViewHolder(
        holder: LiveMpgHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        when {
            payloads.isEmpty() -> holder.bindView(getItem(position))
            else -> holder.updateExpanded((payloads[0] as MatchLive).isExpended)
        }
    }

    enum class ViewType {
        HEADER,
        ITEM,
        FOOTER;

        val id: Int = ordinal
    }

    companion object {
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<LiveMpgEntity> =
            object : DiffUtil.ItemCallback<LiveMpgEntity>() {
                override fun areItemsTheSame(
                    oldItem: LiveMpgEntity,
                    newItem: LiveMpgEntity
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: LiveMpgEntity,
                    newItem: LiveMpgEntity
                ): Boolean {
                    return oldItem.hashCode() == newItem.hashCode()
                }

                override fun getChangePayload(
                    oldItem: LiveMpgEntity,
                    newItem: LiveMpgEntity
                ): Any? {
                    return newItem
                }
            }
    }
}
