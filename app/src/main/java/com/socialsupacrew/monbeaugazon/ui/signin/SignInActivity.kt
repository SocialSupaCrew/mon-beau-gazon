package com.socialsupacrew.monbeaugazon.ui.signin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.socialsupacrew.monbeaugazon.core.BaseActivity
import com.socialsupacrew.monbeaugazon.Entry
import com.socialsupacrew.monbeaugazon.Preferences
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.UserRepository
import com.socialsupacrew.monbeaugazon.data.repository.remote.CallbackWrapper
import com.socialsupacrew.monbeaugazon.data.repository.remote.SignInUserBody
import com.socialsupacrew.monbeaugazon.data.repository.remote.SignInUserJson
import com.socialsupacrew.monbeaugazon.ui.dashboard.DashboardActivity
import com.socialsupacrew.monbeaugazon.utils.MbgLogger
import com.socialsupacrew.monbeaugazon.utils.Snack
import com.socialsupacrew.monbeaugazon.utils.isEnterEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_signin.emailField
import kotlinx.android.synthetic.main.activity_signin.passwordField
import kotlinx.android.synthetic.main.activity_signin.signInButton
import org.koin.android.ext.android.inject

class SignInActivity : BaseActivity() {

    private val userRepository: UserRepository by inject()
    private val preferences: Preferences by inject()
    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)

        signInButton.setOnClickListener { signIn(it, getUserBody()) }

        passwordField.setOnEditorActionListener { _, actionId, event ->
            if (isEnterEvent(actionId, event)) {
                signInButton.performClick()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    private fun getUserBody(): SignInUserBody {
        return SignInUserBody(
            emailField.text.toString(),
            passwordField.text.toString()
        )
    }

    private fun signIn(view: View, signInUserBody: SignInUserBody) {
        disposable = userRepository.signInUser(signInUserBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : CallbackWrapper<SignInUserJson>() {
                override fun onSuccess(result: SignInUserJson) {
                    preferences.setString(Entry.TOKEN, result.token)
                    preferences.setString(Entry.OID, result.oid)
                    startActivity(DashboardActivity.newIntent(this@SignInActivity))
                    this@SignInActivity.finish()
                }

                override fun onError(throwable: Throwable) {
                    super.onError(throwable)
                    MbgLogger.error(throwable)
                    Snack.displayError(view, R.string.error)
                }
            })
    }

    override fun onDestroy() {
        disposable?.dispose()
        super.onDestroy()
    }

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, SignInActivity::class.java)
        }
    }
}
