package com.socialsupacrew.monbeaugazon.ui.live.ranking

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.socialsupacrew.monbeaugazon.core.BaseAdapter
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity

class LiveRankingAdapter(
    private val listener: LiveRankingHolder.Listener
) : BaseAdapter<LiveRankingEntity, LiveRankingHolder>(DIFF_CALLBACK) {

    override fun getItemViewType(position: Int): Int =
        when (items[position]) {
            LiveRankingEntity.LiveRankingHeaderEntity -> ViewType.HEADER.id
            is LiveRankingEntity.LiveRankingTeamEntity -> ViewType.TEAM.id
            LiveRankingEntity.LiveRankingFooterEntity -> ViewType.FOOTER.id
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LiveRankingHolder {
        return when (viewType) {
            ViewType.HEADER.id -> LiveRankingHeaderHolder(parent)
            ViewType.TEAM.id -> LiveRankingTeamHolder(parent, listener)
            ViewType.FOOTER.id -> LiveRankingFooterHolder(parent)
            else -> throw NoSuchElementException("$viewType doesn't match with any type")
        }
    }

    override fun onBindViewHolder(holder: LiveRankingHolder, position: Int) {
        holder.bind(items[position])
    }

    enum class ViewType {
        HEADER,
        TEAM,
        FOOTER;

        val id: Int = ordinal
    }

    companion object {
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<LiveRankingEntity> =
            object : DiffUtil.ItemCallback<LiveRankingEntity>() {
                override fun areItemsTheSame(
                    oldItem: LiveRankingEntity,
                    newItem: LiveRankingEntity
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: LiveRankingEntity,
                    newItem: LiveRankingEntity
                ): Boolean {
                    return oldItem.hashCode() == newItem.hashCode()
                }
            }
    }
}
