package com.socialsupacrew.monbeaugazon.ui.league

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.core.BaseActivity
import com.socialsupacrew.monbeaugazon.data.repository.LeagueRepository
import com.socialsupacrew.monbeaugazon.design.setupToolbar
import com.socialsupacrew.monbeaugazon.utils.bindView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject

class LeagueActivity : BaseActivity() {

    private val leagueRepository: LeagueRepository by inject()

    private val viewPager: ViewPager by bindView(R.id.league_view_pager)
    private val toolbar: Toolbar by bindView(R.id.toolbar_league)
    private val tabLayout: TabLayout by bindView(R.id.league_tabs)

    private val leagueId: String by lazy { intent.getStringExtra(EXTRA_LEAGUE_ID) ?: "" }

    private var leagueDisposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_league)

        viewPager.offscreenPageLimit = 2
        viewPager.adapter = LeagueViewPagerAdapter(this, supportFragmentManager, leagueId)
        tabLayout.setupWithViewPager(viewPager)

        setupActionBarTitle()
        setupIconTabs()
    }

    private fun setupActionBarTitle() {
        leagueDisposable?.dispose()
        leagueDisposable = leagueRepository.getLeague(leagueId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                setupToolbar(toolbar, it.name, true)
            }, { Log.e(TAG, it.message, it) })
    }

    private fun setupIconTabs() {
        LeagueTabs.values().forEach {
            tabLayout.getTabAt(it.ordinal)?.icon = ContextCompat.getDrawable(this, it.icon)
        }
    }

    override fun onDestroy() {
        leagueDisposable?.dispose()
        super.onDestroy()
    }

    companion object {
        private const val TAG = "LeagueActivity"
        private const val EXTRA_LEAGUE_ID = "extra:leagueId"

        fun newIntent(context: Context, id: String): Intent {
            return Intent(
                Intent.ACTION_VIEW,
                Uri.EMPTY,
                context,
                LeagueActivity::class.java
            ).apply {
                putExtra(EXTRA_LEAGUE_ID, id)
            }
        }
    }
}

@Suppress("unused")
enum class LeagueTabs(
    val fragment: Fragment,
    @StringRes val title: Int,
    @DrawableRes val icon: Int
) {
    CALENDAR(
        CalendarFragment(),
        R.string.title_calendar,
        R.drawable.ic_outline_calendar_white_24
    ),
    RANKING(RankingFragment(), R.string.title_ranking, R.drawable.ic_outline_trophy_white_24)
//    TRAINER(TrainerFragment(), R.string.title_trainer, R.drawable.ic_outline_field_white_24px)
}
