package com.socialsupacrew.monbeaugazon.ui.live.realmatch

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.socialsupacrew.monbeaugazon.core.EventPath
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveRealMatchInteractor
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveRealMatchInteractor.Result

class LiveRealMatchViewModelImpl(
    private val state: MediatorLiveData<State>,
    private val navigation: MutableLiveData<EventPath<Path>>,
    private val getLiveRealMatchInteractor: GetLiveRealMatchInteractor,
    matchId: String
) : LiveRealMatchViewModel() {

    init {
        state.addSource(getLiveRealMatchInteractor.getLiveData(), ::onFetchLiveRealMatchResult)
        fetchMatch(matchId)
    }

    override fun onCleared() {
        getLiveRealMatchInteractor.cleanUp()
        super.onCleared()
    }

    override fun getState(): LiveData<State> = state

    override fun getNavigation(): LiveData<EventPath<Path>> = navigation

    override fun fetchMatch(matchId: String) {
        getLiveRealMatchInteractor.fetchMatch(matchId)
    }

    override fun navigateTo(path: Path) {
        navigation.value = EventPath(path)
    }

    private fun onFetchLiveRealMatchResult(result: Result) {
        state.value = when (result) {
            is Result.OnSuccess -> State.LiveRealMatchLoaded(result.matchInfo)
            Result.OnError -> State.LiveRealMatchError
            Result.OnLoading -> State.LiveRealMatchLoading
        }
    }
}
