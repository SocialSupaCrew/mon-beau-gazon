package com.socialsupacrew.monbeaugazon.ui.live.realmatch

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.ui.live.realmatch.LiveRealMatchViewModel.State
import com.socialsupacrew.monbeaugazon.utils.CustomDividerItem
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class LiveRealMatchDialog : BottomSheetDialogFragment() {

    private val matchId: String by lazy { arguments?.getString(EXTRA_MATCH_ID, "") ?: "" }

    private val viewModel: LiveRealMatchViewModel by viewModel { parametersOf(matchId) }

    private val adapter: LiveRealMatchAdapter by lazy { LiveRealMatchAdapter() }

    private lateinit var list: RecyclerView

    private val bottomSheetBehavior: BottomSheetBehavior<View> = BottomSheetBehavior()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheet = super.onCreateDialog(savedInstanceState)
        bottomSheet.setContentView(R.layout.live_real_match_dialog)
        bindViews(bottomSheet)

        //setting Peek at the 16:9 ratio keyline of its parent.
//        bottomSheetBehavior.peekHeight = BottomSheetBehavior.PEEK_HEIGHT_AUTO

        return bottomSheet
    }

    private fun bindViews(bottomSheet: Dialog) {
        list = bottomSheet.findViewById(R.id.live_real_match_list)

        list.layoutManager = LinearLayoutManager(requireContext())
        list.adapter = adapter
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        viewModel.getState().observe(owner = this) { onStateChanged(it) }
    }

    private fun onStateChanged(state: State) {
        Log.d("QSD", "onStateChanged: $state")

        when (state) {
            is State.LiveRealMatchLoaded -> adapter.items = state.matchInfo
            State.LiveRealMatchLoading -> {
            }
            State.LiveRealMatchError -> {
            }
        }
    }

    companion object {
        private const val EXTRA_MATCH_ID = "extra:matchId"

        fun newInstance(matchId: String): LiveRealMatchDialog {
            val args = Bundle().apply {
                putString(EXTRA_MATCH_ID, matchId)
            }
            return LiveRealMatchDialog().apply { arguments = args }
        }
    }
}
