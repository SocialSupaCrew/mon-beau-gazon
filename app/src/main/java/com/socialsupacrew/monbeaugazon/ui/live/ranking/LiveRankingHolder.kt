package com.socialsupacrew.monbeaugazon.ui.live.ranking

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity.LiveRankingFooterEntity
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity.LiveRankingHeaderEntity
import com.socialsupacrew.monbeaugazon.data.repository.entity.LiveRankingEntity.LiveRankingTeamEntity
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_live_ranking_team.drawn
import kotlinx.android.synthetic.main.item_live_ranking_team.goalAgainst
import kotlinx.android.synthetic.main.item_live_ranking_team.goalDiff
import kotlinx.android.synthetic.main.item_live_ranking_team.goalFor
import kotlinx.android.synthetic.main.item_live_ranking_team.jersey
import kotlinx.android.synthetic.main.item_live_ranking_team.lost
import kotlinx.android.synthetic.main.item_live_ranking_team.name
import kotlinx.android.synthetic.main.item_live_ranking_team.played
import kotlinx.android.synthetic.main.item_live_ranking_team.points
import kotlinx.android.synthetic.main.item_live_ranking_team.rank
import kotlinx.android.synthetic.main.item_live_ranking_team.won

sealed class LiveRankingHolder(
    override val containerView: View
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    abstract fun bind(entity: LiveRankingEntity)

    interface Listener {
        fun onClicked(entity: LiveRankingEntity)
    }
}

class LiveRankingHeaderHolder(
    view: View
) : LiveRankingHolder(view) {

    constructor(parent: ViewGroup) : this(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.item_live_ranking_header, parent, false)
    )

    override fun bind(entity: LiveRankingEntity) {
        entity as LiveRankingHeaderEntity
    }
}

class LiveRankingTeamHolder(
    private val view: View,
    private val listener: Listener
) : LiveRankingHolder(view) {

    constructor(parent: ViewGroup, listener: Listener) : this(
        LayoutInflater.from(parent.context).inflate(R.layout.item_live_ranking_team, parent, false),
        listener
    )

    override fun bind(entity: LiveRankingEntity) {
        entity as LiveRankingTeamEntity
        rank.text = entity.rank
        jersey.setClubOrUrl(entity.clubId, entity.jerseyUrl)
        name.text = entity.name
        points.text = entity.points
        played.text = entity.played
        won.text = entity.won
        lost.text = entity.lost
        drawn.text = entity.drawn
        if (goalFor != null) goalFor.text = entity.goalFor
        if (goalAgainst != null) goalAgainst.text = entity.goalAgainst
        goalDiff.text = entity.goalDiff

        view.setOnClickListener { listener.onClicked(entity) }
    }
}

class LiveRankingFooterHolder(
    view: View
) : LiveRankingHolder(view) {

    constructor(parent: ViewGroup) : this(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.item_live_ranking_footer, parent, false)
    )

    override fun bind(entity: LiveRankingEntity) {
        entity as LiveRankingFooterEntity
    }
}
