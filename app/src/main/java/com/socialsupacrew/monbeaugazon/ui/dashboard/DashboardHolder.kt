package com.socialsupacrew.monbeaugazon.ui.dashboard

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.google.android.material.card.MaterialCardView
import com.socialsupacrew.monbeaugazon.R
import com.socialsupacrew.monbeaugazon.data.repository.local.Championship
import com.socialsupacrew.monbeaugazon.data.repository.local.League
import com.socialsupacrew.monbeaugazon.data.repository.local.LeagueStatus.FINISHED
import com.socialsupacrew.monbeaugazon.data.repository.local.LeagueStatus.IN_PROGRESS
import com.socialsupacrew.monbeaugazon.data.repository.local.LeagueStatus.MERCATO
import com.socialsupacrew.monbeaugazon.data.repository.local.LeagueStatus.MERCATO_RELAUNCH
import com.socialsupacrew.monbeaugazon.data.repository.local.LeagueStatus.PREPARING
import com.socialsupacrew.monbeaugazon.data.repository.local.LeagueStatus.UNKNOWN
import com.socialsupacrew.monbeaugazon.utils.bindView
import com.socialsupacrew.monbeaugazon.utils.toBitmap

class DashboardHolder(itemView: View, private val listener: Listener) :
    RecyclerView.ViewHolder(itemView) {

    private val container: MaterialCardView by bindView(R.id.league_container)
    private val flagChampionship: ImageView by bindView(R.id.flag_championship)
    private val name: TextView by bindView(R.id.league_name)
    private val status: TextView by bindView(R.id.league_status)
    private val btnGoBack: MaterialButton by bindView(R.id.btn_go_back)

    fun bindView(league: League) {
        name.text = league.name
        status.text = computeStatus(league.leagueStatus)

        computeFlag(league.championship)
        container.setOnClickListener { listener.onCardClicked(league.id) }
        btnGoBack.setOnClickListener { listener.onCardClicked(league.id) }
    }

    private fun computeStatus(leagueStatus: Int): String {
        return when (leagueStatus) {
            PREPARING.statusId -> itemView.context.getString(R.string.league_status_preparing)
            UNKNOWN.statusId -> itemView.context.getString(R.string.league_status_unknown)
            MERCATO.statusId -> itemView.context.getString(R.string.league_status_mercato)
            IN_PROGRESS.statusId -> itemView.context.getString(R.string.league_status_in_progress)
            FINISHED.statusId -> itemView.context.getString(R.string.league_status_finished)
            MERCATO_RELAUNCH.statusId -> itemView.context.getString(R.string.league_status_finished)
            else -> itemView.context.getString(R.string.league_status_unknown)
        }
    }

    private fun computeFlag(championshipId: Int) {
        val championship = Championship.getChampionshipById(championshipId)

        val res = itemView.resources
        val drawable =
            ContextCompat.getDrawable(itemView.context, championship.flag)?.toBitmap() ?: return
        val roundedDrawable = RoundedBitmapDrawableFactory.create(res, drawable)
        roundedDrawable.cornerRadius = Math.max(drawable.width, drawable.height) / 2.0f
        flagChampionship.setImageDrawable(roundedDrawable)
    }

    interface Listener {
        fun onCardClicked(id: String)
    }
}
