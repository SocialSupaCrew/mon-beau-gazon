package com.socialsupacrew.monbeaugazon.ui.live

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.socialsupacrew.monbeaugazon.core.EventPath
import com.socialsupacrew.monbeaugazon.data.interactor.GetEmptyLiveMpgInteractor
import com.socialsupacrew.monbeaugazon.data.interactor.GetLiveMpgInteractor
import com.socialsupacrew.monbeaugazon.ui.live.LiveMpgViewModel.Path.MatchDetail

class LiveMpgViewModelImpl(
    private val state: MediatorLiveData<State>,
    private val navigation: MutableLiveData<EventPath<Path>>,
    private val getLiveMpgInteractor: GetLiveMpgInteractor,
    private val getEmptyLiveMpgInteractor: GetEmptyLiveMpgInteractor
) : LiveMpgViewModel() {

    init {
        state.addSource(getLiveMpgInteractor.getLiveData(), ::onFetchLiveMpgResult)
        state.addSource(getEmptyLiveMpgInteractor.getLiveData(), ::onFetchEmptyLiveMpgResult)
        fetchLive()
    }

    override fun onCleared() {
        getLiveMpgInteractor.cleanUp()
        getEmptyLiveMpgInteractor.cleanUp()
        super.onCleared()
    }

    override fun getState(): LiveData<State> = state

    override fun getNavigation(): LiveData<EventPath<Path>> = navigation

    override fun fetchLive() {
        getLiveMpgInteractor.execute()
    }

    override fun fetchEmptyLive() {
        getEmptyLiveMpgInteractor.execute()
    }

    override fun navigateTo(path: Path) {
        navigation.value = EventPath(path)
    }

    override fun onTeamClicked(leagueId: String, matchId: String) {
        navigateTo(MatchDetail(leagueId, matchId))
    }

    override fun onItemClicked(matchId: String) {
        getLiveMpgInteractor.getWithSelection(matchId)
    }

    private fun onFetchLiveMpgResult(result: GetLiveMpgInteractor.Result) {
        state.value = when (result) {
            is GetLiveMpgInteractor.Result.OnSuccess -> State.LiveLoaded(result.teams)
            GetLiveMpgInteractor.Result.OnError -> {
                fetchEmptyLive()
                State.LiveLoading
            }
            GetLiveMpgInteractor.Result.OnLoading -> State.LiveLoading
        }
    }

    private fun onFetchEmptyLiveMpgResult(result: GetEmptyLiveMpgInteractor.Result) {
        state.value = when (result) {
            is GetEmptyLiveMpgInteractor.Result.OnSuccess -> State.EmptyLiveLoaded(result.time)
            GetEmptyLiveMpgInteractor.Result.OnError -> State.LiveError
            GetEmptyLiveMpgInteractor.Result.OnLoading -> State.LiveLoading
        }
    }
}
